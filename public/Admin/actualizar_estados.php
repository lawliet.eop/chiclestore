<?php
    include '../conexion/conn.php';
    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    date_default_timezone_set('america/lima');

    $month = date('m');
    $day = date('d');
    $year = date('Y');

    $today = $year . '-' . $month . '-' . $day;

    $fecha = date("Y-m-d", strtotime($today));
    $nuevafecha = strtotime ( '-6 month' , strtotime ( $fecha ) ) ;
    $nuevafecha = date ( 'Y-m-d' , $nuevafecha );

    $result1 = mysqli_query($conn, "UPDATE `cuenta` SET `estadoCuenta` = 3 WHERE `estadoCuenta` = 4 AND `ultimoReseteo` = '$nuevafecha'");

    header('location: cuentas.php');
?>
