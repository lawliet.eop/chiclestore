<?php
    @session_start();
    include '../conexion/conn.php';
    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    date_default_timezone_set('america/lima');

    $month = date('m');
    $day = date('d');
    $year = date('Y');

    $today = $year . '-' . $month . '-' . $day;
    //id de la cuenta a cambiar el estado
    $micuenta = $_POST["id"];
    $nuevoEstado = $_POST["nuevoEstado"];

    $result1 = mysqli_query($conn, "SELECT `idCliente` FROM `cliente` WHERE cuenta_idCuenta = '$micuenta'");
    if (mysqli_num_rows($result1) == 0){
        /*
         * todo dentro de este if se modifico para cambiar de estado sin un cliente asociado
         */
        $result2 = mysqli_query($conn, "SELECT `nReseteos` FROM `cuenta` WHERE idCuenta = '$micuenta'");
        $row2 = mysqli_fetch_assoc($result2);
        $reset = $row2["nReseteos"];

        $reset++;

        $result3 = mysqli_query($conn, "UPDATE `cuenta` SET `nReseteos` = '$reset', `ultimoReseteo` = '$today', `estadoCuenta` = '$nuevoEstado' WHERE `idCuenta` = '$micuenta';");

        $json = array();
        $json[0] = array(
            'nReseteos'=>$reset,
            'FechaReseteo'=>$today
        );
        $jsonstring = json_encode($json[0]);
        echo $jsonstring;
    }else{
        $row1 = mysqli_fetch_assoc($result1);
        $idCliente = $row1["idCliente"];

        $result2 = mysqli_query($conn, "SELECT `nReseteos` FROM `cuenta` WHERE idCuenta = '$micuenta'");
        $row2 = mysqli_fetch_assoc($result2);
        $reset = $row2["nReseteos"];

        $reset++;

        $result3 = mysqli_query($conn, "UPDATE `cuenta` SET `nReseteos` = '$reset', `ultimoReseteo` = '$today', `estadoCuenta` = '$nuevoEstado' WHERE `idCuenta` = '$micuenta';");

        $result4 = mysqli_query($conn, "UPDATE `cliente` SET `cuenta_idCuenta` = NULL WHERE `idCliente` = '$idCliente';");

        $json = array();
        $json[0] = array(
            'nReseteos'=>$reset,
            'FechaReseteo'=>$today
        );
        $jsonstring = json_encode($json[0]);
        echo $jsonstring;
    }

