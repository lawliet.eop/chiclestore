    <?php

@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }
  include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
  //error_reporting (0);
?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Comprar</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'comprar'; include('../includes/navbar1.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

        <!-- Begin Page Content -->
        <div class="container-fluid" id="mi-tabla">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 "><strong>Juegos por Comprar</strong></h1>
          <p class="mb-4">Lista de Juegos requeridos que no se encuentran disponibles.</p>

          <!-- DataTales Example -->
          <div id="cardshadow" class="card shadow mb-4" >
            <div id="cardbody" class="card-body">
<!-- ******************************************************************************************** -->

            <?php
              $cont = 1;
              //$result = mysqli_query($conn, "SELECT * FROM cliente WHERE estCliente ='NO ATENDIDO' INNER JOIN  cuenta WHERE juego_idJuego = '$idJuego' AND estadoCuenta = 'DISPONIBLE'");
            $result = mysqli_query($conn, "SELECT juego_idJuego,COUNT(juego_idJuego) FROM `cliente` WHERE estCliente = 'NO ATENDIDO' AND juego_idJuego != '1' AND  tVenta_idtVenta != '1' GROUP BY `cliente`.`juego_idJuego`");
              while ($row = mysqli_fetch_assoc($result)){
                $juego_idJuego = $row["juego_idJuego"];
                $count = $row["COUNT(juego_idJuego)"];

                  $result1 = mysqli_query($conn, "SELECT * FROM `cuenta` WHERE estadoCuenta = 1 AND juego_idJuego = '$juego_idJuego'");
                  $nCuentas = mysqli_num_rows($result1);
                if ($nCuentas < $count){
                    $cantidad = $count-$nCuentas;
                    echo "<div class=\"col-xl-12 col-lg-8\">
                  <div class=\"card shadow mb-4\">
                    <div class=\"card-header bg-dark py-3 d-flex flex-row align-items-center justify-content-between\">
                      <h6 class=\"m-0 font-weight-bold text-white\">Juego ".$cont."</h6>
                    </div>
                    <div class=\"card-body\">
                      <form class=\"user\" action=\"registrar-cuenta.php\" method=\"POST\">
                        <div class=\"form-group row\">
                          <div class=\"col-sm-6 mb-3 mb-sm-0\">
                          <label>Juego Requerido</label>";

                                  $result2 = mysqli_query($conn, "SELECT nombre FROM juego WHERE idJuego = '$juego_idJuego' ");
                                  $row2 = mysqli_fetch_assoc($result2);
                                  $nombre_juego = $row2["nombre"];

                    echo "<input name=\"game\" type=\"text\" class=\"form-control\" value=\"$nombre_juego\" readonly>
                          </div>";
                    echo "<div class=\"col-sm-6 mb-3 mb-sm-0\">
                            <label>Cantidad de Cuentas Requeridas</label>
                            <input name=\"nombre\" type=\"text\" class=\"form-control\" value=\"$cantidad\" readonly>
                          </div>";
                    $result3 = mysqli_query($conn, "SELECT * FROM cuenta WHERE estadoCuenta = 2 AND juego_idJuego = '$juego_idJuego'");
                    $juegosEsperaDescarga = mysqli_num_rows($result3);

                    $result6 = mysqli_query($conn, "SELECT * FROM cuenta WHERE estadoCuenta = 3 AND juego_idJuego = '$juego_idJuego'");
                    $juegosPeticionReseteo = mysqli_num_rows($result6);

                    $result10 = mysqli_query($conn, "SELECT * FROM cuenta WHERE estadoCuenta = 4 AND juego_idJuego = '$juego_idJuego' AND ventaSecundario = 0");
                    $juegosVentaSecundaria = mysqli_num_rows($result10);

                          echo "<div class=\"col-sm-6 \"><br>

                          <div class=\"input-group mb-4\">
                            <div class=\"input-group-prepend\">
                              <span class=\"input-group-text bg-danger\" id=\"basic-addon1\" style=\"color: #FFF;\"><strong>Esperando Descarga</strong></span>
                            </div>
                            <input type=\"text\" class=\"form-control\" aria-describedby=\"basic-addon1\" value=\"$juegosEsperaDescarga   cuentas encontrada(s).\" readonly>
                          </div>

                          <div class=\"input-group mb-4\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text bg-info\" id=\"basic-addon1\" style=\"color: #FFF;\"><strong>Petición de Reseteo&nbsp</strong></span>
                          </div>
                          <input type=\"text\" class=\"form-control\" aria-describedby=\"basic-addon1\" value=\"$juegosPeticionReseteo   cuentas encontrada(s).\" readonly>
                          </div>

                          <div class=\"input-group mb-4\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text bg-warning\" id=\"basic-addon1\" style=\"color: #FFF;\"><strong>Para Venta Secundaria</strong></span>
                          </div>
                          <input type=\"text\" class=\"form-control\" aria-describedby=\"basic-addon1\" value=\"$juegosVentaSecundaria   cuentas encontrada(s).\" readonly>
                          </div>

                          </div>";


                    echo "<hr>";

                          if ($juego_idJuego == 102) {
                            echo "<div class=\"col-sm-6\">

                              </div>";
                          }else {
                            echo "<div class=\"col-sm-6\">
                            <br><br><br>
                              <a href= \"#\" id=\"submit\" class=\"btn btn-primary btn-user btn-block\" data-toggle=\"modal\" data-target=\"#confirma$cont\" disabled>
                                  <strong>AGREGAR</strong>
                              </a>
                              </div>";
                          }
                    echo "</div>";
                    echo"
                            <div class=\"modal fade\" id=\"confirma$cont\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                              <div class=\"modal-dialog\" role=\"document\">
                                <div class=\"modal-content\">
                                  <div class=\"modal-header\">
                                    <h5 class=\"modal-title\" id=\"exampleModalLabel\">¿Desea Agregar Una Cuenta?</h5>
                                    <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">
                                      <span aria-hidden=\"true\">×</span>
                                    </button>
                                  </div>
                                  <div class=\"modal-body\">Seleccione \"Continuar\" para agregar cuenta(s) para el Juego <strong>\"$nombre_juego\".</strong></div>
                                  <div class=\"modal-footer\">
                                    <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">Cancelar</button>
                                    <button class=\"btn btn-primary\" type='submit'>Continuar</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                      </form>
                    </div>
                  </div>
                </div>";
                    $cont++;
                }

                }
            ?>


            </div>
          </div>
        </div>
      </div>



      <!-- End of Main Content -->

      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>
  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>
  <script src="../js/clipboard.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.rawgit.com/zenorocha/clipboard.js/v1.5.3/dist/clipboard.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/dark-mode.js"></script>
<!--<script>
      $(document).ready(function()
      {
          $("#mconfirm").modal("show");
      });
  </script>-->

</body>

</html>
