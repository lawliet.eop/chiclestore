<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }

include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Cuentas Disponibles</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script src="../js/jquery.min.js"></script>

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'cuentasd'; include('../includes/navbar1.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

        <!-- Begin Page Content -->
        <div class="container-fluid" id="mi-tabla">

          <!-- Page Heading -->
          <h1 class="h3 mb-2"><strong>Cuentas Disponibles en Secundario</strong></h1>
          <p class="mb-4">Tabla de datos de las cuentas con estado DISPONIBLE.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4" >
            <div class="card-header bg-dark py-3">
            </br>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr class="bg-dark text-white">
                        <th class="text-center" style="border: none;">Videojuego</th>
                        <th class="text-center" style="border: none;">Videojuego</th>
                        <th class="text-center" style="border: none;">Cantidad</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  $cont = 1;

                  $result = mysqli_query($conn, "SELECT juego_idJuego,COUNT(juego_idJuego) FROM `cuenta` WHERE estadoCuenta = '4' AND ventaSecundario = 0 GROUP BY `cuenta`.`juego_idJuego`");
                    while ($row = mysqli_fetch_assoc($result)){
                      $juego_idJuego = $row["juego_idJuego"];
                      $count = $row["COUNT(juego_idJuego)"];

                      $result2 = mysqli_query($conn, "SELECT * FROM `cuenta` WHERE estadoCuenta = '4' AND ventaSecundario = 0 AND juego_idJuego = $juego_idJuego LIMIT 1");
                      $row2 = mysqli_fetch_assoc($result2);
                      $preciov = $row2["precioVendido"];

                      echo "<tr>";
                      $result1 = mysqli_query($conn, "SELECT * FROM juego WHERE idJuego = $juego_idJuego");
                      $row1 = mysqli_fetch_assoc($result1);
                      $juego = $row1["nombre"];
                      echo "<td class=\"text-center\"><strong>$juego</strong></td>";
                      echo "<td class=\"text-center\">".$preciov." S/.</td>";
                      echo "<td class=\"text-center\">".$count."</td>";
                  $cont++;}
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>



      <!-- End of Main Content -->


      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/ajax/estado-cuenta.js"></script>
  <script src="../js/dark-mode.js"></script>

</body>

</html>
