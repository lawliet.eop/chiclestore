<?php
    @session_start();
    include '../conexion/conn.php';
    $idFinan = $_SESSION['id'];
    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $result = mysqli_query($conn, "SELECT * FROM finanzas WHERE idFinanza = '$idFinan'");
    $row = mysqli_fetch_assoc($result);
    $ingreso = $row["ingresos"];

    $diaant = $row["diaAnterior"];
    $result3 = mysqli_query($conn, "SELECT * FROM `finanzas` WHERE idFinanza = '$diaant'");
    $row3 = mysqli_fetch_assoc($result3);
    $totalant = $row3["hastaHoy"];

    $sComp = $_POST["gcomp"];
    $gastPer = $_POST["gpers"];
    $gastPub = $_POST["gpub"];
    $gastE = $_POST["gext"];


    $gastost = $gastE + $gastPub + $gastPer + $sComp;

    $total = $ingreso - $gastost;
    $hoy = $totalant + $total;

      //actualizar finanza
    $result3 = mysqli_query($conn, "UPDATE `finanzas` SET `saldoComprado` = '$sComp', `gastoPersonal` = '$gastPer', `gastoPublicidad` = '$gastPub', `gastoExtra` = '$gastE', `total` = '$total', `hastaHoy` = '$hoy' WHERE `finanzas`.`idFinanza` = '$idFinan';");


    $_SESSION['alert-edit-finan'] = "<div class=\"modal fade\" id=\"error1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\" id=\"exampleModalLabel\">¡Hecho!</h5>
          </div>
          <div class=\"modal-body text-center\"><p>Los cambios han sido guardados.</p>
          <i class=\"fas fa-check-circle text-success\" style=\"font-size: 35px;\"></i>
          </div>
          <div class=\"modal-footer\">
            <button class=\"btn btn-primary\" type=\"button\" data-dismiss=\"modal\">Aceptar</button>
          </div>
        </div>
      </div>
    </div>";
    header('location: ../finanzas.php');
