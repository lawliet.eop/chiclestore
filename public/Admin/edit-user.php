<?php
@session_start();
include '../conexion/conn.php';
$idUsuario = $_SESSION['id'];
// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

    $rol = $_POST["rol"];
    $nombre = $_POST["name"];
    $apellido = $_POST["lastname"];
    $nuser = $_POST["nameuser"];
    $pswd = $_POST["pswd2"];
    $dni = $_POST["dni"];

    $result10 = mysqli_query($conn,"SELECT `pswd` FROM `usuario` WHERE `idUsuario` = '$idUsuario'");
    $password = mysqli_fetch_assoc($result10)['pswd'];

    //echo $password."<br><br>";
    //echo $pswd;

    //echo $rango."-".$padre."-".$juego."-".$email."-".$pswd."-".$Fnac."-".$vPrin."-".$vSec."-".$nReset."-**".$fReset."**-".$nPSN."-".$precio;
    //buscar id del rol
    switch ($rol) {
      case 'Administrador':
          $idRol = 0;
        break;

      case 'Vendedor':
          $idRol = 1;
        break;

      case 'Creador de Juegos':
          $idRol = 2;
        break;

      default:
         $idRol = 1;
        break;
    }

    if ($pswd == $password) {
      $result = mysqli_query($conn, "UPDATE `usuario` SET `user` = '$nuser', `nomUsuario` = '$nombre', `apellUsuario` = '$apellido', `dniUsuario` = '$dni', `rol_idRol` = '$idRol' WHERE `usuario`.`idUsuario` = '$idUsuario';");
    }else {
      $pswd_cifrada = password_hash($pswd,PASSWORD_DEFAULT,array("cost"=>12));
      $result = mysqli_query($conn, "UPDATE `usuario` SET `user` = '$nuser', `pswd` = '$pswd_cifrada', `nomUsuario` = '$nombre', `apellUsuario` = '$apellido', `dniUsuario` = '$dni', `rol_idRol` = '$idRol' WHERE `usuario`.`idUsuario` = '$idUsuario';");
    }
    //actualizar usuario

      $_SESSION['alert-editar-user'] = "<div class=\"toast float-right\" id=\"toast1\">
      <div class=\"toast-header bg-success text-white\">
        <i class=\"fas fa-check-circle\">&nbsp&nbsp</i>
        <strong class=\"mr-auto\">ChicleStore&nbsp&nbsp</strong>
        <small>1 segundo</small>
        <button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"toast-body\">
      <strong>Los cambios en el usuario se guardaron.</strong>
      </div>
    </div>";


      header('location: usuarios.php');
