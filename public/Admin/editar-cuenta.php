<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }

    include '../conexion/conn.php';

    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $id = $_GET["dato"];

    $_SESSION['id'] = $id;

    $result = mysqli_query($conn, "SELECT * FROM cuenta WHERE idCuenta = '$id'");
    $row = mysqli_fetch_assoc($result);
    $cPadre = $row["cuenta_idPadre"];
    $rango = $row["rango"];
    $email = $row["email"];
    $pswd = $row["pswd"];
    $Fnacimiento = $row["Fnacimiento"];
    $ventaPrincipal = $row["ventaPrincipal"];
    $ventaSecundario = $row["ventaSecundario"];
    $nReseteos = $row["nReseteos"];
    $ultimoReseteo = $row["ultimoReseteo"];
    $nPSN = $row["nPSN"];
    $precio = $row["precio"];
    $idJuego = $row["juego_idJuego"];

    $result1 = mysqli_query($conn, "SELECT * FROM juego WHERE idJuego = '$idJuego'");
    $row2 = mysqli_fetch_assoc($result1);
    $nomJuego = $row2["nombre"];
?>

<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Editar Cuenta</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'cuentas'; include('../includes/navbar1.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

      </div>
      <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 mb-4">Editar Cuenta</h1>
                  </div>
                  <form class="user" action="edit-cta.php" method="post">
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label >Videojuego</label>
                            <?php
                            $result4 = mysqli_query($conn, "SELECT nombre FROM juego WHERE idJuego = '$idJuego'");
                            $row4 = mysqli_fetch_assoc($result4);
                            $juego = $row4["nombre"];
                            echo "<select name=\"game\" class=\"form-control\" id=\"game\">";
                            echo "<option selected>".$juego."</option>";
                            $result5 = mysqli_query($conn, "SELECT * FROM juego WHERE idJuego != '$idJuego' ORDER BY nombre ASC");
                            while ($row5 = mysqli_fetch_assoc($result5)){
                                echo "<option>".$row5['nombre']."</option>";
                            }
                            ?>
                              </select>
                      </div>
                      <div class="col-sm-6">
                        <label>&nbsp</label>
                        <a href="" class="btn btn-success btn-user btn-block" data-toggle="modal" data-target="#newgame">
                          Nuevo Juego
                        </a>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label >Rango</label>
                        <select name="rango" class="form-control" id="rango" onchange="if(this.value=='Menor') {document.getElementById('dpadre').hidden = false} else {document.getElementById('dpadre').hidden = true} " disabled>
                            <?php
                                if ($rango == "Adulto" || $rango == "Adulto "){
                                    echo "<option >Adulto</option>
                          <option value=\"Menor\">Menor</option>";
                                }else if($rango == "Menor"){
                                    echo "<option selected value=\"Menor\">Menor</option>
                          <option>Adulto</option>";
                                }
                            ?>
                        </select>
                      </div>
                      <div class="col-sm-6" id="dpadre" hidden>
                        <label >Padre</label>
                        <select name="padre" class="form-control" >
                            <?php
                            $result2 = mysqli_query($conn, "SELECT email FROM cuenta WHERE idCuenta = '$cPadre'");
                            $row2 = mysqli_fetch_assoc($result2);
                            $emailPadre = $row2["email"];
                            echo "<option>".$emailPadre."</option>";
                            $result3 = mysqli_query($conn, "SELECT email FROM cuenta WHERE idCuenta != '$cPadre' AND rango = 'Adulto'");
                            while ($row3 = mysqli_fetch_assoc($result3)){
                                echo "<option>".$row3['email']."</option>";
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Email</label>
                          <?php
                          echo "<input name=\"email\" type=\"email\" class=\"form-control\" id=\"email\" value=\"$email\" required>";
                          ?>

                      </div>
                      <div class="col-sm-6">
                        <label>Password</label>
                        <?php
                        echo "<input name=\"pswd\" type=\"text\" class=\"form-control\" id=\"pass\" value=\"$pswd\" required>";
                        ?>
                      </div>
                    </div>
                    <hr>
                      <div class="form-group row">
                          <div class="col-sm-6 mb-3 mb-sm-0">
                              <label>Fecha de Nacimiento</label>
                              <?php
                              echo "<input name=\"Fnac\" class=\"form-control\" type=\"date\" id=\"fnac\" value=\"$Fnacimiento\">"
                              ?>
                          </div>
                      </div>
                      <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Ventas en Principal</label>
                        <?php
                        echo "<input name=\"vPrin\" type=\"number\" min=\"0\" class=\"form-control\" id=\"vppal\" value=\"$ventaPrincipal\">"
                        ?>
                      </div>
                      <div class="col-sm-6">
                        <label>Ventas en Secundario</label>
                        <?php
                        echo "<input name=\"vSec\" type=\"number\" min=\"0\" class=\"form-control\" id=\"vsec\" value=\"$ventaSecundario\">"
                        ?>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Reseteos</label>
                        <?php
                        echo "<input name=\"reset\" type=\"number\" min=\"0\" class=\"form-control\" id=\"reset\" value=\"$nReseteos\" onchange=\"if(this.value!=0) {document.getElementById('fureset').disabled = false} else {document.getElementById('fureset').disabled = true}\">"
                        ?>
                      </div>
                      <div class="col-sm-6">
                        <label>Fecha de Ultimo Reseteo</label>
                          <?php
                          echo "<input name=\"Freset\" class=\"form-control\" type=\"date\" id=\"fureset\" value=\"$ultimoReseteo\" disabled>"
                          ?>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <label>ID PSN</label>
                        <?php
                        echo "<input name=\"nPSN\" type=\"text\" class=\"form-control\" id=\"idpsn\" value=\"$nPSN\">"
                        ?>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Precio en Dólares</label>
                        <?php
                        echo "<input name=\"precio\" type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"precio\" value=\"$precio\">";
                        ?>
                      </div>
                    </div>
                    <br>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                          <button type="submit" class="btn btn-primary btn-user btn-block">
                              Guardar Cambios
                          </button>
                      </div>
                      <div class="col-sm-6">
                        <a href="cuentas.php" class="btn btn-danger btn-user btn-block">
                          Cancelar
                        </a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Main Content -->

        <!-- Modal para Nuevo Juego -->
        <div class="modal fade" id="newgame" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Registrar Nuevo Juego</h5>
                    </div>
                    <div class="modal-body text-center">
                        <form class="user" id="formNewGame">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label>Nombre del Juego</label>
                                    <input name="juego" type="text" class="form-control" id="namegame" placeholder="nombre">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="btnCancelar" class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary" >Agregar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>


  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>

  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/ajax/game.js"></script>
  <script src="../js/dark-mode.js"></script>
</body>

</html>
