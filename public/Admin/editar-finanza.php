<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }
    include '../conexion/conn.php';

    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $id = $_GET["dato"];

    $_SESSION['id'] = $id;

    $result = mysqli_query($conn, "SELECT * FROM finanzas WHERE idFinanza = '$id'");
    $row = mysqli_fetch_assoc($result);
    $ingreso = $row["ingresos"];
    $sComp = $row["saldoComprado"];
    $gastPer = $row["gastoPersonal"];
    $gastPub = $row["gastoPublicidad"];
    $gastE = $row["gastoExtra"];
    $Total = $row["total"];
    $dAnt = $row["diaAnterior"];
    $hHoy = $row["hastaHoy"];
    $fechFinan = $row["fechFinan"];

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>ChicleStore - Editar Venta</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'finanzas'; include('../includes/navbar1.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include('../includes/topbar.php')?>

      </div>
      <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Editar Finanza</h1>
                  </div>
                  <form class="user" action="edit-finan.php" method="post">
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <label>Fecha</label>
                        <?php
                        echo "<input name=\"Ffinan\" class=\"form-control\" type=\"date\" id=\"fAten\" value=\"$fechFinan\" disabled>"
                        ?>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-12 mb-3 mb-sm-0">
                        <label>Ingresos por Ventas</label>
                        <?php
                        echo "<input name=\"ipv\" type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"monto\" value=\"$ingreso\" disabled>";
                        ?>
                          </select>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-12 mb-3 mb-sm-0">
                        <label>Gasto en Saldo Comprado</label>
                        <?php
                        echo "<input name=\"gcomp\" type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"monto\" value=\"$sComp\">";
                        ?>
                          </select>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-12 mb-3 mb-sm-0">
                        <label>Gasto en Personal</label>
                        <?php
                        echo "<input name=\"gpers\" type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"monto\" value=\"$gastPer\">";
                        ?>
                          </select>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-12 mb-3 mb-sm-0">
                        <label>Gasto en Publicidad</label>
                        <?php
                        echo "<input name=\"gpub\" type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"monto\" value=\"$gastPub\">";
                        ?>
                          </select>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-12 mb-3 mb-sm-0">
                        <label>Gasto Extra</label>
                        <?php
                        echo "<input name=\"gext\" type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"monto\" value=\"$gastE\">";
                        ?>
                          </select>
                      </div>
                    </div>

                    <br>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Guardar Cambios
                        </button>
                      </div>
                      <div class="col-sm-6">
                        <a href="finanzas.php" class="btn btn-danger btn-user btn-block">
                          Cancelar
                        </a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Main Content -->

      <div class="modal fade" id="newgame" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Registrar Nuevo Juego</h5>
            </div>
            <div class="modal-body text-center">
              <form class="user" action="addJuego.php" method="post">
                <div class="form-group row">
                  <div class="col-sm-4">
                    <label>Nombre del Juego</label>
                    <input name="juego" type="text" class="form-control" id="namegame" placeholder="nombre">
                      <input id="url" name="url" value="n" type="hidden">
                      <script type="text/javascript">
                          document.getElementById("url").value = window.location;
                      </script>
                  </div>
                </div>
                  <div class="modal-footer">
                      <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-primary" >Agregar</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="newmethod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Agregar Nuevo Método de Pago</h5>
            </div>
            <div class="modal-body text-center">
              <form class="user" action="addMetodo.php" method="post">
                <div class="form-group row">
                  <div class="col-sm-4">
                    <label>Nombre del Método de Pago</label>
                    <input name="mpago" type="text" class="form-control" id="namemetodo" placeholder="nombre">
                    <input id="url1" name="url1" value="n" type="hidden">
                    <script type="text/javascript">
                        document.getElementById("url1").value = window.location;
                    </script>
                  </div>
                </div>
                  <div class="modal-footer">
                      <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-primary" >Agregar</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>


      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>

  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>

</body>

</html>
