<?php
    @session_start();
    include '../conexion/conn.php';
    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $cliente = $_POST["idCliente"];
    $forzarborrado = $_POST["forzar"];

    $result = mysqli_query($conn, "SELECT * FROM `cliente` WHERE idCliente = '$cliente'");
    $row = mysqli_fetch_assoc($result);
    $nomCliente = $row["nomCliente"];
    $ctaCliente = $row["cuenta_idCuenta"];
    $fecha = $row["fechAten"];
    $monto = $row["monto"];

    //echo $ctaCliente;

    if ($forzarborrado == 0) {
      if (is_null($ctaCliente)) {
          //se puede eliminar
        mysqli_query($conn, "DELETE FROM `cliente` WHERE `cliente`.`idCliente` = '$cliente'");

        $result4 = mysqli_query($conn, "SELECT * FROM `finanzas` WHERE fechFinan = '$fecha'");
        $row4 = mysqli_fetch_assoc($result4);
        $finanza = $row4["idFinanza"];

          $diaant = $row4["diaAnterior"];
          $result3 = mysqli_query($conn, "SELECT * FROM `finanzas` WHERE idFinanza = '$diaant'");
          $row3 = mysqli_fetch_assoc($result3);
          $totalant = $row3["hastaHoy"];

          $ingresos = $row4["ingresos"];
          $ingresos = $ingresos - $monto;

          $total = $row4["total"] - $monto;
          $totalhoy = $totalant + $total;
          mysqli_query($conn, "UPDATE `finanzas` SET `ingresos` = '$ingresos', `total` = '$total', `hastaHoy` = '$totalhoy' WHERE `finanzas`.`idFinanza` = '$finanza';");

          $respuesta = mysqli_query($conn, "SELECT idFinanza,total,diaAnterior FROM `finanzas` WHERE idFinanza > '$finanza'");
          while ($row5 = mysqli_fetch_assoc($respuesta)){
              $idHoy = $row5['idFinanza'];
              $idAnterior = $row5['diaAnterior'];
              $respuesta1 = mysqli_query($conn, "SELECT hastaHoy FROM `finanzas` WHERE idFinanza = '$idAnterior'");
              $diaAnterior = mysqli_fetch_assoc($respuesta1)['hastaHoy'];
              $nuevoHastaHoy = $row5['total']+$diaAnterior;
              mysqli_query($conn, "UPDATE `finanzas` SET `hastaHoy` = '$nuevoHastaHoy' WHERE `finanzas`.`idFinanza` = '$idHoy'");
          }

          $json = array();
          $json[] = array(
              'resultado'=>'correcto',
              'nombre'=>$nomCliente
          );
          $jsonstring = json_encode($json[0]);
          echo $jsonstring;

      }else {
          //no se puede eliminar xq esta usando una cuenta
          $result1 = mysqli_query($conn, "SELECT * FROM `cuenta` WHERE idCuenta = '$ctaCliente'");
          $row1 = mysqli_fetch_assoc($result1);
          $email = $row1["email"];

          $json = array();
          $json[] = array(
              'resultado'=>'falla',
              'nombre'=>$nomCliente,
              'email'=>$email
          );
          $jsonstring = json_encode($json[0]);
          echo $jsonstring;
      }
    }else {
        //se fuerza la eliminacion
      mysqli_query($conn, "DELETE FROM `cliente` WHERE `cliente`.`idCliente` = '$cliente'");

      $result4 = mysqli_query($conn, "SELECT * FROM `finanzas` WHERE fechFinan = '$fecha'");
      $row4 = mysqli_fetch_assoc($result4);
      $finanza = $row4["idFinanza"];

        $diaant = $row4["diaAnterior"];
        $result3 = mysqli_query($conn, "SELECT * FROM `finanzas` WHERE idFinanza = '$diaant'");
        $row3 = mysqli_fetch_assoc($result3);
        $totalant = $row3["hastaHoy"];

        $ingresos = $row4["ingresos"];
        $ingresos = $ingresos - $monto;

        $total = $row4["total"] - $monto;
        $totalhoy = $totalant + $total;
        mysqli_query($conn, "UPDATE `finanzas` SET `ingresos` = '$ingresos', `total` = '$total', `hastaHoy` = '$totalhoy' WHERE `finanzas`.`idFinanza` = '$finanza';");

        $respuesta = mysqli_query($conn, "SELECT idFinanza,total,diaAnterior FROM `finanzas` WHERE idFinanza > '$finanza'");
        while ($row5 = mysqli_fetch_assoc($respuesta)){
            $idHoy = $row5['idFinanza'];
            $idAnterior = $row5['diaAnterior'];
            $respuesta1 = mysqli_query($conn, "SELECT hastaHoy FROM `finanzas` WHERE idFinanza = '$idAnterior'");
            $diaAnterior = mysqli_fetch_assoc($respuesta1)['hastaHoy'];
            $nuevoHastaHoy = $row5['total']+$diaAnterior;
            mysqli_query($conn, "UPDATE `finanzas` SET `hastaHoy` = '$nuevoHastaHoy' WHERE `finanzas`.`idFinanza` = '$idHoy'");
        }

        $json = array();
        $json[] = array(
            'resultado'=>'correcto',
            'nombre'=>$nomCliente
        );
        $jsonstring = json_encode($json[0]);
        echo $jsonstring;
    }
