<?php
    @session_start();
    include '../conexion/conn.php';
    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $finanza = $_POST["finanedit"];


      $result2 = mysqli_query($conn, "DELETE FROM `finanzas` WHERE `finanzas`.`idFinanza` = '$finanza'");
      $_SESSION['alert-finanza'] = "<div class=\"modal fade\" id=\"error1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
          <div class=\"modal-content\">
            <div class=\"modal-header\">
              <h5 class=\"modal-title\" id=\"exampleModalLabel\">¡Advertencia!</h5>
            </div>
            <div class=\"modal-body text-center\"><p>El balance se eliminó correctamente.</p>
            <i class=\"fas fa-check-circle text-success\" style=\"font-size: 35px;\"></i>
            </div>
            <div class=\"modal-footer\">
              <button class=\"btn btn-primary\" type=\"button\" data-dismiss=\"modal\">Aceptar</button>
            </div>
          </div>
        </div>
      </div>";

    header('location: ../finanzas.php');
