<?php
include '../conexion/conn.php';
// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
//si existe una variable post de compra de saldos
if (isset($_POST['compraSaldos'])){
    $idFinanza = $_POST['idFinanza'];
    $saldoComprado = $_POST['compraSaldos'];
    $result = mysqli_query($conn, "SELECT ingresos,saldoComprado,gastoPersonal,gastoPublicidad,gastoExtra,diaAnterior FROM finanzas WHERE idFinanza = '$idFinanza'");
    $row = mysqli_fetch_assoc($result);
    $ingresos = $row['ingresos'];
    $gastos = $saldoComprado+$row['gastoPersonal']+$row['gastoPublicidad']+$row['gastoExtra'];
    $total = $ingresos-$gastos;

    if (is_null($row['diaAnterior'])){
        $diaAnterior = 0;
    }else{
        $idAnterior = $row['diaAnterior'];
        $result1 = mysqli_query($conn, "SELECT hastaHoy FROM finanzas WHERE idFinanza = '$idAnterior'");
        $diaAnterior = mysqli_fetch_assoc($result1)['hastaHoy'];
    }
    $hastaHoy = $total+$diaAnterior;
    mysqli_query($conn, "UPDATE `finanzas` SET `saldoComprado` = '$saldoComprado', `total` = '$total', `hastaHoy` = '$hastaHoy' WHERE `finanzas`.`idFinanza` = '$idFinanza'");
    $result2 = mysqli_query($conn, "SELECT idFinanza,total,diaAnterior,hastaHoy FROM finanzas WHERE idFinanza > '$idFinanza'");
    while ($row2 = mysqli_fetch_assoc($result2)){
        $idHoy = $row2['idFinanza'];
        $idAyer = $row2['diaAnterior'];
        $result3 = mysqli_query($conn, "SELECT hastaHoy FROM finanzas WHERE idFinanza = '$idAyer'");
        $nuevoHastaHoy = $row2['total']+mysqli_fetch_assoc($result3)['hastaHoy'];
        mysqli_query($conn, "UPDATE `finanzas` SET `hastaHoy` = '$nuevoHastaHoy' WHERE `finanzas`.`idFinanza` = '$idHoy'");
    }
    echo "1 if - ingresos : ".$ingresos." Gastos : ".$gastos;
}
if (isset($_POST['pagoPersonal'])){
    $idFinanza = $_POST['idFinanza'];
    $gastoPersonal = $_POST['pagoPersonal'];
    $result = mysqli_query($conn, "SELECT ingresos,saldoComprado,gastoPersonal,gastoPublicidad,gastoExtra,diaAnterior FROM finanzas WHERE idFinanza = '$idFinanza'");
    $row = mysqli_fetch_assoc($result);
    $ingresos = $row['ingresos'];
    $gastos = $row['saldoComprado']+$gastoPersonal+$row['gastoPublicidad']+$row['gastoExtra'];
    $total = $ingresos-$gastos;
    if (is_null($row['diaAnterior'])){
        $diaAnterior = 0;
    }else{
        $idAnterior = $row['diaAnterior'];
        $result1 = mysqli_query($conn, "SELECT hastaHoy FROM finanzas WHERE idFinanza = '$idAnterior'");
        $diaAnterior = mysqli_fetch_assoc($result1)['hastaHoy'];
    }
    $hastaHoy = $total+$diaAnterior;
    mysqli_query($conn, "UPDATE `finanzas` SET `gastoPersonal` = '$gastoPersonal', `total` = '$total', `hastaHoy` = '$hastaHoy' WHERE `finanzas`.`idFinanza` = '$idFinanza'");
    $result2 = mysqli_query($conn, "SELECT idFinanza,total,diaAnterior,hastaHoy FROM finanzas WHERE idFinanza > '$idFinanza'");
    while ($row2 = mysqli_fetch_assoc($result2)){
        $idHoy = $row2['idFinanza'];
        $idAyer = $row2['diaAnterior'];
        $result3 = mysqli_query($conn, "SELECT hastaHoy FROM finanzas WHERE idFinanza = '$idAyer'");
        $nuevoHastaHoy = $row2['total']+mysqli_fetch_assoc($result3)['hastaHoy'];
        mysqli_query($conn, "UPDATE `finanzas` SET `hastaHoy` = '$nuevoHastaHoy' WHERE `finanzas`.`idFinanza` = '$idHoy'");
    }
    echo "2 if - ingresos : ".$ingresos." Gastos : ".$gastos;
}
if (isset($_POST['publicidad'])){
    $idFinanza = $_POST['idFinanza'];
    $gastoPublicidad = $_POST['publicidad'];
    $result = mysqli_query($conn, "SELECT ingresos,saldoComprado,gastoPersonal,gastoPublicidad,gastoExtra,diaAnterior FROM finanzas WHERE idFinanza = '$idFinanza'");
    $row = mysqli_fetch_assoc($result);
    $ingresos = $row['ingresos'];
    $gastos = $row['saldoComprado']+$row['gastoPersonal']+$gastoPublicidad+$row['gastoExtra'];
    $total = $ingresos-$gastos;
    if (is_null($row['diaAnterior'])){
        $diaAnterior = 0;
    }else{
        $idAnterior = $row['diaAnterior'];
        $result1 = mysqli_query($conn, "SELECT hastaHoy FROM finanzas WHERE idFinanza = '$idAnterior'");
        $diaAnterior = mysqli_fetch_assoc($result1)['hastaHoy'];
    }
    $hastaHoy = $total+$diaAnterior;
    mysqli_query($conn, "UPDATE `finanzas` SET `gastoPublicidad` = '$gastoPublicidad', `total` = '$total', `hastaHoy` = '$hastaHoy' WHERE `finanzas`.`idFinanza` = '$idFinanza'");
    $result2 = mysqli_query($conn, "SELECT idFinanza,total,diaAnterior,hastaHoy FROM finanzas WHERE idFinanza > '$idFinanza'");
    while ($row2 = mysqli_fetch_assoc($result2)){
        $idHoy = $row2['idFinanza'];
        $idAyer = $row2['diaAnterior'];
        $result3 = mysqli_query($conn, "SELECT hastaHoy FROM finanzas WHERE idFinanza = '$idAyer'");
        $nuevoHastaHoy = $row2['total']+mysqli_fetch_assoc($result3)['hastaHoy'];
        mysqli_query($conn, "UPDATE `finanzas` SET `hastaHoy` = '$nuevoHastaHoy' WHERE `finanzas`.`idFinanza` = '$idHoy'");
    }
    echo "3 if - ingresos : ".$ingresos." Gastos : ".$gastos;
}
if (isset($_POST['pagoExtra'])){
    $idFinanza = $_POST['idFinanza'];
    $gastoExtra = $_POST['pagoExtra'];
    $result = mysqli_query($conn, "SELECT ingresos,saldoComprado,gastoPersonal,gastoPublicidad,gastoExtra,diaAnterior FROM finanzas WHERE idFinanza = '$idFinanza'");
    $row = mysqli_fetch_assoc($result);
    $ingresos = $row['ingresos'];
    $gastos = $row['saldoComprado']+$row['gastoPersonal']+$row['gastoPublicidad']+$gastoExtra;
    $total = $ingresos-$gastos;
    if (is_null($row['diaAnterior'])){
        $diaAnterior = 0;
    }else{
        $idAnterior = $row['diaAnterior'];
        $result1 = mysqli_query($conn, "SELECT hastaHoy FROM finanzas WHERE idFinanza = '$idAnterior'");
        $diaAnterior = mysqli_fetch_assoc($result1)['hastaHoy'];
    }
    $hastaHoy = $total+$diaAnterior;
    mysqli_query($conn, "UPDATE `finanzas` SET `gastoExtra` = '$gastoExtra', `total` = '$total', `hastaHoy` = '$hastaHoy' WHERE `finanzas`.`idFinanza` = '$idFinanza'");
    $result2 = mysqli_query($conn, "SELECT idFinanza,total,diaAnterior,hastaHoy FROM finanzas WHERE idFinanza > '$idFinanza'");
    while ($row2 = mysqli_fetch_assoc($result2)){
        $idHoy = $row2['idFinanza'];
        $idAyer = $row2['diaAnterior'];
        $result3 = mysqli_query($conn, "SELECT hastaHoy FROM finanzas WHERE idFinanza = '$idAyer'");
        $nuevoHastaHoy = $row2['total']+mysqli_fetch_assoc($result3)['hastaHoy'];
        mysqli_query($conn, "UPDATE `finanzas` SET `hastaHoy` = '$nuevoHastaHoy' WHERE `finanzas`.`idFinanza` = '$idHoy'");
    }
    echo "4 if - ingresos : ".$ingresos." Gastos : ".$gastos;
}
if (isset($_GET['idListar'])){
    $id = $_GET['idListar'];
    $bool = true;
    $json = array();
    while ($bool){
        $result = mysqli_query($conn,"SELECT idFinanza,total,diaAnterior,hastaHoy FROM finanzas WHERE idFinanza = '$id' ORDER BY fechFinan DESC");
        if (mysqli_num_rows($result) != 0){
            $row = mysqli_fetch_assoc($result);
            if (is_null($row['diaAnterior'])){
                $TotalAyer = 0;
            }else{
                $hastaAyer = $row['diaAnterior'];
                $result1 = mysqli_query($conn,"SELECT hastaHoy FROM finanzas WHERE idFinanza = '$hastaAyer'");
                $TotalAyer = mysqli_fetch_assoc($result1)['hastaHoy'];
            }
            $json[] = array(
                'id'=>$row['idFinanza'],
                'total'=>$row['total'],
                'TotalAyer'=>$TotalAyer,
                'HastaHoy'=>$row['hastaHoy']
            );
            $id++;
        }else{
            $bool = false;
        }
    }
    $jsonstring = json_encode($json);
    echo $jsonstring;
}
