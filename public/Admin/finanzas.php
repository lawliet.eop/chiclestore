<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }

include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Finanzas</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'finanzas'; include('../includes/navbar1.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

        <!-- Begin Page Content -->
        <div class="container-fluid" id="mi-tabla">

          <!-- Page Heading -->
          <h1 class="h3 mb-2"><strong>Finanzas</strong></h1>
          <p class="mb-4">Balance de ingresos y gastos diarios.</p>

          <!-- DataTales Example -->
          <div id="cardshadow" class="card shadow mb-4" >
            <div id="cardheader" class="card-header bg-dark py-3">
            </br>
            </div>
            <div id="cardbody" class="card-body">
              <div class="table-responsive">
                <table id="dataTable" class="table" width="100%" cellspacing="0">
                  <thead>
                    <tr id="cabecera" class="bg-dark text-white">
                      <th style="display:none;"> NO DEBERIAS VERME</th>
                      <th class="text-center" style="border: none;">Fecha de Atención</th>
                      <th class="text-center" style="border: none;">Ingreso por ventas</th>
                      <th class="text-center" style="border: none;">Compra de saldos</th>
                      <th class="text-center" style="border: none;">Pago de personal</th>
                      <th class="text-center" style="border: none;">Publicidad</th>
                      <th class="text-center" style="border: none;">Pagos&nbspextras</th>
                      <th class="text-center" style="border: none;">Total</th>
                      <th class="text-center" style="border: none;">Total Ayer</th>
                      <th class="text-center" style="border: none;">Hasta Hoy</th>
                    </tr>
                  </thead>
                  <tbody id="tabla">
                    <?php

                    $cont=1;
                    $result = mysqli_query($conn, "SELECT * FROM finanzas ORDER BY fechFinan DESC ");
                    while ($row = mysqli_fetch_assoc($result)){
                        $dato = $row["idFinanza"];
                        $dant = $row["diaAnterior"];
                        $scomp = $row["saldoComprado"];
                        $gper = $row["gastoPersonal"];
                        $gpub = $row["gastoPublicidad"];
                        $gext = $row["gastoExtra"];

                        echo "<tr  name=\"contenido\">";
                        echo "<td style=\"display:none;\"></td>";
                        $fechab = $row["fechFinan"];

                        echo "<td class=\"text-center almanaque\"><strong>".fechaCastellano($fechab)."</strong></td>";
                        echo "<td class=\"text-center align-middle\"><strong>".$row["ingresos"]." S/.</strong></td>";
                        echo "<td class=\"text-center align-middle\"><div class=\"input-group mb-3\"><input atributo='$dato' type=\"text\" class=\"compra-saldos form-control\" value=\"$scomp\">
                        <div class=\"input-group-append\"><span class=\"input-group-text\">S/.</span></div></div></td>";
                        echo "<td class=\"text-center align-middle\"><div class=\"input-group mb-3\"><input atributo='$dato' type=\"text\" class=\" pago-personal form-control\" value=\"$gper\">
                        <div class=\"input-group-append\"><span class=\"input-group-text\">S/.</span></div></div></td>";
                        echo "<td class=\"text-center align-middle\"><div class=\"input-group mb-3\"><input atributo='$dato' type=\"text\" class=\"publicidad form-control\" value=\"$gpub\">
                        <div class=\"input-group-append\"><span class=\"input-group-text\">S/.</span></div></div></td>";
                        echo "<td class=\"text-center align-middle\"><div class=\"input-group mb-3\"><input atributo='$dato' type=\"text\" class=\"publicidad form-control\" value=\"$gext\">
                        <div class=\"input-group-append\"><span class=\"input-group-text\">S/.</span></div></div></td>";

                        if ($row["total"] >= 0) {
                          echo "<td id=\"total$dato\" bgcolor= \"#2ECC71\" style=\"color: white;\" class=\"text-center align-middle\"><strong>".$row["total"]."&nbspS/.</strong></td>";
                        }else {
                          echo "<td id=\"total$dato\" bgcolor= \"#E74C3C\" style=\"color: white;\" class=\"text-center align-middle\"><strong>".$row["total"]."&nbspS/.</strong></td>";
                        }
                        if (is_null($dant)){
                            $totalayer = 0;
                        }else{
                            $result1 = mysqli_query($conn, "SELECT * FROM `finanzas` WHERE idFinanza = '$dant'");
                            $row1 = mysqli_fetch_assoc($result1);
                            $totalayer = $row1["hastaHoy"];
                        }
                        echo "<td id=\"totalAyer$dato\" class=\"text-center align-middle\"><strong>".$totalayer."&nbspS/.</strong></td>";
                        if ($row["hastaHoy"] >= 0) {
                          echo "<td id=\"hastaHoy$dato\" bgcolor= \"#2ECC71\" style=\"color: white;\" class=\"text-center align-middle\"><strong>".$row["hastaHoy"]."&nbspS/.</strong></td>";
                        }else {
                          echo "<td id=\"hastaHoy$dato\" bgcolor= \"#E74C3C\" style=\"color: white;\" class=\"text-center align-middle\"><strong>".$row["hastaHoy"]."&nbspS/.</strong></td>";
                        }
                    $cont++;}
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <?php
          if (isset($_SESSION['alert-edit-finan'])){
            if ($_SESSION['alert-edit-finan'] != " ") {

              echo $_SESSION['alert-edit-finan'];
              $_SESSION['alert-edit-finan']= " ";
            }
          }else{
          }
          ?>
          <?php
          if (isset($_SESSION['alert-finanza'])){
            if ($_SESSION['alert-finanza'] != " ") {

              echo $_SESSION['alert-finanza'];
              $_SESSION['alert-finanza']= " ";
            }
          }else{
          }
          ?>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
        <?php
        function fechaCastellano ($fecha) {
            $fecha = substr($fecha, 0, 10);
            $numeroDia = date('d', strtotime($fecha));
            $dia = date('l', strtotime($fecha));
            $mes = date('F', strtotime($fecha));
            $anio = date('Y', strtotime($fecha));
            $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
            $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
            $nombredia = str_replace($dias_EN, $dias_ES, $dia);
            $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
            return $nombredia."<br><h1>".$numeroDia."</h1>".$nombreMes." ".$anio;
        }
        ?>
      <!-- Footer -->

    <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <script>
     $(document).ready(function()
            {
                $("#error1").modal("show");
            })
  </script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/ajax/finanzas.js"></script>
  <script src="../js/dark-mode.js"></script>
</body>

</html>
