<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }

include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

date_default_timezone_set('america/lima');

$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;
?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Registrar Venta</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'ventas'; include('../includes/navbar1.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

      </div>
      <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 mb-4">Registrar Nueva Venta</h1>
                  </div>
                  <form class="user" action="registrar-clte.php" method="post">
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <label>Fecha de Atención</label>
                        <input name="Faten" class="form-control" type="date" id="faten" value="<?php echo $today; ?>" disabled>
                      </div>
                      <div class="col-sm-6">
                        <label>Tipo de Venta</label>
                        <select name="tipoventa" class="form-control" id="tipoventa"> <!-- imprimir los tipos de venta -->
                          <option>Principal</option>";
                          <option>Secundaria</option>";
                        </select>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Nombre del Cliente</label>
                        <input name="nomClte" type="text" class="form-control" id="name" placeholder="Nombre" required>
                      </div>
                      <div class="col-sm-6">
                        <label>URL del chat</label>
                        <input name="urlChat" type="text" class="form-control" id="urlchat" placeholder="URL">
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label >Producto (Juego)</label>
                        <select name="game" class="form-control" id="game"> <!-- imprimir los videojuegos -->
                            <?php
                            $result1 = mysqli_query($conn, "SELECT * FROM juego WHERE nombre != 'Sin Juego' ORDER BY nombre ASC");
                                echo "<option>Sin Juego</option>";
                            while ($row1 = mysqli_fetch_assoc($result1)){
                                echo "<option>".$row1["nombre"]."</option>";
                            }
                            ?>
                        </select>
                      </div>
                      <div class="col-sm-6">
                        <label>&nbsp</label>
                        <a href="" class="btn btn-success btn-user btn-block" data-toggle="modal" data-target="#newgame">
                          Nuevo Juego
                        </a>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label >Método de Pago</label>
                        <select name="metodp" class="form-control" id="metodp"> <!-- imprimir los metodos de pago -->
                            <?php
                            $result2 = mysqli_query($conn, "SELECT nombreMP FROM `metpagos`");
                            while ($row2 = mysqli_fetch_assoc($result2)){
                                echo "<option>".$row2["nombreMP"]."</option>";
                            }
                            ?>
                        </select>
                      </div>
                      <div class="col-sm-6">
                        <label>&nbsp</label>
                        <a href="" class="btn btn-warning btn-user btn-block" data-toggle="modal" data-target="#newmethod">
                          Nuevo Método de Pago
                        </a>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Monto</label>
                        <input name="monto" type="number" min="0" class="form-control" id="monto" value="0">
                      </div>
                      <div class="col-sm-6">
                        <label>Fecha de Deposito</label>
                        <input name="Fdepo" class="form-control" type="date" id="fdepo" value="<?php echo $today; ?>">
                      </div>
                    </div>
                    <br>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Registrar Cliente
                        </button>
                      </div>
                      <div class="col-sm-6">
                        <a href="clientes.php" class="btn btn-danger btn-user btn-block">
                          Cancelar
                        </a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Main Content -->

        <!-- Modal para Nuevo Juego -->
        <div class="modal fade" id="newgame" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Registrar Nuevo Juego</h5>
                    </div>
                    <div class="modal-body text-center">
                        <form class="user" id="formNewGame">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label>Nombre del Juego</label>
                                    <input name="juego" type="text" class="form-control" id="namegame" placeholder="nombre">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="btnCancelar" class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary" >Agregar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

      <div class="modal fade" id="newmethod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Agregar Nuevo Método de Pago</h5>
            </div>
            <div class="modal-body text-center">
              <form class="user" action="addMetodo.php" method="post">
                <div class="form-group row">
                  <div class="col-sm-12">
                    <label>Nombre del Método de Pago</label>
                    <input name="mpago" type="text" class="form-control" id="namemetodo" placeholder="nombre">
                    <input id="url1" name="url1" value="n" type="hidden">
                    <script type="text/javascript">
                        document.getElementById("url1").value = window.location;
                    </script>
                  </div>
                </div>
                  <div class="modal-footer">
                      <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-primary" >Agregar</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>


      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>

  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/ajax/game.js"></script>
  <script src="../js/dark-mode.js"></script>
</body>

</html>
