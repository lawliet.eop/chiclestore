<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }
  include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Registrar Cuenta</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'cuentas'; include('../includes/navbar1.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

      </div>
      <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 mb-4">Registrar Nueva Cuenta</h1>
                  </div>
                  <form class="user" action="registrar-cta.php" method="post">
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0" >
                        <label >Rango</label>
                        <select name="rango" class="form-control" id="rango" onchange="if(this.value=='Menor') {document.getElementById('dpadre').hidden = false} else {document.getElementById('dpadre').hidden = true} ">
                          <option>Adulto</option>
                          <option value="Menor">Menor</option>
                        </select>
                      </div>
                      <div class="col-sm-6" id="dpadre" hidden>
                        <label >Padre</label>
                        <select name="padre" class="form-control" >
                            <?php
                            $result = mysqli_query($conn, "SELECT email FROM cuenta WHERE cuenta_idPadre IS NULL");
                            while ($row = mysqli_fetch_assoc($result)){
                                echo "<option>".$row["email"]."</option>";
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Email</label>
                        <input name="email" type="email" class="form-control" id="email" placeholder="name@example.com" required>
                      </div>
                      <div class="col-sm-6">
                        <label>Password</label>
                        <input name="pswd" type="text" class="form-control" id="pass" placeholder="password" required>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label >Videojuego</label>
                          <select name="game" class="form-control" id="game">   <!-- imprimir los videojuegos -->
                              <?php
                              $result1 = mysqli_query($conn, "SELECT * FROM juego WHERE nombre != 'Sin Juego' ORDER BY nombre ASC");
                              if (isset($_POST['game'])){
                                  echo "<option>".$_POST['game']."</option>";
                              }else{
                                  echo "<option>Sin Juego</option>";
                              }
                              while ($row1 = mysqli_fetch_assoc($result1)){
                                  echo "<option>".$row1["nombre"]."</option>";
                              }
                              ?>
                          </select>
                      </div>
                      <div class="col-sm-6">
                        <label>&nbsp</label>
                        <a href="" class="btn btn-success btn-user btn-block" data-toggle="modal" data-target="#newgame">
                          Nuevo Juego
                        </a>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Fecha de Nacimiento</label>
                        <input name="Fnac" class="form-control" type="date" id="fnac" value="2005-01-01">
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Ventas en Principal</label>
                        <input name="vPrin" type="number" min="0" class="form-control" id="vppal" value="0">
                      </div>
                      <div class="col-sm-6">
                        <label>Ventas en Secundario</label>
                        <input name="vSec" type="number" min="0" class="form-control" id="vsec" value="0">
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Reseteos</label>
                        <input name="reset" type="number" min="0" class="form-control" id="reset" value="0" onchange="if(this.value!=0) {document.getElementById('fureset').disabled = false} else {document.getElementById('fureset').disabled = true}">
                      </div>
                      <div class="col-sm-6">
                        <label>Fecha de Ultimo Reseteo</label>
                        <input name="Freset" class="form-control" type="date" id="fureset" disabled>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <label>ID PSN</label>
                        <input name="nPSN" type="text" class="form-control" id="idpsn" placeholder="ID">
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Precio en Dólares</label>
                        <input name="precio" type="number" min="0" class="form-control" id="precio" value="0">
                      </div>
                    </div>
                    <br>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                          <button type="submit" class="btn btn-primary btn-user btn-block">
                              Registrar Cuenta
                          </button>
                      </div>
                      <div class="col-sm-6">
                        <a href="cuentas.php" class="btn btn-danger btn-user btn-block">
                          Cancelar
                        </a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- End of Main Content -->

        <!-- Modal para Nuevo Juego -->
      <div class="modal fade" id="newgame" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Registrar Nuevo Juego</h5>
            </div>
            <div class="modal-body text-center">
              <form class="user" id="formNewGame">
                <div class="form-group row">
                  <div class="col-sm-12">
                    <label>Nombre del Juego</label>
                    <input name="juego" type="text" class="form-control" id="namegame" placeholder="nombre">
                  </div>
                </div>
                  <div class="modal-footer">
                      <button id="btnCancelar" class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-primary" >Agregar</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>


      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>


  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>

  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->

  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/ajax/game.js"></script>
  <script src="../js/dark-mode.js"></script>
</body>

</html>
