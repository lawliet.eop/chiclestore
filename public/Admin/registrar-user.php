<?php
    @session_start();
    include '../conexion/conn.php';

    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

        $rol = $_POST["rol"];
        $nombre = $_POST["name"];
        $apellido = $_POST["lastname"];
        $nuser = $_POST["nameuser"];
        $pswd = $_POST["pswd2"];
        $dni = $_POST["dni"];

        $pswd_cifrada = password_hash($pswd,PASSWORD_DEFAULT,array("cost"=>12));

        //echo $rango."-".$padre."-".$juego."-".$email."-".$pswd."-".$Fnac."-".$vPrin."-".$vSec."-".$nReset."-**".$fReset."**-".$nPSN."-".$precio;
        //buscar id del rol
        switch ($rol) {
          case 'Administrador':
              $idRol = 0;
            break;

          case 'Vendedor':
              $idRol = 1;
            break;

          case 'Creador de Juegos':
              $idRol = 2;
            break;

          default:
             $idRol = 1;
            break;
        }

        //buscar nombre de usuario
        $result2 = mysqli_query($conn, "SELECT * FROM `usuario` WHERE user = '$nuser'");
        if (mysqli_num_rows($result2) == 0) {
          //insert usuario
          $result = mysqli_query($conn, "INSERT INTO `usuario` (`idUsuario`, `user`, `pswd`, `nomUsuario`, `apellUsuario`, `dniUsuario`, `rol_idRol`) VALUES (NULL, '$nuser', '$pswd_cifrada', '$nombre', '$apellido', '$dni', '$idRol')");

          $_SESSION['alert-registro-user'] = "<div class=\"toast float-right\" id=\"toast1\">
          <div class=\"toast-header bg-success text-white\">
            <i class=\"fas fa-check-circle\">&nbsp&nbsp</i>
            <strong class=\"mr-auto\">¡Hecho!&nbsp&nbsp</strong>
            <small>1 segundo</small>
            <button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"toast-body\">
          Usuario <strong>".$nuser."</strong> registrado correctamente.
          </div>
        </div>";
        }else {
          $_SESSION['alert-registro-user'] = "<div class=\"toast float-right\" id=\"toast1\">
          <div class=\"toast-header bg-danger text-white\">
            <i class=\"fas fa-times-circle\">&nbsp&nbsp</i>
            <strong class=\"mr-auto\">¡Error!&nbsp&nbsp</strong>
            <small>1 segundo</small>
            <button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"toast-body\">
          <strong>Nombre de usuario ya registrado.</strong>
          </div>
        </div>";
        }
          header('location: usuarios.php');
