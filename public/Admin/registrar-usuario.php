<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }
  include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Registrar Cuenta</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'cuentas'; include('../includes/navbar1.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

      </div>
      <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 mb-4"><strong>Registrar Nuevo Usuario</strong></h1>
                  </div>
                  <form class="user" action="registrar-user.php" method="post">
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label><strong>Nombres</strong></label>
                        <input name="name" type="text" class="form-control" id="name" placeholder="Nombres">
                      </div>
                      <div class="col-sm-6">
                        <label><strong>Apellidos</strong></label>
                        <input name="lastname" type="text" class="form-control" id="lastname" placeholder="Apellidos">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label><strong>DNI</strong></label>
                        <input name="dni" type="text"  maxlength="8" class="form-control" id="dni" placeholder="DNI">
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label><strong>Nombre de Usuario</strong></label>
                        <input name="nameuser" type="text" class="form-control" id="nameuser" placeholder="Nombre de Usuario" required>
                      </div>
                      <div class="col-sm-6">
                        <label><strong>Contraseña</strong></label>
                        <div class="input-group">
                          <input name="pswd" type="password" class="form-control" id="pswd" placeholder="Contraseña" required>
                          <div class="input-group-append">
                            <a id="revel1" onclick="revelpass1()" class="btn btn-light input-group-text"><i id="ojito1" class="far fa-eye"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">

                      </div>
                      <div class="col-sm-6">
                        <label><strong>Confirmar Contraseña</strong></label><label id="alert" style="color:red"></label>
                        <div class="input-group">
                          <input name="pswd2" type="password" class="form-control" id="pswd2" placeholder="Repita la contraseña" required>
                          <div class="input-group-append">
                            <a id="revel2" onclick="revelpass2()" class="btn btn-light input-group-text"><i id="ojito2" class="far fa-eye"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label><strong>Rol de Usuario</strong></label>
                          <select name="rol" class="form-control" id="rol">   <!-- imprimir los roles -->
                              <option>Vendedor</option>";
                              <option>Creador de Juegos</option>";
                              <option>Administrador</option>";
                          </select>
                      </div>
                      <!--<div class="col-sm-6">
                        <label>&nbsp</label>
                        <a href="" class="btn btn-success btn-user btn-block" data-toggle="modal" data-target="#newgame">
                          Nuevo Rol
                        </a>
                      </div>-->
                    </div>
                    <br>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                          <input type="submit" class="btn btn-primary btn-user btn-block" value="Registrar Usuario">
                      </div>
                      <div class="col-sm-6">
                        <a href="usuarios.php" class="btn btn-danger btn-user btn-block">
                          Cancelar
                        </a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- End of Main Content -->

        <!-- Modal para Nuevo Juego -->
      <div class="modal fade" id="newgame" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Registrar Nuevo Juego</h5>
            </div>
            <div class="modal-body text-center">
              <form class="user" id="formNewGame">
                <div class="form-group row">
                  <div class="col-sm-4">
                    <label>Nombre del Juego</label>
                    <input name="juego" type="text" class="form-control" id="namegame" placeholder="nombre">
                  </div>
                </div>
                  <div class="modal-footer">
                      <button id="btnCancelar" class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-primary" >Agregar</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>


      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>


  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>

  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->

  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/ajax/game.js"></script>
  <script src="../js/dark-mode.js"></script>
  <script src="../js/users-pass.js"></script>

</body>

</html>
