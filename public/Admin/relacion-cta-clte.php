<?php
    include '../conexion/conn.php';
    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $idCliente = $_POST["idCliente"];
    $emailcta = $_POST["emailCuenta"];
    $tipoventa = $_POST["tipoVenta"];


    //echo "llegaron estos datos: CUENTA= ".$emailcta." CLIENTE=".$idCliente;

    //buscar el cliente
    $result = mysqli_query($conn, "SELECT * FROM `cliente` WHERE idCliente = '$idCliente'");
    $row = mysqli_fetch_assoc($result);
    $monto = $row["monto"];
    $nombre_cliente = $row['nomCliente'];
    $fecha_atencion = $row['fechAten'];

    //buscar la cuenta
    $result1 = mysqli_query($conn, "SELECT * FROM `cuenta` WHERE email = '$emailcta'");
    $row1 = mysqli_fetch_assoc($result1);
    $idCuenta = $row1["idCuenta"];
    $vp = $row1["ventaPrincipal"];
    $vs = $row1["ventaSecundario"];
    $reset = $row1["nReseteos"];
    $vp++;


    if ($reset != 0) {
      if ($tipoventa == 0) {
        $result2 = mysqli_query($conn, "UPDATE `cuenta` SET `estadoCuenta` = 4 , `ventaPrincipal` = '$vp' WHERE `idCuenta` = '$idCuenta';");
        //actualizar cliente
        $result2 = mysqli_query($conn, "UPDATE `cliente` SET `cuenta_idCuenta` = '$idCuenta', `estCliente` = 'ATENDIDO', `cuenta_Comprada` = '$idCuenta' WHERE `idCliente` = '$idCliente';");
        //pasa a venta finalizada
          $json = array();
          $json[0] = array(
              'estado_cuenta'=>'VENTA FINALIZADA',
              'fecha_atencion'=>$fecha_atencion,
              'nombre_cliente'=>$nombre_cliente,
              'email_cuenta'=>$emailcta
          );
          $jsonstring = json_encode($json[0]);
          echo $jsonstring;
      }else {
        $vs++;
        $result2 = mysqli_query($conn, "UPDATE `cuenta` SET `ventaSecundario` = '$vs' WHERE `idCuenta` = '$idCuenta';");
          //actualizar cliente
        $result2 = mysqli_query($conn, "UPDATE `cliente` SET  `estCliente` = 'ATENDIDO', `cuenta_Comprada` = '$idCuenta' WHERE `idCliente` = '$idCliente';");
        //pasa a venta finalizada
          $json = array();
          $json[0] = array(
              'estado_cuenta'=>'VENTA FINALIZADA',
              'fecha_atencion'=>$fecha_atencion,
              'nombre_cliente'=>$nombre_cliente,
              'email_cuenta'=>$emailcta
          );
          $jsonstring = json_encode($json[0]);
          echo $jsonstring;
      }
      //`cuenta_idCuenta` = '$idCuenta', esto se quito para que la cuenta con reseteo no se asocie a un cliente y se pueda volver a vender
    }else {
      $result2 = mysqli_query($conn, "UPDATE `cuenta` SET `estadoCuenta` = '2', `ventaPrincipal` = '$vp', `precioVendido` = '$monto' WHERE `idCuenta` = '$idCuenta';");
        //actualizar cliente
      $result2 = mysqli_query($conn, "UPDATE `cliente` SET `cuenta_idCuenta` = '$idCuenta', `estCliente` = 'ATENDIDO', `cuenta_Comprada` = '$idCuenta' WHERE `idCliente` = '$idCliente';");
      //esperando descarga
        $json = array();
        $json[0] = array(
            'estado_cuenta'=>'ESPERANDO DESCARGA',
            'fecha_atencion'=>$fecha_atencion,
            'nombre_cliente'=>$nombre_cliente,
            'email_cuenta'=>$emailcta
        );
        $jsonstring = json_encode($json[0]);
        echo $jsonstring;

    }
