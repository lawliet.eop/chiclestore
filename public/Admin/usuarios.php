<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }

include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Usuarios</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <link href="../css/jquery-ui.custom.min.css" rel="stylesheet">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery-ui.custom.min.js"></script>
  <script src="../js/demo/bililiteRange.js"></script>
  <script src="../js/demo/jquery.simulate.js"></script>
  <script src="../js/demo/jquery.simulate.key-sequence.js"></script>
  <script src="../js/demo/jquery.simulate.key-combo.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $eventLog = $('#eventLog'); // Global variable
      $eventLog.indentation = "";
      $eventLog.incIndentation = function() {
        $eventLog.indentation += "    ";
      }
      $eventLog.decIndentation = function() {
        if ($eventLog.indentation.length <= 4)
          $eventLog.indentation = " ";
        else
          $eventLog.indentation = $eventLog.indentation.substr(-4);
      }
      $eventLog.append = function(str, indent) {
        if (indent === true || indent === undefined)
          $eventLog.val($eventLog.val()+$eventLog.indentation+str+"\n");
        else
          $eventLog.val($eventLog.val()+str+"\n");
      }
      logMouseMove = false; // Global variable

      function logButtonEvent(event) {
        var indentLength = 10-event.type.length;
        var indent = "";
        for (var i=0; i < indentLength; i+=1) {
          indent += " ";
        }
        var modifiers = ['shift', 'alt', 'ctrl', 'meta'];
        var activeMods = []
        for (var mod in modifiers) {
          if (event[modifiers[mod]+"Key"]) {
            activeMods.push(modifiers[mod]);
          }
        }
        var activeModStr = "";
        if (activeMods.length > 0) {
          activeModStr = ", modifiers: " + activeMods.join("+");
        }

        $eventLog.append(event.type+indent+"(which: "+event.which/*+", target: "+event.target*//*+", keyCode: "+event.keyCode*//*+", charCode: "+event.charCode*/+activeModStr+")");
      }

      initDemo();
    });
  </script>

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'usuarios'; include('../includes/navbar1.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

        <!-- Begin Page Content -->
        <div class="container-fluid" id="mi-tabla">

          <!-- Page Heading -->
          <h1 class="h3 mb-2">
            <strong>Usuarios</strong>
          <div class="toast float-right" id="eliminado" style="display:none">
          <div class="toast-header bg-success text-white">
            <i class="fas fa-check-circle">&nbsp&nbsp</i>
            <strong class="mr-auto">ChicleStore&nbsp&nbsp</strong>
            <small>1 segundo</small>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="toast-body">
          <strong>El usuario se eliminó correctamente.</strong>
          </div>
        </div>
          <?php
          if (isset($_SESSION['alert-registro-user'])){
            if ($_SESSION['alert-registro-user'] != " ") {

              echo $_SESSION['alert-registro-user'];
              $_SESSION['alert-registro-user']= " ";
            }
          }else{
          }


          if (isset($_SESSION['alert-editar-user'])){
            if ($_SESSION['alert-editar-user'] != " ") {
              echo $_SESSION['alert-editar-user'];
              $_SESSION['alert-editar-user']= " ";
            }
          }else{
          }
          ?>
        </h1>
          <p class="mb-4">Tabla de datos de los usuarios existentes.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4" >
            <div  class="card-header bg-dark py-3">
              <a href="registrar-usuario.php" class="btn btn-primary btn-icon-split">
                <span class="text">Agregar Usuario</span>
              </a>
              <div class="float-right text-white">
                Busqueda por:&nbsp&nbsp
                <div class="btn-group" role="group">
                <a href="#" class="btn btn-outline-success" id="combo-admin">
                    <strong>Administrador</strong>
                </a>
                <a href="#" class="btn btn-outline-warning" id="combo-vend">
                    <strong>Vendedor</strong>
                </a>
                <a href="#" class="btn btn-outline-light" id="combo-creaj">
                    <strong>Creador de Juegos</strong>
                </a>
              </div>
              </div>
            </div>

            <div class="card-body">
              <div class="table-responsive rounded">
                <table id="dataTable" class="table table-striped table-light"  width="100%" cellspacing="0">
                  <thead>
                    <tr class="bg-dark text-white">
                      <th style="display:none;"> NO DEBERIAS VERME</th>
                      <th class="text-center" style="border: none;">Rol</th>
                      <th class="text-center" style="border: none;">DNI</th>
                      <th class="text-center" style="border: none;">Nombres</th>
                      <th class="text-center" style="border: none;">Apellidos</th>
                      <th class="text-center" style="border: none;">Nombre de Usuario</th>
                      <th class="text-center" style="border: none;">Editar</th>
                      <th class="text-center" style="border: none;">Borrar</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  $cont = 1;
                  $result = mysqli_query($conn, "SELECT * FROM roles R INNER JOIN usuario U ON R.idRol = U.rol_idRol ORDER BY idUsuario DESC");
                  while ($row = mysqli_fetch_assoc($result)){
                      $dato = $row["idUsuario"];

                      echo "<tr id='fila$dato'>";
                      echo "<td style=\"display:none;\"></td>";
                      echo "<td class=\"text-center\">".$row["nombreRol"]."</td>";
                      echo "<td class=\"text-center\">".$row["dniUsuario"]."</td>";
                      echo "<td class=\"text-center\">".$row["nomUsuario"]."</td>";
                      echo "<td class=\"text-center\">".$row["apellUsuario"]."</td>";
                      echo "<td class=\"text-center\">".$row["user"]."</td>";
                      echo "<td class=\"text-center\"><a class=\"nav-link text-secondary\" href=\"editar-usuario.php?dato=$dato\">
                            <i class=\"fas fa-edit \"></i></a></td>";
                      echo "<td class=\"text-center\">
                                <input name=\"useredit\" value=\"$dato\" type='hidden'>
                                <a href='#' class=\"nav-link text-danger\" id=\"submit$dato\" data-toggle=\"modal\" data-target=\"#confirmar$dato\">
                                <i class=\"fas fa-trash \">
                                </i></a>
                                  <!-- nuevo modal -->
                                <div class=\"modal fade\" id=\"confirmar$dato\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" role=\"document\">
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <h5 class=\"modal-title\" id=\"exampleModalLabel\"><strong>Confirme la siguiente acción</strong></h5>
                                            </div>
                                            <div class=\"modal-body text-center\">
                                              <p>¿Está seguro de eliminar al usuario <strong>".$row["nomUsuario"]." ".$row["apellUsuario"]."</strong>?</p>
                                            </div>
                                            <div class=\"modal-footer\">
                                                <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">No</button>
                                                <button class=\"btn btn-primary\" type=\"button\" onclick='eliminar_usuario($dato)'>Si</button>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                            </td>";
                  $cont++;}
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>


        </div>
      </div>



      <!-- End of Main Content -->


      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
  <script type="text/javascript">
	function simulateWrapper(target, type, options) {

		switch (type) {
		case "key-sequence":
			options.delay = $('#sequence-delay').val();
			break;
		case "drag":

		}

		switch (type) {
		case "key-sequence":

			$(document).one("simulate-keySequence", simulationEnd);
			break;
		case "drag":
			$(document).one("simulate-drag", simulationEnd);
			break;
		case "drag-n-drop":
		case "drop":
			$(document).one("simulate-drop", simulationEnd);
			break;
		}

		$(target).simulate(type, options);

	}

	function initDemo() {

		$('#combo-admin').bind('click', function() {
      document.getElementById("mysearch").value = " ";
			simulateWrapper('#mysearch', 'key-combo', {combo: "A+d+m+i+n+i+s+t+r+a+d+o+r"});
		});

    $('#combo-vend').bind('click', function() {
      document.getElementById("mysearch").value = " ";
			simulateWrapper('#mysearch', 'key-combo', {combo: "V+e+n+d+e+d+o+r"});
		});

    $('#combo-creaj').bind('click', function() {
      document.getElementById("mysearch").value = " ";
			simulateWrapper('#mysearch', 'key-combo', {combo: "C+r+e+a+d+o+r+ +d+e+ +J+u+e+g+o+s"});
		});

	}
	</script>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#toast1").toast({delay: 4000});
      $("#toast1").toast("show");
      setTimeout(function(){$("#toast1").remove();}, 5000);
    });
  </script>


  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/ajax/estado-cuenta.js"></script>
  <script src="../js/ajax/eliminar_usuario.js"></script>
  <script src="../js/dark-mode.js"></script>

</body>

</html>
