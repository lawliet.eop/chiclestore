<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }

include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

error_reporting(0);
?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Ventas</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <link href="../css/jquery-ui.custom.min.css" rel="stylesheet">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery-ui.custom.min.js"></script>
  <script src="../js/demo/bililiteRange.js"></script>
  <script src="../js/demo/jquery.simulate.js"></script>
  <script src="../js/demo/jquery.simulate.key-sequence.js"></script>
  <script src="../js/demo/jquery.simulate.key-combo.js"></script>

  <script type="text/javascript">
		$(document).ready(function() {
			$eventLog = $('#eventLog'); // Global variable
			$eventLog.indentation = "";
			$eventLog.incIndentation = function() {
				$eventLog.indentation += "    ";
			}
			$eventLog.decIndentation = function() {
				if ($eventLog.indentation.length <= 4)
					$eventLog.indentation = " ";
				else
					$eventLog.indentation = $eventLog.indentation.substr(-4);
			}
			$eventLog.append = function(str, indent) {
				if (indent === true || indent === undefined)
					$eventLog.val($eventLog.val()+$eventLog.indentation+str+"\n");
				else
					$eventLog.val($eventLog.val()+str+"\n");
			}
			logMouseMove = false; // Global variable

			function logButtonEvent(event) {
				var indentLength = 10-event.type.length;
				var indent = "";
				for (var i=0; i < indentLength; i+=1) {
					indent += " ";
				}
				var modifiers = ['shift', 'alt', 'ctrl', 'meta'];
				var activeMods = []
				for (var mod in modifiers) {
					if (event[modifiers[mod]+"Key"]) {
						activeMods.push(modifiers[mod]);
					}
				}
				var activeModStr = "";
				if (activeMods.length > 0) {
					activeModStr = ", modifiers: " + activeMods.join("+");
				}

				$eventLog.append(event.type+indent+"(which: "+event.which/*+", target: "+event.target*//*+", keyCode: "+event.keyCode*//*+", charCode: "+event.charCode*/+activeModStr+")");
			}

			initDemo();
		});
	</script>

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'ventas'; include('../includes/navbar3.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

        <!-- Begin Page Content -->
        <div class="container-fluid" id="mi-tabla">

          <!-- Page Heading -->
          <h1 class="h3 mb-2">
            <strong>Ventas</strong>
            <div class="toast float-right" id="mensaje_eliminacion" style="display:none">
              <div id="estilo1" class="toast-header text-white">
                <i id="estilo2" class="fas fa-check-circle">&nbsp&nbsp</i>
                <strong class="mr-auto" id="exampleModalLabel">¡Exito!</strong>
                <small>1 segundo</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div id="texto" class="toast-body">
                Se eliminó correctamente
              </div>
            </div>
            <?php
            if (isset($_SESSION['alert-registro-clte'])){
              if ($_SESSION['alert-registro-clte'] != " ") {
                echo $_SESSION['alert-registro-clte'];
                $_SESSION['alert-registro-clte']= " ";
              }
            }else{
            }


            if (isset($_SESSION['alert-edit-clte'])){
              if ($_SESSION['alert-edit-clte'] != " ") {
                echo $_SESSION['alert-edit-clte'];
                $_SESSION['alert-edit-clte']= " ";
              }
            }else{
            }
            ?>
          </h1>
          <p class="mb-4">Tabla de datos de las ventas registradas.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4" >
            <div class="card-header bg-dark py-3">
              <a href="registrar-cliente.php" class="btn btn-primary btn-icon-split">
                <span class="text">Registrar Venta</span>
              </a>
              <div class="float-right text-white">
                Busqueda por:&nbsp&nbsp
                <div class="btn-group" role="group">
                <a href="#" class="btn btn-primary" id="combo-atender">
                    <strong>ATENDER</strong>
                </a>
                <a href="#" class="btn btn-danger" id="combo-espdesc">
                    <strong>ESPERANDO DESCARGA</strong>
                </a>
              </div>
            </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr class="bg-dark text-white">
                      <th style="display:none;"> NO DEBERIAS VERME</th>
                      <th class="text-center" style="border: none;">Fecha de Atención</th>
                      <th class="text-center" style="border: none;">Estado de Atención</th>
                      <th class="text-center" style="border: none;">Estado de la Cuenta</th>
                      <th class="text-center" style="border: none;">Nombre del Cliente / Chat</th>
                      <th class="text-center" style="border: none;">Producto</th>
                      <th class="text-center" style="border: none;">Método de Pago</th>
                      <th class="text-center" style="border: none;">Monto</th>
                      <th class="text-center" style="border: none;">Fecha de Depósito</th>
                      <th class="text-center" style="border: none;">Editar</th>
                      <th class="text-center" style="border: none;">Borrar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $cont=1;
                    $result = mysqli_query($conn, "SELECT * FROM juego J INNER JOIN cliente Cl ON J.idJuego = Cl.juego_idJuego INNER JOIN metpagos MP ON Cl.metpagos_idPago = MP.idPago ORDER BY idCliente DESC ");
                    while ($row = mysqli_fetch_assoc($result)){
                        $dato = $row["idCliente"];
                        $nombreCliente = $row['nomCliente'];
                        $fechaAtencion = $row["fechAten"];
                        $micuenta = $row["cuenta_idCuenta"];
                        $chat = $row["urlChat"];
                        $monto = $row["monto"];
                        $estClte = $row["estCliente"];
                        $tipoventa = $row["tVenta_idtVenta"];
                        $ctaComprada = $row["cuenta_Comprada"];
                        $juego = $row["idJuego"];

                        //busca el email de la cuenta comprada
                        if (is_null($ctaComprada)) {
                          $cuentaComprada = "";
                        }else {
                          $result7 = mysqli_query($conn, "SELECT * FROM cuenta WHERE idCuenta = '$ctaComprada'");
                          $row7 = mysqli_fetch_assoc($result7);
                          $cuentaComprada = $row7["email"];
                        }

                        echo "<tr id='fila$dato'>";
                        echo "<td style=\"display:none;\"></td>";
                        echo "<td id='fecha_atencion$dato' class=\"text-center\">".$row["fechAten"]."</td>";

                        //empieza estado de atencion
                          if ($estClte == "ATENDIDO") {
                            echo "<td class=\"text-center\">
                                    <a id='estado_atencion$dato' class=\"btn btn-success \" style=\"color:white; font-size: 12px;\">
                                        <strong>$estClte<strong>
                                    </a>
                                  </td>";
                          }else{
                            echo "<td id='td_estado_atencion$dato' class=\"text-center\">
                                    <a id='estado_atencion$dato' class=\"btn btn-danger \" style=\"color:white; font-size: 12px;\">
                                        <strong>$estClte<strong>
                                    </a>
                                  </td>";
                          }
                        //termina estado de atencion

                        //inicio de estado de la cuenta
                        if ($micuenta == NULL) { //SI NO TENGO CUENTA
                          if ($estClte == "ATENDIDO") { // PUDE QUE YA ESTE ATENDIDO Y MI CUENTA SE ELIMINO
                            echo "<td class=\"text-center\">
                                    <a id='estado_cuenta$dato' href= \"#\" class=\"btn btn-dark\" style=\"font-size: 12px;\" onclick=\"venta_finalizada($dato,'$nombreCliente','$cuentaComprada','$fechaAtencion')\">
                                        <strong>VENTA FINALIZADA</strong>
                                    </a>
                                  </td>";
                          }else{// SI NO ESTOY ATENDIDO

                            if ($row["idJuego"] == 102) { // SI MI JUEGO ES PS PLUS NO NECESITO BUSCAR CUENTAS YA LAS CUENTAS DE ESTE JUEGO NO SE ALMACENAN
                                echo "<td class=\"text-center\">
                                        <a id='estado_cuenta$dato' href='#' class=\"btn btn-primary btn-block\" style=\"color: white; font-size: 12px;\" onclick=\"atender_plus('$nombreCliente', $dato,$monto)\">
                                            <div class=\"form-group row\" style=\"margin:0px;\">
                                                <div class=\"col-sm-12\"><strong>ATENDER</strong>&nbsp&nbsp<img style=\"width:12px; height:12px;\" src=\"../img/psplus.png\"/></div>
                                            </div>
                                        </a>
                                      </td>";

                              }else{ // MI JUEGO ES CUALQUIER OTRO JUEGO DIFERENTE A PS PLUS, AHORA SI PASO A BUSCAR EN LA BD

                                if ($tipoventa == 0) { // SI MI TIPO DE VENTA ES PRINCIPAL BUSCO SOLO CUENTAS DISPONIBLES
                                  $result1 = mysqli_query($conn, "SELECT * FROM cuenta WHERE juego_idJuego = '$juego' AND estadoCuenta = 1 LIMIT 1");


                                  if (mysqli_num_rows($result1) == 0) {//SI NO ENCUENTRO CUENTAS MUESTRO QUE NO HAY DISPONIBLES
                                    echo "<td class=\"text-center\">
                                            <a class=\" btn btn-dark disabled\" style=\"font-size: 12px; color:white;\">
                                                <strong>SIN CUENTAS DISPONIBLES</strong>
                                            </a>
                                          </td>";
                                  }else{//SI ENCUENTRO ALGUNA ESA LE PODRE ASIGNAR A ESE CLIENTE
                                      /*
                                    $row3 = mysqli_fetch_assoc($result1);
                                    $email = $row3['email'];
                                    $pass = $row3['pswd'];*/
                                      $nombreJuego = $row["nombre"];

                                    echo "<td id='venta_principal$dato' class=\"text-center\">
                                            <a href= \"#\" id=\"atender_principal$dato\" class=\"btn btn-primary btn-block\" style=\"color: white; font-size: 14px;\" onclick=\"modal_atender_principal($dato,$juego,1,'$nombreJuego',$tipoventa)\">
                                                <strong>ATENDER</strong>
                                            </a>
                                          </td>";


                                            echo "
                                              <!-- modal venta -->
                                              <div  data-backdrop=\"static\" class=\"modal fade\" id=\"modal_VP_$dato\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                                                  <div class=\"modal-dialog\" role=\"document\">
                                                      <div class=\"modal-content\">
                                                          <div class=\"modal-header\">
                                                              <h5 class=\"modal-title\" id=\"exampleModalLabel\"><strong>Confirme la siguiente acción</strong></h5>
                                                          </div>
                                                          <div class=\"modal-body text-center \" >
                                                            <p>Está a punto de vender a <strong>".$row["nomCliente"]."</strong> como <strong>VENTA PRINCIPAL</strong> la siguiente cuenta:</p>
                                                            <div class=\"form-group row\">
                                                              <div class=\"col-sm-8 text-left\" style=\" padding-left: 30px !important;\" id='VP_datos_cuenta$dato'>
                                                                <p><strong>Juego: </strong>NOMBRE JUEGO...<br>
                                                                <strong>Email: </strong>EMAIL...<br>
                                                                <strong>Password: </strong>PASSWORD...</p>
                                                              </div>
                                                            <div class=\"col-sm-4\" >
                                                            </div>
                                                          </div>
                                                          <div class=\"input-group col-sm-12\">
                                                            <input class=\"form-control\" type=\"text\" value=\"Juego: ".$row["nombre"]." Email: -- Password: --\" id=\"myInput$dato\" aria-label=\"Recipient's username\" aria-describedby=\"basic-addon$dato\">
                                                            <div class=\"input-group-append\">
                                                                <a  id=\"basic-addon$dato\" class=\"btn input-group-text\" onclick=\"myFunction$dato();\" style=\"background: #d9534f; color: #FFFFFF; cursor: pointer;\">Copiar datos</a>
                                                            </div>
                                                          </div>
                                                          <br>
                                                          <strong>¡Enviar datos al cliente!</strong>
                                                          <br>
                                                          <br>
                                                          <a  id=\"mensaje$dato\" class=\"nav-link btn disabled\" onclick=\"activar$dato();\" style=\"background: #0078FF; margin: auto 15px auto 12px;\" target=\"_blank\" href=\"$chat\">
                                                              <i class=\"fab fa-facebook-messenger\" style=\"color: white; font-size:18px;\">
                                                              </i>
                                                          </a>
                                                            </br>
                                                      </div>
                                                      <div class=\"modal-footer\">
                                                        <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">Cancelar</button>
                                                        <button id='si$dato' class=\"btn btn-primary\" type=\"button\"  onclick=\"atender_P('id','correo')\" disabled=\"true\" title=\"Asegurese de copiar y enviar los datos de la cuenta\">Aceptar</button>
                                                      </div>
                                                  </div>
                                              </div>";
                                      }
                                }else {// SI MI TIPO DE VENTA ES SECUNDARIA BUSCO SOLO CUENTAS NO DISPONIBLES SIN VENTAS SECUNDARIAS

                                  $result10 = mysqli_query($conn, "SELECT * FROM cuenta WHERE juego_idJuego = '$juego' AND estadoCuenta = 4 AND ventaSecundario = 0 LIMIT 1");


                                  if (mysqli_num_rows($result10) == 0) {//SI NO ENCUENTRO CUENTAS MUESTRO QUE NO HAY DISPONIBLES
                                    echo "<td class=\"text-center\"><a class=\" btn btn-dark disabled\" style=\"font-size: 12px; color:white;\">
                                          <strong>SIN CUENTAS DISPONIBLES</strong></a></td>";
                                  }else{//SI ENCUENTRO ALGUNA ESA LE PODRE ASIGNAR A ESE CLIENTE
                                      /*
                                    $row9 = mysqli_fetch_assoc($result10);
                                    $email = $row9['email'];
                                    $pass = $row9['pswd'];*/
                                      $nombreJuego = $row["nombre"];
                                      echo "<td id='venta_principal$dato' class=\"text-center\">
                                            <a href= \"#\" id=\"atender_principal$dato\" class=\"btn btn-orange btn-block\" style=\"color: white;font-size: 14px;\" onclick=\"modal_atender_principal($dato, $juego, 4, '$nombreJuego',$tipoventa)\">
                                                <strong>ATENDER</strong>
                                            </a>
                                          </td>";


                                      echo "
                                              <!-- modal venta -->
                                              <div  data-backdrop=\"static\" class=\"modal fade\" id=\"modal_VP_$dato\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                                                  <div class=\"modal-dialog\" role=\"document\">
                                                      <div class=\"modal-content\">
                                                          <div class=\"modal-header\">
                                                              <h5 class=\"modal-title\" id=\"exampleModalLabel\"><strong>Confirme la siguiente acción</strong></h5>
                                                          </div>
                                                          <div class=\"modal-body text-center \" >
                                                            <p>Está a punto de vender a <strong>".$row["nomCliente"]."</strong> como <strong>VENTA SECUNDARIA</strong> la siguiente cuenta:</p>
                                                            <div class=\"form-group row\">
                                                              <div class=\"col-sm-8 text-left\" style=\" padding-left: 30px !important;\" id='VP_datos_cuenta$dato'>
                                                                <p><strong>Juego: </strong>NOMBRE JUEGO...<br>
                                                                <strong>Email: </strong>EMAIL...<br>
                                                                <strong>Password: </strong>PASSWORD...</p>
                                                              </div>
                                                            <div class=\"col-sm-4\" >
                                                            </div>
                                                          </div>
                                                          <div class=\"input-group col-sm-12\">
                                                            <input class=\"form-control\" type=\"text\" value=\"Juego: ".$row["nombre"]." Email: -- Password: --\" id=\"myInput$dato\" aria-label=\"Recipient's username\" aria-describedby=\"basic-addon$dato\">
                                                            <div class=\"input-group-append\">
                                                                <a  id=\"basic-addon$dato\" class=\"btn input-group-text\" onclick=\"myFunction$dato();\" style=\"background: #d9534f; color: #FFFFFF; cursor: pointer;\">Copiar datos</a>
                                                            </div>
                                                          </div>
                                                          <br>
                                                          <strong>¡Enviar datos al cliente!</strong>
                                                          <br>
                                                          <br>
                                                          <a  id=\"mensaje$dato\" class=\"nav-link btn disabled\" onclick=\"activar$dato();\" style=\"background: #0078FF; margin: auto 15px auto 12px;\" target=\"_blank\" href=\"$chat\">
                                                              <i class=\"fab fa-facebook-messenger\" style=\"color: white; font-size:18px;\">
                                                              </i>
                                                          </a>
                                                            </br>
                                                      </div>
                                                      <div class=\"modal-footer\">
                                                        <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">Cancelar</button>
                                                        <button id='si$dato' class=\"btn btn-primary\" type=\"button\"  onclick=\"atender_P('id','correo')\" disabled=\"true\" title=\"Asegurese de copiar y enviar los datos de la cuenta\">Aceptar</button>
                                                      </div>
                                                  </div>
                                              </div>";

                                      }
                                    }
                                  }
                              }
                        }else{//SI YA TENGO UNA CUENTA
                          $result1 = mysqli_query($conn, "SELECT * FROM `cuenta` WHERE idCuenta = '$micuenta'");
                          $row1 = mysqli_fetch_assoc($result1);
                          $estaCta = $row1["estadoCuenta"];

                          if ($estaCta == 2 || $estaCta == '2'){
                              echo "<td class=\"text-center\">
                                      <a href= \"#\" id=\"esperando_descarga$dato\" class=\"btn btn-danger\" style=\"font-size: 12px;\" onclick=\"estado_cuenta_ED($dato,'$nombreCliente','$cuentaComprada','$fechaAtencion')\">
                                          <strong>ESPERANDO DESCARGA</strong>
                                      </a >
                                  </td>";
                          }else{
                              echo "<td class=\"text-center\">
                                        <a href= \"#\" class=\"btn btn-dark\" style=\"font-size: 12px;\" onclick=\"venta_finalizada($dato,'$nombreCliente','$cuentaComprada','$fechaAtencion')\">
                                            <strong>VENTA FINALIZADA</strong>
                                        </a>
                                    </td>";
                          }


                        }
                        //termina estado cuenta

                        if ($tipoventa == 0) {
                          echo "<td class=\"text-center\"><a class=\"nav-link btn\" target=\"_blank\" href=\"$chat\" style=\"background: #0078FF; color: #FFF;\">
                          <div class=\"form-group row\" style=\"margin:0px;\">
                            <div class=\"col-sm-10\"><strong>".$row["nomCliente"]."</strong></div><i class=\"fab fa-facebook-messenger\" style=\"color: #FFF; font-size:14px; margin:2% auto;\"></i>
                          </div></a></td>";
                        }else {
                          echo "<td class=\"text-center\"><a class=\"nav-link btn btn-orange\" target=\"_blank\" href=\"$chat\" style=\"color: #FFF;\">
                          <div class=\"form-group row\" style=\"margin:0px;\">
                            <div class=\"col-sm-10\"><strong>".$row["nomCliente"]."</strong></div><i class=\"fab fa-facebook-messenger\" style=\"color: #FFF; font-size:14px; margin:2% auto;\"></i>
                          </div></a></td>";
                        }


                         if ($row["idJuego"] == 102) {// SI MI JUEGO ES PS PLUS LO RESALTO
                          echo "<td class=\"text-center\"><strong>".$row["nombre"]."</strong></td>";
                        }else {
                          echo "<td class=\"text-center\">".$row["nombre"]."</td>";
                        }
                        echo "<td class=\"text-center\">".$row["nombreMP"]."</td>";
                        echo "<td class=\"text-center\">".$row["monto"]."</td>";
                        echo "<td class=\"text-center\">".$row["fechDepo"]."</td>";
                        //editar una cliente
                        echo "<td class=\"text-center\"><a class=\"nav-link text-secondary\" href=\"editar-cliente.php?dato=$dato\">
                              <i class=\"fas fa-edit\"></i></a></td>";

                        //eliminar un cliente
                        $nomCliente = $row["nomCliente"];
                        echo "<td class=\"text-center\">
                                  <a href='#' class=\"nav-link text-danger\" onclick=\"eliminar_cuenta($dato,'$estClte','$nomCliente')\">
                                    <i class=\"fas fa-trash\"></i>
                                  </a>
                              </td>";
                        echo "</tr>";


                          echo "<script>
                          function myFunction$dato() {
                          var copyText = document.getElementById(\"myInput$dato\");
                          copyText.select();
                          copyText.setSelectionRange(0, 99999)
                          document.execCommand(\"copy\");
                          //alert(\"Copied the text: \" + copyText.value);
                          document.getElementById(\"mensaje$dato\").classList.remove('disabled');
                          document.getElementById(\"basic-addon$dato\").style.backgroundColor = \"#5cb85c\";
                          }

                          </script>
                          <script language=\"javascript\">
                           function activar$dato(){
                              document.getElementById(\"si$dato\").disabled = false;
                            }
                          </script>";

                    $cont++;}
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-- Modal antes de eliminar una cuenta -->
      <div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><strong>Confirme la siguiente acción</strong></h5>
              </div>
              <div class="modal-body text-center">
              <p id="mensaje_eliminar">¿Está seguro de eliminar al cliente <strong> *** </strong>?</p><br>
              <div class="form-group">
                <script type="text/javascript">
                function cambiarforzado() {

                  var checkBox = document.getElementById("chkForzar");

                  if (checkBox.checked == true){
                    forzar = 1;
                  } else {
                    forzar = 0;
                  }
                }
                </script>
              <div class="custom-control custom-checkbox small">
                <input type="checkbox" class="custom-control-input" id="chkForzar" onclick="ShowHideDiv(this)" />
                <label class="custom-control-label" for="chkForzar" style="font-size:16px"><strong>Forzar eliminación</strong></label>
              </div>
                      <p style="font-size:12px">(Usar en caso de querer eliminar un cliente asociado a una cuenta,
                      esto eliminara al cliente y el monto ingresado a la finanza del día).</p>
              </div>
              </div>
          <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
              <button id="boton_eliminar" class="btn btn-primary" type="button" onclick="">Si</button>
          </div>
      </div>
  </div>
</div>


        <!-- Modal para VENTA FINALIZADA -->
        <div class="modal fade" id="mensaje_venta_finalizada" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><strong>Información de venta</strong></h5>
                    </div>
                    <div class="modal-body text-center">
                        <p>Cliente: <strong id="cliente_nombre">NOMBRE DEL CLIENTE</strong></p>
                        <p>Cuenta: <strong id="cuenta">CUENTA</strong></p>
                        <p>Fecha de Atención: <strong id="fecha_atencion">FECHA DE ATENCION</strong></p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal para Vender una cuenta PS Plus -->
        <div class="modal fade" id="atender_ps_plus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><strong>Confirme la siguiente acción</strong></h5>
                    </div>
                    <div class="modal-body text-center">
                    <p>¿Está seguro de que entrego la cuenta de <strong>PS Plus</strong> al cliente <strong id="psPLUS_cliente">CLIENTE PS PLUS</strong>?</p>
                </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                        <button id="vender_plus" class="btn btn-primary" type="button" onclick="vender_plus(ID_CLIENTE)">Si</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal de mensaje al atender -->
        <div class="modal fade" id="atencion_correcta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¡Exito!</h5>
                    </div>
                    <div class="modal-body text-center">
                        <p id="operacion_exitosa">Se atendio al Cliente Correctamente.</p>
                        <i id="estilo" class="fas fa-check-circle text-success" style="font-size: 35px;"></i>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal de Esperando Descarga -->
        <div class="modal fade" id="modal_esperando_descarga" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><strong>Confirme la siguiente acción</strong></h5>
                    </div>
                    <div class="modal-body text-center">
                        <p id="pregunta$micuenta">
                            ¿Está seguro de que el cliente: <strong id="ED_nombre_cliente">NOMBRE DEL CLIENTE</strong> descargo por completo el juego?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                        <button id="boton_ED" class="btn btn-primary" type="button" onclick="cambiarEstado($micuenta,'PETICIÓN DE RESETEO')">Si</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal de error al querer atender -->
        <div class="modal fade" id="modal_error_venta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><strong>¡Ocurrio un error!</strong></h5>
                    </div>
                    <div class="modal-body text-center">
                        <p id="pregunta$micuenta">
                            Al parecer la cuenta ya no esta disponible para este usuario. <strong id="ED_nombre_cliente">Porfavor recargue la pagina para actulizar los datos</strong>
                        </p>
                        <i class="fas fa-times-circle text-danger" style="font-size: 35px;"></i>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>


      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>

  <script type="text/javascript">
	function simulateWrapper(target, type, options) {

		switch (type) {
		case "key-sequence":
			options.delay = $('#sequence-delay').val();
			break;
		case "drag":

		}

		switch (type) {
		case "key-sequence":

			$(document).one("simulate-keySequence", simulationEnd);
			break;
		case "drag":
			$(document).one("simulate-drag", simulationEnd);
			break;
		case "drag-n-drop":
		case "drop":
			$(document).one("simulate-drop", simulationEnd);
			break;
		}

		$(target).simulate(type, options);

	}

	function initDemo() {

		$('#combo-atender').bind('click', function() {
      document.getElementById("mysearch").value = " ";
			simulateWrapper('#mysearch', 'key-combo', {combo: "A+T+E+N+D+E+R"});
		});

    $('#combo-espdesc').bind('click', function() {
      document.getElementById("mysearch").value = " ";
			simulateWrapper('#mysearch', 'key-combo', {combo: "E+S+P+E+R+A+N+D+O+ +D+E+S+C+A+R+G+A"});
		});

	}
	</script>


  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#toast1").toast({delay: 4000});
      $("#toast1").toast("show");
      setTimeout(function(){$("#toast1").remove();}, 5000);
    });
  </script>
  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/ajax/eliminar_cliente.js"></script>
  <script src="../js/ajax/venta_finalizada.js"></script>
  <script src="../js/ajax/atender_ps_plus.js"></script>
  <script src="../js/ajax/atender_principal.js"></script>
  <script src="../js/ajax/estado_cuenta_cliente.js"></script>
  <script src="../js/dark-mode.js"></script>
</body>

</html>
