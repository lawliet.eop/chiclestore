<?php
@session_start();

  if (isset($_SESSION['ok'])) {
    // el usuario existe
  }else {
    header("location: ../index.php");
  }

include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon"  href="../img/icon.ico">

  <title>Cuentas</title>

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="misestilos" href="../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <link href="../css/jquery-ui.custom.min.css" rel="stylesheet">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery-ui.custom.min.js"></script>
  <script src="../js/demo/bililiteRange.js"></script>
  <script src="../js/demo/jquery.simulate.js"></script>
  <script src="../js/demo/jquery.simulate.key-sequence.js"></script>
  <script src="../js/demo/jquery.simulate.key-combo.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $eventLog = $('#eventLog'); // Global variable
      $eventLog.indentation = "";
      $eventLog.incIndentation = function() {
        $eventLog.indentation += "    ";
      }
      $eventLog.decIndentation = function() {
        if ($eventLog.indentation.length <= 4)
          $eventLog.indentation = " ";
        else
          $eventLog.indentation = $eventLog.indentation.substr(-4);
      }
      $eventLog.append = function(str, indent) {
        if (indent === true || indent === undefined)
          $eventLog.val($eventLog.val()+$eventLog.indentation+str+"\n");
        else
          $eventLog.val($eventLog.val()+str+"\n");
      }
      logMouseMove = false; // Global variable

      function logButtonEvent(event) {
        var indentLength = 10-event.type.length;
        var indent = "";
        for (var i=0; i < indentLength; i+=1) {
          indent += " ";
        }
        var modifiers = ['shift', 'alt', 'ctrl', 'meta'];
        var activeMods = []
        for (var mod in modifiers) {
          if (event[modifiers[mod]+"Key"]) {
            activeMods.push(modifiers[mod]);
          }
        }
        var activeModStr = "";
        if (activeMods.length > 0) {
          activeModStr = ", modifiers: " + activeMods.join("+");
        }

        $eventLog.append(event.type+indent+"(which: "+event.which/*+", target: "+event.target*//*+", keyCode: "+event.keyCode*//*+", charCode: "+event.charCode*/+activeModStr+")");
      }

      initDemo();
    });
  </script>

  <script type="text/javascript">
    let estadoluz = localStorage.getItem('Luz')
    console.log(`Luz esta ${estadoluz}`);
    if (estadoluz == 1) {
      document.getElementById('misestilos').href = "../css/sb-admin-2.1.min.css";
    }else {
      document.getElementById('misestilos').href = "../css/sb-admin-2.min.css";
    }
  </script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $page = 'cuentas'; include('../includes/navbar3.php')?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" class="my-content">

        <?php include('../includes/topbar.php')?>

        <!-- Begin Page Content -->
        <div class="container-fluid" id="mi-tabla">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 ">
            <strong>Cuentas</strong>
            <div class="toast float-right" id="eliminando" style="display:none">
              <div id="estilo1" class="toast-header text-white">
                <i id="estilo2" class="fas fa-check-circle">&nbsp&nbsp</i>
                <strong class="mr-auto" id="exampleModalLabel">¡Exito!</strong>
                <small>1 segundo</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div id="texto" class="toast-body">
                Se eliminó correctamente
              </div>
            </div>
              <?php
                if (isset($_SESSION['alert-registro-cta'])){
                  if ($_SESSION['alert-registro-cta'] != " ") {

                    echo $_SESSION['alert-registro-cta'];
                    $_SESSION['alert-registro-cta']= " ";
                    }
                  }else{
                  }


                if (isset($_SESSION['alert-editar-cta'])){
                    if ($_SESSION['alert-editar-cta'] != " ") {

                      echo $_SESSION['alert-editar-cta'];
                      $_SESSION['alert-editar-cta']= " ";
                    }
                  }else{
                }
                ?>
          </h1>
          <p class="mb-4">Tabla de datos de las cuentas existentes.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4" >
            <div  class="card-header bg-dark py-3">
              <a href="registrar-cuenta.php" class="btn btn-primary btn-icon-split">
                <span class="text">Agregar Cuenta</span>
              </a>
              <div class="float-right text-white">
                Busqueda por:&nbsp&nbsp
                <div class="btn-group" role="group">
                <a href="#" class="btn btn-info" id="combo-petrest">
                    <strong>PETICIÓN DE RESETEO</strong>
                </a>
                <a href="#" class="btn btn-danger" id="combo-espdesc">
                    <strong>ESPERANDO DESCARGA</strong>
                </a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive rounded">
                <table id="dataTable" class="table table-striped table-light"  width="100%" cellspacing="0">
                  <thead>
                    <tr class="bg-dark text-white">
                      <th style="display:none;"> NO DEBERIAS VERME</th>
                      <th class="text-center" style="border: none;">Rango</th>
                      <th class="text-center" style="border: none;">Estado</th>
                      <th class="text-center" style="border: none;">Videojuego</th>
                      <th class="text-center" style="border: none;">Email</th>
                      <th class="text-center" style="border: none;">Password</th>
                      <th class="text-center" style="border: none;">Ventas en Principal.</th>
                      <th class="text-center" style="border: none;">Ventas en Secundario</th>
                      <th class="text-center" style="border: none;">Reseteos</th>
                      <th class="text-center" style="border: none;">Fecha de Últ. Reseteo</th>
                      <th class="text-center" style="border: none;">ID PSN</th>
                      <th class="text-center" style="border: none;">Precio en Dólares</th>
                      <th class="text-center" style="border: none;">Cuenta Padre</th>
                      <th class="text-center" style="border: none;">Editar</th>
                      <th class="text-center" style="border: none;">Borrar</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  $cont = 1;
                  $result = mysqli_query($conn, "SELECT * FROM juego J INNER JOIN cuenta C ON J.idJuego = C.juego_idJuego ORDER BY idCuenta DESC");
                  while ($row = mysqli_fetch_assoc($result)){
                      $dato = $row["idCuenta"];
                      $ferest = $row["ultimoReseteo"];
                      $estaCta = $row["estadoCuenta"];
                      $idPadre = $row["cuenta_idPadre"];
                      $correo = $row["email"];
                      echo "<tr id='fila$dato'>";
                      echo "<td style=\"display:none;\"></td>";
                      echo "<td class=\"text-center\">".$row["rango"]."</td>";
                      switch ($estaCta) {
                        case 1:
                          if ($row["nombre"] == "Sin Juego") {
                            echo "<td class=\"text-center\"><a class=\" btn btn-light\" style=\"font-size: 12px; color:black;\">
                                <strong>NO TIENE JUEGO</strong></a></td>";
                          }else {
                            echo "<td class=\"text-center\">
                                    <a  class=\" btn btn-success\" style=\"font-size: 12px; color:white;\">
                                        <strong>DISPONIBLE</strong>
                                    </a>
                                  </td>";
                          }
                          break;
                        case 2:
                          $result6 = mysqli_query($conn, "SELECT * FROM `cliente` WHERE cuenta_idCuenta = '$dato'");
                          $row6 = mysqli_fetch_assoc($result6);
                          echo "<td class=\"text-center\">
                                    <a href= \"#\" id=\"submit$dato\" class=\"btn btn-danger\" style=\"font-size: 12px;\" data-toggle=\"modal\" data-target=\"#confirmas$dato\">
                                        <strong>ESPERANDO DESCARGA</strong>
                                    </a >
                                    <!-- Modal -->
                                    <div class=\"modal fade\" id=\"confirmas$dato\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                                        <div class=\"modal-dialog\" role=\"document\">
                                            <div class=\"modal-content\">
                                                <div class=\"modal-header\">
                                                    <h5 class=\"modal-title\" id=\"exampleModalLabel\"><strong>Confirme la siguiente acción</strong></h5>
                                                </div>
                                                <div class=\"modal-body text-center\">
                                                    <p id=\"pregunta$dato\">¿Está seguro de que el cliente: <strong>".$row6["nomCliente"]."</strong> descargo por completo el juego?</p>
                                                </div>
                                                <div class=\"modal-footer\">
                                                    <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">No</button>
                                                    <button id=\"botonSi$dato\" class=\"btn btn-primary\" type=\"button\" onclick=\"cambiarEstado($dato,'PETICIÓN DE RESETEO')\">Si</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>";
                          break;
                        case 3:
                          echo"<td class=\"text-center\">
                                    <a href= \"#\" id=\"submit$dato\" class=\"btn btn-info\" style=\"font-size: 12px;\" data-toggle=\"modal\" data-target=\"#confirmas$dato\">
                                        <strong>PETICIÓN DE RESETEO</strong>
                                    </a>
                                      <!-- nuevo modal -->
                                    <div class=\"modal fade\" id=\"confirmas$dato\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                                        <div class=\"modal-dialog\" role=\"document\">
                                            <div class=\"modal-content\">
                                                <div class=\"modal-header\">
                                                    <h5 class=\"modal-title\" id=\"exampleModalLabel\"><strong>Confirme la siguiente acción</strong></h5>
                                                </div>
                                                <div class=\"modal-body text-center\">
                                                  <p>¿Está seguro de que la cuenta <strong>".$row["email"]."</strong> ha sido reseteada?</p>
                                                </div>
                                                <div class=\"modal-footer\">
                                                    <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">No</button>
                                                    <button class=\"btn btn-primary\" type=\"button\" onclick=\"cambiarEstado($dato, 'DISPONIBLE')\">Si</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>";
                          break;
                        case 4:
                              $fecha = date("Y-m-d", strtotime($ferest));
                              $nuevafecha = strtotime ( '+6 month' , strtotime ( $fecha ) ) ;
                              $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                          echo "<td class=\"text-center\"><a class=\" btn btn-warning\" style=\"font-size: 12px; color:white;\">
                              <strong> NO DISP. HASTA ".$nuevafecha."</strong></a></td>";
                          break;
                        default:
                          echo 'Opcion no valida</br>';
                          break;
                      }
                      echo "<td class=\"text-center\">".$row["nombre"]."</td>";
                      echo "<input id=\"correo$dato\" value=\"$correo\" type='hidden'>";
                      echo "<td class=\"text-center\">".$row["email"]."</td>";
                      echo "<td class=\"text-center\">".$row["pswd"]."</td>";
                      echo "<td class=\"text-center\">".$row["ventaPrincipal"]."</td>";
                      echo "<td class=\"text-center\">".$row["ventaSecundario"]."</td>";
                      echo "<td class=\"text-center\" id=\"nReseteos$dato\">".$row["nReseteos"]."</td>";
                      echo "<td class=\"text-center\" id=\"ultimoReseteo$dato\">".$row["ultimoReseteo"]."</td>";
                      echo "<td class=\"text-center\">".$row["nPSN"]."</td>";
                      echo "<td class=\"text-center\">".$row["precio"]."</td>";
                      if ($idPadre == NULL) {
                        echo "<td class=\"text-center\"> --- </td>";
                      }else {
                        $result5 = mysqli_query($conn, "SELECT * FROM `cuenta` WHERE idCuenta = '$idPadre'");
                        $row5 = mysqli_fetch_assoc($result5);
                          echo "<td class=\"text-center\">".$row5["email"]."</td>";
                      }
                      echo "<td class=\"text-center\"><a class=\"nav-link text-secondary\" href=\"editar-cuenta.php?dato=$dato\">
                            <i class=\"fas fa-edit \"></i></a></td>";

                      echo "<td class=\"text-center\">
                                <input name=\"ctaedit\" value=\"$dato\" type='hidden'>
                                <a href='#' class=\"nav-link text-danger\" id=\"submit$dato\" data-toggle=\"modal\" data-target=\"#confirmar$dato\">
                                <i class=\"fas fa-trash \">
                                </i></a>
                                  <!-- nuevo modal -->
                                <div class=\"modal fade\" id=\"confirmar$dato\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" role=\"document\">
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <h5 class=\"modal-title\" id=\"exampleModalLabel\"><strong>Confirme la siguiente acción</strong></h5>
                                            </div>
                                            <div class=\"modal-body text-center\">
                                              <p>¿Está seguro de eliminar la cuenta <strong>".$row["email"]."</strong>?</p>
                                            </div>
                                            <div class=\"modal-footer\">
                                                <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">No</button>
                                                <button class=\"btn btn-primary\" type=\"button\" onclick='eliminar_cuenta($dato)'>Si</button>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                            </td>";
                  $cont++;}
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>



      </div>



      <!-- End of Main Content -->


      <?php include('../includes/footer.php')?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../includes/modal-logout.php')?>

  <script type="text/javascript">
  function simulateWrapper(target, type, options) {

    switch (type) {
    case "key-sequence":
      options.delay = $('#sequence-delay').val();
      break;
    case "drag":

    }

    switch (type) {
    case "key-sequence":

      $(document).one("simulate-keySequence", simulationEnd);
      break;
    case "drag":
      $(document).one("simulate-drag", simulationEnd);
      break;
    case "drag-n-drop":
    case "drop":
      $(document).one("simulate-drop", simulationEnd);
      break;
    }

    $(target).simulate(type, options);

  }

  function initDemo() {

    $('#combo-petrest').bind('click', function() {
      document.getElementById("mysearch").value = " ";
      simulateWrapper('#mysearch', 'key-combo', {combo: "P+E+T+I+C+I+Ó+N+ +D+E+ +R+E+S+E+T+E+O"});
    });

    $('#combo-espdesc').bind('click', function() {
      document.getElementById("mysearch").value = " ";
      simulateWrapper('#mysearch', 'key-combo', {combo: "E+S+P+E+R+A+N+D+O+ +D+E+S+C+A+R+G+A"});
    });

  }
  </script>

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#toast1").toast({delay: 4000});
      $("#toast1").toast("show");
      setTimeout(function(){$("#toast1").remove();}, 5000);
    });
  </script>
  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>
  <script src="../js/ajax/estado-cuenta.js"></script>
  <script src="../js/ajax/eliminar_cuenta.js"></script>
  <script src="../js/dark-mode.js"></script>

</body>

</html>
