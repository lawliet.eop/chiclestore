<?php
@session_start();
include '../conexion/conn.php';

// variables de conexion
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

    date_default_timezone_set('america/lima');

    $month = date('m');
    $day = date('d');
    $year = date('Y');

    $today = $year . '-' . $month . '-' . $day;


    $juego = $_POST["game"];
    $nomclte = $_POST["nomClte"];
    $chat = $_POST["urlChat"];
    $tipoventa = $_POST["tipoventa"];
    $metpago = $_POST["metodp"];
    $fDepo = $_POST["Fdepo"];
    $monto = $_POST["monto"];


    //buscar id del juego
    if($juego == "Sin Juego"){
        $idJuego = "1";
    }else{
        $result1 = mysqli_query($conn, "SELECT * FROM `juego` WHERE nombre = '$juego'");
        $row1 = mysqli_fetch_assoc($result1);
        $idJuego = $row1["idJuego"];
    }

    //buscar metodo de pago
    if($metpago == "---"){
        $idMet = "1";
    }else{
        $result2 = mysqli_query($conn, "SELECT * FROM `metpagos` WHERE nombreMP = '$metpago'");
        $row2 = mysqli_fetch_assoc($result2);
        $idMet = $row2["idPago"];
    }

    //buscar tipo de venta
    if($tipoventa == "Principal"){
        $idTV = "0";
    }else{
        $idTV = "1";
    }

    $result = mysqli_query($conn, "INSERT INTO `cliente` (`idCliente`, `cuenta_idCuenta`, `fechAten`, `nomCliente`, `urlChat`, `metpagos_idPago`, `monto`, `fechDepo`, `estCliente`, `juego_idJuego`, `tVenta_idtVenta`) VALUES (NULL, NULL, '$today', '$nomclte', '$chat', '$idMet', '$monto', '$fDepo', 'NO ATENDIDO', '$idJuego', '$idTV')");
    if ($result){
        //cuando se logra registrar un cliente

        //update de la finanza
        date_default_timezone_set('america/lima');
        $anio = date('Y');
        $mes = date('m');
        $dia = date('d');
        $fecha_atencion = $anio."-".$mes."-".$dia;

        $result = mysqli_query($conn,"SELECT `idFinanza` FROM `finanzas` WHERE `fechFinan` = '$today'");

        if (mysqli_num_rows($result) == 0){
            //no existe finanza para esa fecha - se busca la ultimo finanza
            //se busca la finanza anterior
            $result1 = mysqli_query($conn,"SELECT MAX(idFinanza) FROM `finanzas`");
            $idFinanza_Anterior = mysqli_fetch_assoc($result1)['MAX(idFinanza)'];
            //busca la fecha de la finanza anterior
            $result2 = mysqli_query($conn,"SELECT `fechFinan` FROM `finanzas` WHERE `idFinanza` = '$idFinanza_Anterior'");
            $ffant = mysqli_fetch_assoc($result2)['fechFinan'];
            $array = explode("-", $ffant);
            //si el mes de esta finanza es diferente al de la ultima finanza
            if ($mes != $array[1]){
                //si es el primer dia del mes
                //inserta una nueva finanza para el primer dia del mes
                mysqli_query($conn,"INSERT INTO `finanzas` (`idFinanza`, `ingresos`, `saldoComprado`, `gastoPersonal`, `gastoPublicidad`, `gastoExtra`, `total`, `diaAnterior`, `hastaHoy`, `fechFinan`) VALUES (NULL, '$monto', '0', '0', '0', '0', '$monto', NULL , '$monto', '$today');");
            }else{
                //busca el hasta hoy de la finanza anterior
                $result3 = mysqli_query($conn,"SELECT `hastaHoy` FROM `finanzas` WHERE `idFinanza` = '$idFinanza_Anterior'");
                $hastaHoy_Anterior = mysqli_fetch_assoc($result3)['hastaHoy']+$monto;
                //inserta una nueva finanza
                mysqli_query($conn,"INSERT INTO `finanzas` (`idFinanza`, `ingresos`, `saldoComprado`, `gastoPersonal`, `gastoPublicidad`, `gastoExtra`, `total`, `diaAnterior`, `hastaHoy`, `fechFinan`) VALUES (NULL, '$monto', '0', '0', '0', '0', '$monto', '$idFinanza_Anterior', '$hastaHoy_Anterior', '$today');");
            }

        }else{
            //existe finanza para esa fecha - se actualiza los campos
            $idFinanza = mysqli_fetch_assoc($result)['idFinanza'];
            //busca los datos de la finanza
            $result3 = mysqli_query($conn,"SELECT `ingresos`,`total`,`hastaHoy` FROM `finanzas` WHERE `idFinanza` = '$idFinanza'");
            $row = mysqli_fetch_assoc($result3);
            $ingresos = $row['ingresos']+$monto;
            $total = $row['total']+$monto;
            $hastaHoy = $row['hastaHoy']+$monto;
            mysqli_query($conn,"UPDATE `finanzas` SET `ingresos` = '$ingresos', `total` = '$total', `hastaHoy` = '$hastaHoy' WHERE `finanzas`.`idFinanza` = '$idFinanza';");
        }

        //modal de correcto al registrar
        $_SESSION['alert-registro-clte'] = "<div class=\"toast float-right\" id=\"toast1\">
        <div class=\"toast-header bg-success text-white\">
          <i class=\"fas fa-check-circle\">&nbsp&nbsp</i>
          <strong class=\"mr-auto\">¡Hecho!&nbsp&nbsp</strong>
          <small>1 segundo</small>
          <button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\" aria-label=\"Close\">
            <span aria-hidden=\"true\">&times;</span>
          </button>
        </div>
        <div class=\"toast-body\">
        Cliente <strong>".$nomclte."</strong> registrado correctamente.
        </div>
      </div>";
    }else{
        //ocurrio un problema al registrar al cliente

        //modal de falla al registrar
        $_SESSION['alert-registro-clte'] = "<div class=\"toast float-right\" id=\"toast1\">
        <div class=\"toast-header bg-danger text-white\">
          <i class=\"fas fa-times-circle\">&nbsp&nbsp</i>
          <strong class=\"mr-auto\">¡Error!&nbsp&nbsp</strong>
          <small>1 segundo</small>
          <button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\" aria-label=\"Close\">
            <span aria-hidden=\"true\">&times;</span>
          </button>
        </div>
        <div class=\"toast-body\">
        <strong>No se pudo registrar, porfavor revise los caracteres ingresados.</strong>
        </div>
      </div>";
    }

    header('location: clientes.php');
