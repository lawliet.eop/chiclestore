<?php
    include '../conexion/conn.php';
    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $idJuego = $_POST["idJuego"];
    $estadoCuenta = $_POST["estadoCuenta"];

    //buscamos una cuenta
    if ($estadoCuenta == 0) {
      $result1 = mysqli_query($conn, "SELECT email,pswd FROM cuenta WHERE juego_idJuego = '$idJuego' AND estadoCuenta = '$estadoCuenta' LIMIT 1");

    }else {
      $result1 = mysqli_query($conn, "SELECT email,pswd FROM cuenta WHERE juego_idJuego = '$idJuego' AND estadoCuenta = '$estadoCuenta' AND ventaSecundario = 0 LIMIT 1");
    }

    if (mysqli_num_rows($result1) == 0){
        //no existe una cuenta para vender - se le pide q recargue la pagina para actualizar datos
        $json = array();
        $json[0] = array(
            'server'=>'fallo'
        );
        $jsonstring = json_encode($json[0]);
        echo $jsonstring;
    }else{
        //si existe una cuenta para vender
        $row = mysqli_fetch_assoc($result1);
        $email_cuenta = $row['email'];
        $pswd_cuenta = $row['pswd'];

        $json = array();
        $json[0] = array(
            'server'=>'exito',
            'email_cuenta'=>$email_cuenta,
            'pswd_cuenta'=>$pswd_cuenta
        );
        $jsonstring = json_encode($json[0]);
        echo $jsonstring;
    }
