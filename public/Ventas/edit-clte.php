<?php
    @session_start();
    include '../conexion/conn.php';
    $idCliente = $_SESSION['id'];
    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $result = mysqli_query($conn, "SELECT * FROM cliente WHERE idCliente = '$idCliente'");
    $row = mysqli_fetch_assoc($result);
    $estClt = $row["estCliente"];
    $fechAten = $row["fechAten"];

    $nombre = $_POST["nombre"];
    $urlChat = $_POST["urlChat"];
    $juego = $_POST["game"];
    $tipoventa = $_POST["tipoventa"];
    $metodp = $_POST["metodp"];
    $monto = $_POST["monto"];
    $montoant = $_POST["montoant"];
    $Fdepo = $_POST["Fdepo"];


    $result4 = mysqli_query($conn, "SELECT * FROM `finanzas` WHERE fechFinan = '$fechAten'");
    $row4 = mysqli_fetch_assoc($result4);
    $finanza = $row4["idFinanza"];

    //buscar id del juego
    if($juego == "Sin Juego"){
        $idJuego = "1";
    }else{
        $result1 = mysqli_query($conn, "SELECT * FROM `juego` WHERE nombre = '$juego'");
        $row1 = mysqli_fetch_assoc($result1);
        $idJuego = $row1["idJuego"];
    }

    //buscar metodo de pago
    if($metodp == "---"){
        $idMet = "1";
    }else{
        $result2 = mysqli_query($conn, "SELECT * FROM `metpagos` WHERE nombreMP = '$metodp'");
        $row2 = mysqli_fetch_assoc($result2);
        $idMet = $row2["idPago"];
    }

    //buscar tipo de venta
    if($tipoventa == "Principal"){
        $idTV = "0";
    }else{
        $idTV = "1";
    }

      //insert cliente
    $result3 = mysqli_query($conn, "UPDATE `cliente` SET `cuenta_idCuenta` = NULL, `fechAten` = '$fechAten', `nomCliente` = '$nombre', `urlChat` = '$urlChat', `metpagos_idPago` = '$idMet', `monto` = '$monto', `fechDepo` = '$Fdepo', `estCliente` = '$estClt', `juego_idJuego` = '$idJuego', `tVenta_idtVenta` = '$idTV' WHERE `idCliente` = '$idCliente';");


$diaant = $row4["diaAnterior"];
$result3 = mysqli_query($conn, "SELECT * FROM `finanzas` WHERE idFinanza = '$diaant'");
$row3 = mysqli_fetch_assoc($result3);
$totalant = $row3["hastaHoy"];
$ingresos = $row4["ingresos"];

$ingresos = $ingresos - $montoant + $monto;
$gastos = $row4["saldoComprado"] + $row4["gastoPersonal"] + $row4["gastoPublicidad"] + $row4["gastoExtra"];
$total = $ingresos - $gastos;
$totalhoy = $totalant + $total;
//echo "ID=".$finanza." Ingreso=".$ingresos." Gastos=".$gastos." total=".$total." Anterior=".$totalant." HastaHoy=".$totalhoy."<br>";
$result5 = mysqli_query($conn, "UPDATE `finanzas` SET `ingresos` = '$ingresos', `total` = '$total', `hastaHoy` = '$totalhoy' WHERE `finanzas`.`idFinanza` = '$finanza';");

$respuesta = mysqli_query($conn,"SELECT * FROM finanzas WHERE idFinanza > '$finanza'");
while ($fila = mysqli_fetch_assoc($respuesta)){
    $idHoy = $fila['idFinanza'];
    $ingreso = $fila['ingresos'];
    $gasto = $fila["saldoComprado"] + $fila["gastoPersonal"] + $fila["gastoPublicidad"] + $fila["gastoExtra"];
    $total = $ingreso-$gasto;
    $idAnterior = $fila['diaAnterior'];
    $respuesta2 = mysqli_query($conn,"SELECT hastaHoy FROM finanzas WHERE idFinanza = '$idAnterior'");
    $diaAnterior = mysqli_fetch_assoc($respuesta2)['hastaHoy'];
    $hastaHoy = $total+$diaAnterior;
    //echo "ID=".$idHoy." Ingreso=".$ingreso." Gastos=".$gasto." total=".$total." Anterior(".$idAnterior.")=".$diaAnterior." HastaHoy=".$hastaHoy."<br>";
    mysqli_query($conn, "UPDATE `finanzas` SET `ingresos` = '$ingreso', `total` = '$total', `hastaHoy` = '$hastaHoy' WHERE `finanzas`.`idFinanza` = '$idHoy';");
}



    $_SESSION['alert-edit-clte'] = "<div class=\"toast float-right\" id=\"toast1\">
    <div class=\"toast-header bg-success text-white\">
      <i class=\"fas fa-check-circle\">&nbsp&nbsp</i>
      <strong class=\"mr-auto\">ChicleStore&nbsp&nbsp</strong>
      <small>1 segundo</small>
      <button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\" aria-label=\"Close\">
        <span aria-hidden=\"true\">&times;</span>
      </button>
    </div>
    <div class=\"toast-body\">
    <strong>Los cambios en el cliente se guardaron.</strong>
    </div>
    </div>";
    header('location: clientes.php');
