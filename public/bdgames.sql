-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2020 a las 17:34:39
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdgames`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `cuenta_idCuenta` int(11) DEFAULT NULL,
  `fechAten` varchar(12) CHARACTER SET latin1 NOT NULL,
  `nomCliente` varchar(50) CHARACTER SET latin1 NOT NULL,
  `urlChat` text CHARACTER SET latin1 NOT NULL,
  `metpagos_idPago` int(11) NOT NULL,
  `monto` float NOT NULL,
  `fechDepo` varchar(12) CHARACTER SET latin1 NOT NULL,
  `estCliente` varchar(25) CHARACTER SET latin1 NOT NULL,
  `juego_idJuego` int(11) NOT NULL,
  `cuenta_Comprada` int(11) DEFAULT NULL,
  `tVenta_idtVenta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `cuenta_idCuenta`, `fechAten`, `nomCliente`, `urlChat`, `metpagos_idPago`, `monto`, `fechDepo`, `estCliente`, `juego_idJuego`, `cuenta_Comprada`, `tVenta_idtVenta`) VALUES
(7, 65, '2019-11-17', 'Curtis Archbell', 'http://dagondesign.com/in/felis/eu/sapien.jsp?id=sed&turpis=interdum&integer=venenatis&aliquet=turpis&massa=enim&id=blandit&lobortis=mi&convallis=in&tortor=porttitor&risus=pede&dapibus=justo&augue=eu&vel=massa&accumsan=donec', 2, 435, '2019-09-22', 'ATENDIDO', 6, 65, 0),
(10, 1, '2019-03-21', 'Kyrstin Maha', 'https://mysql.com/ipsum/primis/in/faucibus.html?congue=odio&diam=cras&id=mi&ornare=pede&imperdiet=malesuada&sapien=in&urna=imperdiet&pretium=et&nisl=commodo&ut=vulputate&volutpat=justo&sapien=in&arcu=blandit&sed=ultrices&augue=enim&aliquam=lorem&erat=ipsum&volutpat=dolor&in=sit', 2, 201, '2019-03-25', 'ATENDIDO', 10, 1, 0),
(11, 66, '2019-05-10', 'Coletta Harrhy', 'http://mlb.com/tempus/sit/amet/sem/fusce/consequat.xml?consequat=nulla&ut=justo&nulla=aliquam&sed=quis&accumsan=turpis&felis=eget&ut=elit&at=sodales&dolor=scelerisque&quis=mauris&odio=sit&consequat=amet&varius=eros&integer=suspendisse&ac=accumsan&leo=tortor', 2, 131, '2019-09-03', 'ATENDIDO', 13, 66, 0),
(12, NULL, '2019-11-03', 'Harlene Whistlecroft', 'http://hugedomains.com/ligula/vehicula/consequat/morbi/a/ipsum.jsp?erat=dis&volutpat=parturient&in=montes&congue=nascetur&etiam=ridiculus&justo=mus&etiam=etiam&pretium=vel&iaculis=augue&justo=vestibulum&in=rutrum&hac=rutrum&habitasse=neque&platea=aenean&dictumst=auctor&etiam=gravida&faucibus=sem&cursus=praesent&urna=id&ut=massa&tellus=id&nulla=nisl&ut=venenatis&erat=lacinia&id=aenean&mauris=sit&vulputate=amet&elementum=justo&nullam=morbi&varius=ut&nulla=odio&facilisi=cras&cras=mi&non=pede', 2, 160, '2019-09-30', 'NO ATENDIDO', 102, NULL, 0),
(13, 86, '2019-11-05', 'Abbye Spofforth', 'http://eepurl.com/fusce/posuere/felis.png?praesent=et&lectus=tempus&vestibulum=semper&quam=est&sapien=quam&varius=pharetra&ut=magna&blandit=ac&non=consequat&interdum=metus&in=sapien&ante=ut&vestibulum=nunc&ante=vestibulum&ipsum=ante&primis=ipsum&in=primis&faucibus=in&orci=faucibus&luctus=orci&et=luctus&ultrices=et&posuere=ultrices&cubilia=posuere&curae=cubilia&duis=curae&faucibus=mauris&accumsan=viverra&odio=diam&curabitur=vitae&convallis=quam&duis=suspendisse', 2, 408, '2019-06-24', 'ATENDIDO', 13, 86, 0),
(15, NULL, '2019-07-17', 'Quintana Derges', 'http://ucla.edu/pulvinar/nulla/pede/ullamcorper.xml?phasellus=turpis&in=elementum&felis=ligula&donec=vehicula&semper=consequat&sapien=morbi&a=a&libero=ipsum&nam=integer&dui=a&proin=nibh&leo=in&odio=quis&porttitor=justo&id=maecenas&consequat=rhoncus&in=aliquam&consequat=lacus&ut=morbi&nulla=quis&sed=tortor&accumsan=id&felis=nulla&ut=ultrices&at=aliquet&dolor=maecenas&quis=leo&odio=odio&consequat=condimentum&varius=id&integer=luctus&ac=nec&leo=molestie&pellentesque=sed&ultrices=justo&mattis=pellentesque&odio=viverra&donec=pede&vitae=ac&nisi=diam&nam=cras&ultrices=pellentesque&libero=volutpat&non=dui&mattis=maecenas&pulvinar=tristique&nulla=est&pede=et&ullamcorper=tempus&augue=semper&a=est&suscipit=quam&nulla=pharetra&elit=magna&ac=ac&nulla=consequat&sed=metus&vel=sapien&enim=ut&sit=nunc&amet=vestibulum&nunc=ante&viverra=ipsum&dapibus=primis&nulla=in&suscipit=faucibus&ligula=orci&in=luctus&lacus=et&curabitur=ultrices&at=posuere&ipsum=cubilia&ac=curae&tellus=mauris&semper=viverra&interdum=diam', 2, 424, '2019-08-11', 'ATENDIDO', 9, NULL, 0),
(16, NULL, '2019-04-01', 'Florencia Schimaschke', 'https://imdb.com/mus/vivamus/vestibulum/sagittis.jpg?convallis=duis&duis=bibendum&consequat=morbi&dui=non&nec=quam&nisi=nec&volutpat=dui&eleifend=luctus&donec=rutrum&ut=nulla&dolor=tellus', 2, 449, '2019-06-21', 'ATENDIDO', 17, NULL, 0),
(17, 10, '2019-08-19', 'Sapphire Piwall', 'https://phoca.cz/blandit/ultrices/enim/lorem.jpg?vivamus=vehicula&tortor=condimentum&duis=curabitur&mattis=in&egestas=libero&metus=ut&aenean=massa&fermentum=volutpat&donec=convallis&ut=morbi&mauris=odio&eget=odio&massa=elementum&tempor=eu&convallis=interdum&nulla=eu&neque=tincidunt&libero=in&convallis=leo&eget=maecenas&eleifend=pulvinar&luctus=lobortis&ultricies=est&eu=phasellus&nibh=sit&quisque=amet&id=erat&justo=nulla&sit=tempus&amet=vivamus&sapien=in&dignissim=felis&vestibulum=eu&vestibulum=sapien&ante=cursus&ipsum=vestibulum&primis=proin&in=eu&faucibus=mi&orci=nulla', 2, 313, '2019-07-27', 'ATENDIDO', 4, 10, 0),
(19, NULL, '2020-01-02', 'Catina Dysert', 'https://yale.edu/adipiscing.json?dui=potenti&vel=cras&nisl=in&duis=purus&ac=eu&nibh=magna&fusce=vulputate&lacus=luctus&purus=cum&aliquet=sociis&at=natoque&feugiat=penatibus&non=et&pretium=magnis&quis=dis&lectus=parturient&suspendisse=montes&potenti=nascetur&in=ridiculus&eleifend=mus&quam=vivamus&a=vestibulum&odio=sagittis&in=sapien&hac=cum&habitasse=sociis&platea=natoque&dictumst=penatibus&maecenas=et&ut=magnis&massa=dis&quis=parturient&augue=montes&luctus=nascetur&tincidunt=ridiculus&nulla=mus&mollis=etiam&molestie=vel&lorem=augue&quisque=vestibulum&ut=rutrum&erat=rutrum&curabitur=neque&gravida=aenean&nisi=auctor&at=gravida&nibh=sem&in=praesent&hac=id&habitasse=massa&platea=id&dictumst=nisl&aliquam=venenatis&augue=lacinia&quam=aenean&sollicitudin=sit&vitae=amet', 2, 254, '2019-03-20', 'NO ATENDIDO', 102, NULL, 0),
(47, NULL, '2020-01-29', 'Jose Rojas Peralta', 'https://www.youtube.com', 2, 19, '2020-01-29', 'NO ATENDIDO', 102, NULL, 0),
(48, NULL, '2020-02-21', 'El pescadito 2', 'https://www.youtube.com', 2, 19, '2020-01-29', 'ATENDIDO', 102, NULL, 0),
(49, NULL, '2020-02-21', 'El pescadito', 'https://messages/t/lalito.ortizportuguez', 2, 85, '2020-01-29', 'ATENDIDO', 102, NULL, 0),
(50, NULL, '2020-02-22', 'Jose Rojas Peralta', 'https://www.youtube.com', 1, 58, '2020-01-30', 'ATENDIDO', 102, NULL, 0),
(51, NULL, '2020-02-22', 'El pescadito', 'https://www.facebook.com/messages/t/lalito.ortizportuguez', 2, 21, '2020-01-30', 'ATENDIDO', 102, NULL, 0),
(52, 68, '2020-01-30', 'gOSHEN', 'https://www.youtube.com', 2, 18, '2020-01-30', 'ATENDIDO', 15, 68, 0),
(53, NULL, '2020-01-31', 'Despacito 4', 'https://www.youtube.com', 2, 19, '2020-01-31', 'ATENDIDO', 26, 77, 0),
(54, NULL, '2020-01-31', 'Eduardo', 'https://www.youtube.com/watch?v=rLj3hkPSaAk&t=4s', 2, 5, '2020-01-31', 'ATENDIDO', 27, NULL, 0),
(55, NULL, '2020-02-22', 'Prueba 2', 'https://www.youtube.com/watch?v=rLj3hkPSaAk&t=4s', 2, 5, '2020-01-31', 'ATENDIDO', 102, NULL, 0),
(56, NULL, '2020-01-31', 'Jose Rojas ', 'https://www.youtube.com', 2, 5, '2020-01-31', 'ATENDIDO', 27, NULL, 0),
(57, NULL, '2020-01-31', 'El pescadito 2', 'https://www.youtube.com', 2, 5, '2020-01-31', 'ATENDIDO', 27, NULL, 0),
(58, 60, '2020-02-01', 'eL MORADITO', 'https://www.youtube.com', 2, 100, '2020-02-01', 'ATENDIDO', 9, 60, 0),
(59, NULL, '2020-02-21', 'eL MORADITO', 'https://www.youtube.com', 2, 500, '2020-02-01', 'ATENDIDO', 102, NULL, 0),
(60, 7, '2020-02-01', 'Yo', 'https://www.youtube.com', 2, 50, '2020-02-01', 'ATENDIDO', 20, 7, 0),
(64, NULL, '2020-02-02', 'prueba lol', 'https://www.youtube.com', 2, 100, '2020-02-02', 'ATENDIDO', 26, 78, 0),
(65, NULL, '2020-02-04', 'soyuna prueba', 'https://www.youtube.com', 2, 100, '2020-02-04', 'ATENDIDO', 28, 71, 0),
(72, 88, '2020-02-11', 'asdas', 'asdad', 1, 50, '2020-02-11', 'ATENDIDO', 15, 88, 1),
(73, NULL, '2020-02-11', 'prueba', 'https://www.youtube.com', 2, 50, '2020-02-11', 'ATENDIDO', 25, 80, 1),
(74, NULL, '2020-02-11', 'hhhhhh', 'www.a.com', 2, 10, '2020-02-11', 'ATENDIDO', 16, 70, 1),
(75, NULL, '2020-02-11', 'hhhhrf', 'www.a.com', 2, 50, '2020-02-11', 'ATENDIDO', 25, 82, 1),
(76, NULL, '2020-02-11', 'OSHEN', 'https://www.youtube.com', 2, 50, '2020-02-11', 'ATENDIDO', 18, 83, 1),
(77, 77, '2020-02-11', 'Jose Peralta', 'https://www.youtube.com', 2, 520, '2020-02-11', 'ATENDIDO', 26, 77, 0),
(78, 78, '2020-02-11', 'eL MORADITO', 'https://www.youtube.com', 2, 520, '2020-02-11', 'ATENDIDO', 26, 78, 0),
(81, 5, '2020-02-12', 'nuevito', 'https://www.youtube.com', 2, 50, '2020-02-12', 'ATENDIDO', 2, 5, 1),
(82, 79, '2020-02-12', 'nuevisimo', 'https://www.youtube.com', 2, 50, '2020-02-12', 'ATENDIDO', 9, 79, 1),
(85, 88, '2020-02-15', 'Lalo Ortiz', 'https://www.youtube.com', 2, 50, '2020-02-15', 'ATENDIDO', 15, 88, 1),
(86, 52, '2020-02-20', 'jairo', 'https://www.youtube.com/watch?v=fE6hag8n7d0', 2, 0, '2020-02-20', 'ATENDIDO', 27, 52, 0),
(87, NULL, '2020-02-20', 'junior', '', 1, 0, '2020-02-20', 'ATENDIDO', 27, 55, 0),
(88, NULL, '2020-02-20', 'Jr', 'https://www.youtube.com/watch?v=fE6hag8n7d0', 2, 0, '2020-02-20', 'NO ATENDIDO', 27, NULL, 0),
(89, NULL, '2020-02-20', 'Jairo Jr.', 'https://www.youtube.com/watch?v=neUp7FQAlNE', 2, 0, '2020-02-20', 'NO ATENDIDO', 27, NULL, 0),
(90, NULL, '2020-02-26', 'SoyunPrueba3', 'www.youtube.com', 2, 50, '2020-02-26', 'NO ATENDIDO', 9, NULL, 0),
(91, NULL, '2020-02-26', 'SoyunPrueba2', 'www.youtube.com', 2, 50, '2020-02-26', 'NO ATENDIDO', 15, NULL, 0),
(92, NULL, '2020-02-26', 'SoyunPrueba', 'www.youtube.com', 2, 20, '2020-02-26', 'NO ATENDIDO', 26, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `idCuenta` int(11) NOT NULL,
  `cuenta_idPadre` int(11) DEFAULT NULL,
  `rango` varchar(10) NOT NULL,
  `email` varchar(40) NOT NULL,
  `pswd` varchar(40) NOT NULL,
  `Fnacimiento` varchar(12) NOT NULL,
  `ventaPrincipal` int(5) NOT NULL,
  `ventaSecundario` int(5) NOT NULL,
  `nReseteos` int(5) NOT NULL,
  `ultimoReseteo` varchar(12) NOT NULL,
  `nPSN` varchar(40) NOT NULL,
  `precio` float NOT NULL,
  `precioVendido` float NOT NULL,
  `estadoCuenta` int(11) NOT NULL,
  `juego_idJuego` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`idCuenta`, `cuenta_idPadre`, `rango`, `email`, `pswd`, `Fnacimiento`, `ventaPrincipal`, `ventaSecundario`, `nReseteos`, `ultimoReseteo`, `nPSN`, `precio`, `precioVendido`, `estadoCuenta`, `juego_idJuego`) VALUES
(1, NULL, 'Adulto', 'pbruford0@wiley.com', 'U3O9Ri8R', '2019-04-01', 1, 0, 1, '2020-01-29', 'acarlesi0', 249, 0, 3, 11),
(4, NULL, 'Adulto', 'neveritt3@squarespace.com', 'vn3NxcRMkZ8', '2019-12-28', 1, 0, 1, '2020-01-29', 'ktebbitt3', 312, 0, 1, 19),
(5, NULL, 'Adulto', 'vhanning4@sourceforge.net', 'zSFhhxKd2', '2019-06-25', 1, 0, 0, ' ', 'acolisbe4', 454, 0, 3, 2),
(6, NULL, 'Adulto', 'sseymer5@shop-pro.jp', 'FjKFyDOtklK', '2019-12-04', 1, 0, 1, '2020-01-29', 'bsprott5', 318, 0, 1, 11),
(7, NULL, 'Adulto', 'mrogge6@webeden.co.uk', 'KGmxRpAwXb', '2019-08-01', 2, 0, 1, '2020-01-29', 'broxburgh6', 312, 0, 4, 20),
(9, NULL, 'Adulto', 'meliassen8@craigslist.org', 'Tld2g8akn', '2019-12-27', 0, 0, 0, ' ', 'hbresnahan8', 478, 0, 1, 12),
(10, NULL, 'Adulto', 'mmacfarland9@upenn.edu', 'LvzwqwbJhN', '2019-04-10', 2, 0, 1, '2020-01-22', 'dsyddie9', 336, 0, 4, 4),
(12, 5, 'Menor', 'saplin1@biblegateway.com', '16WSyg54', '2020-01-02', 0, 0, 0, ' ', 'htattersill1', 114, 0, 1, 8),
(15, 1, 'Menor', 'awilshere4@mlb.com', 'Jw49v5B1', '2019-11-11', 2, 0, 1, '2020-01-28', 'rpenberthy4', 413, 0, 4, 7),
(50, NULL, 'Adulto', 'lalitolalokura@hotmail.com', 'asdfasd', '2005-01-01', 1, 0, 1, '2020-01-02', 'asdad', 90, 0, 4, 2),
(52, 4, 'Menor', 'esteesnuevo@hotmail.com', '1234567890', '2005-01-01', 1, 0, 0, '', '123ju123', 19, 0, 3, 27),
(53, NULL, 'Adulto', 'lawliet.eop@gmail.com', 'asdq', '2005-01-01', 0, 0, 0, '', 'adwda10', 19, 0, 1, 27),
(54, 6, 'Menor', 'eduard.ortiz369@nuevo.com', '123456879', '2005-01-01', 0, 0, 0, '', 'quechu', 90, 0, 1, 27),
(55, NULL, 'Adulto', 'lalitol123456@hotmail.com', '000', '2005-01-01', 1, 0, 1, '2020-02-26', 'nico', 54, 0, 1, 27),
(59, 1, 'Menor', '123eop@gmail.com', 'ASDQ', '2005-01-01', 1, 0, 1, '2020-01-08', 'nico', 158, 0, 1, 2),
(60, 6, 'Menor', 'asdada@himail.com', 'aq1', '2005-01-01', 2, 0, 1, '', 'ad', 12, 100, 4, 9),
(61, NULL, 'Adulto', 'asdada@mail.com', 'aqw', '2005-01-01', 1, 0, 0, '', 'asda', 211, 0, 3, 9),
(62, NULL, 'Adulto', 'name@example.com', 'asd', '2005-01-01', 0, 0, 0, '', '', 0, 0, 1, 1),
(63, NULL, 'Adulto', 'lalitolalokura@mail.com', 'weqwe', '2005-01-01', 0, 0, 0, '', '', 0, 0, 1, 3),
(64, 53, 'Menor', 'kuro@hotmail.com', '12313', '2005-01-01', 0, 0, 0, '', '', 0, 0, 1, 1),
(65, NULL, 'Adulto', 'lalikura@hotmail.com', '1234567890', '2005-01-01', 2, 0, 1, '2020-01-08', 'lolito', 50, 0, 4, 6),
(66, NULL, 'Adulto', 'ortiz369@hotmail.com', '963852147', '2005-01-01', 2, 0, 1, '2020-01-02', '123ju123', 19, 0, 4, 13),
(67, NULL, 'Adulto', 'lalitolalokura@nuevo.com', '1234567890', '2005-01-01', 1, 0, 1, '2020-01-15', 'adwda10', 0, 0, 1, 5),
(68, NULL, 'Adulto', '369@hotmail.com', '963852147', '2005-01-01', 1, 0, 0, '', '123ju123', 18, 0, 3, 15),
(69, NULL, 'Adulto', 'nuevvoooeop@gmail.com', '123548', '2005-01-01', 0, 0, 0, '', 'asdad', 0, 0, 1, 7),
(70, NULL, 'Adulto', 'sdasd@nuevo.com', 'asd', '2005-01-01', 1, 0, 1, '2020-02-11', 'elpapu', 0, 0, 1, 16),
(71, 1, 'Menor', 'prueba1@gmail.com', '11', '2005-01-01', 1, 0, 0, '', '11lol', 50, 0, 3, 28),
(72, NULL, 'Adulto', 'prueba2@gmail.com', '22', '2005-01-01', 0, 0, 0, '', '22lol', 50, 0, 1, 28),
(73, NULL, 'Adulto', 'prueba3@gmail.com', '33', '2005-01-01', 0, 0, 0, '', '33lol', 50, 0, 1, 28),
(74, NULL, 'Adulto', 'prueba4@gmail.com', '44', '2005-01-01', 0, 0, 0, '', '44lol', 50, 0, 1, 28),
(75, NULL, 'Adulto', 'prueba5@gmail.com', '555', '2005-01-01', 0, 0, 0, '', '555lol', 50, 0, 1, 28),
(76, NULL, 'Adulto', 'lawlip@gmail.com', 'asd123', '2005-01-01', 0, 0, 0, '', '112gr', 50, 0, 1, 10),
(77, NULL, 'Adulto', 'lawli@gmail.com', '15615', '2005-01-01', 2, 0, 1, '2020-02-11', '51', 59, 0, 4, 26),
(78, NULL, 'Adulto', 'lawliet0@gmail.com', '000', '2005-01-01', 2, 0, 1, '2020-02-11', 'lolito', 50, 0, 4, 26),
(79, NULL, 'Adulto', 'kur0i@nuevisimo.com', '1235', '2005-01-01', 1, 0, 0, '', 'quechu', 25, 0, 3, 9),
(80, NULL, 'Adulto', 'lawliet00@gmail.com', '00', '2005-01-01', 1, 0, 1, '2020-02-11', 'nico', 50, 0, 1, 25),
(81, NULL, 'Adulto', 'asd@gmail.com', '1', '2005-01-01', 0, 0, 0, '', '1', 10, 0, 1, 16),
(82, NULL, 'Adulto', 'asd102@gmail.com', '000', '2005-01-01', 1, 0, 1, '2020-02-11', 'asd', 50, 0, 1, 25),
(83, NULL, 'Adulto', 'lal@hotmail.com', '000', '2005-01-01', 1, 0, 1, '2020-02-11', '213', 50, 0, 1, 18),
(85, NULL, 'Adulto', 'namenuevo@gmail.com', '121', '2005-01-01', 1, 0, 1, '2020-02-15', 'elpapu', 50, 0, 1, 29),
(86, NULL, 'Adulto', 'nuevo@gimail.com', '000', '2005-01-01', 1, 0, 0, '', 'elpapu', 50, 408, 3, 9),
(88, NULL, 'Adulto', 'pruebasuhey@gmail.com', '123', '2005-01-01', 2, 1, 1, '2020-02-15', 'quechu', 50, 50, 4, 15),
(89, NULL, 'Adulto', 'lalitolalo@hotmail.com', '123', '2005-01-01', 0, 0, 0, '', 'quechu', 50, 0, 1, 15),
(90, NULL, 'Adulto', 'esperandodesc@gmail.com', '1000', '2005-01-01', 0, 0, 0, '', 'asd11', 10, 0, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocuenta`
--

CREATE TABLE `estadocuenta` (
  `idEstado` int(11) NOT NULL,
  `nombreEstado` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estadocuenta`
--

INSERT INTO `estadocuenta` (`idEstado`, `nombreEstado`) VALUES
(1, 'DISPONIBLE'),
(2, 'ESPERANDO DESCARGA'),
(3, 'PETICIÓN DE RESETEO'),
(4, 'NO DISPONIBLE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `finanzas`
--

CREATE TABLE `finanzas` (
  `idFinanza` int(11) NOT NULL,
  `ingresos` float NOT NULL,
  `saldoComprado` float NOT NULL,
  `gastoPersonal` float NOT NULL,
  `gastoPublicidad` float NOT NULL,
  `gastoExtra` float NOT NULL,
  `total` float NOT NULL,
  `diaAnterior` int(11) DEFAULT NULL,
  `hastaHoy` float NOT NULL,
  `fechFinan` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `finanzas`
--

INSERT INTO `finanzas` (`idFinanza`, `ingresos`, `saldoComprado`, `gastoPersonal`, `gastoPublicidad`, `gastoExtra`, `total`, `diaAnterior`, `hastaHoy`, `fechFinan`) VALUES
(20, 950, 50, 150, 300, 450, 0, NULL, 0, '2020-02-01'),
(21, 1485, 60, 150, 100, 40, 1135, 20, 1135, '2020-02-02'),
(24, 1500, 100, 150, 150, 150, 950, 21, 2085, '2020-02-03'),
(25, 1490, 100, 100, 100, 100, 1090, 24, 3175, '2020-02-04'),
(26, 0, 20, 20, 10, 50, -100, 25, 3075, '2020-02-05'),
(27, 0, 0, 0, 0, 0, 0, 26, 3075, '2020-02-06'),
(28, 0, 0, 0, 0, 0, 0, 27, 3075, '2020-02-07'),
(29, 0, 0, 0, 0, 0, 0, 28, 3075, '2020-02-08'),
(30, 0, 0, 0, 0, 0, 0, 29, 3075, '2020-02-09'),
(31, 0, 0, 0, 0, 0, 0, 30, 3075, '2020-02-10'),
(32, 5, 0, 0, 0, 0, 0, 31, 3080, '2020-02-11'),
(33, 189, 0, 0, 0, 0, 189, 32, 3269, '2020-02-20'),
(34, 19, 0, 0, 0, 0, 19, 33, 3288, '2020-02-21'),
(35, 120, 0, 0, 0, 0, 120, 34, 3408, '2020-02-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juego`
--

CREATE TABLE `juego` (
  `idJuego` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `juego`
--

INSERT INTO `juego` (`idJuego`, `nombre`) VALUES
(1, 'Sin Juego'),
(2, 'Trilia'),
(3, 'Eabox'),
(4, 'Browsedrive'),
(5, 'Devpoint'),
(6, 'Yata'),
(7, 'Leenti'),
(8, 'Pixope'),
(9, 'Dabtype'),
(10, 'Blogtags'),
(11, 'Roodel'),
(12, 'Skyba'),
(13, 'Yamia'),
(14, 'Skiba'),
(15, 'Demivee'),
(16, 'Mymm'),
(17, 'Zoonder'),
(18, 'Oodoo'),
(19, 'Zoombox'),
(20, 'Jetwire'),
(25, 'Jump Force'),
(26, 'Death Stranding'),
(27, 'God of War III'),
(28, 'laptmr'),
(29, 'nuevojuego'),
(30, 'otronuevo'),
(102, 'PS Plus');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metpagos`
--

CREATE TABLE `metpagos` (
  `idPago` int(11) NOT NULL,
  `nombreMP` varchar(35) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `metpagos`
--

INSERT INTO `metpagos` (`idPago`, `nombreMP`) VALUES
(1, '---'),
(2, 'Paypal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodeventa`
--

CREATE TABLE `tipodeventa` (
  `idTipoVenta` int(11) NOT NULL,
  `nombreVenta` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipodeventa`
--

INSERT INTO `tipodeventa` (`idTipoVenta`, `nombreVenta`) VALUES
(0, 'Principal'),
(1, 'Secundaria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `user` varchar(30) NOT NULL,
  `pswd` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `user`, `pswd`) VALUES
(1, 'admin111', 'admin000'),
(2, 'admin', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`),
  ADD KEY `idCuenta` (`cuenta_idCuenta`,`metpagos_idPago`,`juego_idJuego`),
  ADD KEY `juego_idJuego` (`juego_idJuego`),
  ADD KEY `metpagos_idPago` (`metpagos_idPago`),
  ADD KEY `tipoVenta` (`tVenta_idtVenta`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`idCuenta`),
  ADD KEY `cuenta_idPadre` (`cuenta_idPadre`,`juego_idJuego`),
  ADD KEY `juego_idJuego` (`juego_idJuego`),
  ADD KEY `estadoCuenta` (`estadoCuenta`);

--
-- Indices de la tabla `estadocuenta`
--
ALTER TABLE `estadocuenta`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indices de la tabla `finanzas`
--
ALTER TABLE `finanzas`
  ADD PRIMARY KEY (`idFinanza`),
  ADD KEY `diaAnterior` (`diaAnterior`);

--
-- Indices de la tabla `juego`
--
ALTER TABLE `juego`
  ADD PRIMARY KEY (`idJuego`);

--
-- Indices de la tabla `metpagos`
--
ALTER TABLE `metpagos`
  ADD PRIMARY KEY (`idPago`);

--
-- Indices de la tabla `tipodeventa`
--
ALTER TABLE `tipodeventa`
  ADD PRIMARY KEY (`idTipoVenta`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `idCuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT de la tabla `estadocuenta`
--
ALTER TABLE `estadocuenta`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `finanzas`
--
ALTER TABLE `finanzas`
  MODIFY `idFinanza` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `juego`
--
ALTER TABLE `juego`
  MODIFY `idJuego` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `metpagos`
--
ALTER TABLE `metpagos`
  MODIFY `idPago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipodeventa`
--
ALTER TABLE `tipodeventa`
  MODIFY `idTipoVenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`juego_idJuego`) REFERENCES `juego` (`idJuego`),
  ADD CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`metpagos_idPago`) REFERENCES `metpagos` (`idPago`),
  ADD CONSTRAINT `cliente_ibfk_3` FOREIGN KEY (`cuenta_idCuenta`) REFERENCES `cuenta` (`idCuenta`),
  ADD CONSTRAINT `cliente_ibfk_4` FOREIGN KEY (`tVenta_idtVenta`) REFERENCES `tipodeventa` (`idTipoVenta`);

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `cuenta_ibfk_1` FOREIGN KEY (`juego_idJuego`) REFERENCES `juego` (`idJuego`),
  ADD CONSTRAINT `cuenta_ibfk_2` FOREIGN KEY (`cuenta_idPadre`) REFERENCES `cuenta` (`idCuenta`),
  ADD CONSTRAINT `cuenta_ibfk_3` FOREIGN KEY (`estadoCuenta`) REFERENCES `estadocuenta` (`idEstado`);

--
-- Filtros para la tabla `finanzas`
--
ALTER TABLE `finanzas`
  ADD CONSTRAINT `finanzas_ibfk_1` FOREIGN KEY (`diaAnterior`) REFERENCES `finanzas` (`idFinanza`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
