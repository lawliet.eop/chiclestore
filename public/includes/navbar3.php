<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">ChicleStore Admin</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Nav Item - Cuentas -->
  <li class="nav-item <?php if($page == 'cuentas'){echo 'active';}?>">
    <a class="nav-link" href="cuentas.php">
      <i class="fas fa-address-card"></i>
      <span>Cuentas</span></a>
  </li>

  <!-- Nav Item - Cuentas Disponibles -->
  <li class="nav-item dropright <?php if($page == 'cuentasd'){echo 'active';}?> ">
    <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="far fa-address-card"></i>
      <span>Cuentas Disponibles</span></a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="cuentas-disp-p.php" >Principales</a>
        <a class="dropdown-item" href="cuentas-disp-s.php">Secundarias</a>
      </div>
  </li>

  <!-- Nav Item - Ventas -->
  <li class="nav-item <?php if($page == 'ventas'){echo 'active';}?>" >
    <a class="nav-link" href="clientes.php">
      <i class="fas fa-address-book"></i>
      <span>Ventas</span></a>
  </li>

  <!-- Nav Item - Juegos por comprar -->
  <li class="nav-item <?php if($page == 'comprar'){echo 'active';}?>">
    <a class="nav-link" href="comprar.php">
      <i class="fab fa-playstation"></i>
      <span>Juegos por Comprar</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
