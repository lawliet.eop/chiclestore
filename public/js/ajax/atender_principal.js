function modal_atender_principal(idCliente,idJuego,estadoCuenta,nombreJuego,tipoVenta) {
    console.log("Los datos a atender son: idCliente="+idCliente+" idJuego="+idJuego+" estadoCuenta="+estadoCuenta);
    //envio al servidor para buscar una cuenta
    $.ajax({
        url:'buscar_cuenta.php',
        type:'POST',
        data:{idJuego,estadoCuenta},
        success:function (response){
            let r = JSON.parse(response);
            console.log("Server dice: "+r.server);

            if (r.server == 'exito'){
                //pintar los datos de la cuenta a vender en el modal
                $('#VP_email'+idCliente).html('');
                let html = `<p>
                                <strong>Juego: </strong>${nombreJuego}<br>
                                <strong>Email: </strong>${r.email_cuenta}<br>
                                <strong>Password: </strong>${r.pswd_cuenta}
                            </p>`;
                $('#VP_datos_cuenta'+idCliente).html(html);
                let inputCopiar = `Juego: ${nombreJuego} Email: ${r.email_cuenta} Password: ${r.pswd_cuenta}`;
                $('#myInput'+idCliente).attr('value',inputCopiar);
                //boton para enviar al server
                $('#si'+idCliente).attr('onclick',`atender_P(${idCliente},'${r.email_cuenta}',${tipoVenta})`);
                //lanza el modal de venta
                $('#modal_VP_'+idCliente).modal('show');

            }else if (r.server == 'fallo'){
                //lanza el modal de fallo
                console.log("ocurrio un falloe al vender - recarge la pagina pls");
                $('#modal_error_venta').modal('show');
                /*
                setTimeout(function () {
                    $('#atencion_correcta').modal('hide');
                },'2000');*/
            }

            console.log("la respuesta del server es = "+response);
        }
    });
}

function atender_P(idCliente,emailCuenta,tipoVenta) {
    console.log("Se atendera pronto");
    console.log("llegaron estos datos para el boton si: idCliente="+idCliente+" email="+emailCuenta)
    $.ajax({
        url:'relacion-cta-clte.php',
        type:'POST',
        data:{idCliente,emailCuenta,tipoVenta},
        success:function (response){

            //++++++++++++

            let r = JSON.parse(response);
            if (r.estado_cuenta == 'VENTA FINALIZADA'){
                //pintar el estado de la cuenta
                let html = `<a id=\"estado_cuenta${idCliente}\" href= \"#\" class=\"btn btn-dark\" style=\"font-size: 12px;\" onclick=\"venta_finalizada(${idCliente},'${r.nombre_cliente}','${emailCuenta}','${r.fecha_atencion}')\">
                            <strong>VENTA FINALIZADA</strong>
                        </a>`;
                $('#venta_principal'+idCliente).html(html);
                //pintar el estado de atencion
                let html2 = `<a id=\"estado_atencion${idCliente}\" class=\"btn btn-success \" style=\"color:white; font-size: 12px;\">
                                <strong>ATENDIDO<strong>
                             </a>`;
                $('#td_estado_atencion'+idCliente).html(html2);
                //cierra el modal de venta
                $('#modal_VP_'+idCliente).modal('hide');
                //muestra modal de exito
                $('#atencion_correcta').modal('show');
                //cierra el modal de exito
                setTimeout(function () {
                    $('#atencion_correcta').modal('hide');
                },'2000');
            }else if (r.estado_cuenta == 'ESPERANDO DESCARGA'){
                //pintar el estado de la cuenta
                let html = `<a href= \"#\" id=\"esperando_descarga${idCliente}\" class=\"btn btn-danger\" style=\"font-size: 12px;\" onclick=\"estado_cuenta_ED(${idCliente},'${r.nombre_cliente}','${emailCuenta}','${r.fecha_atencion}')\">
                                <strong>ESPERANDO DESCARGA</strong>
                            </a >`;
                $('#venta_principal'+idCliente).html(html);
                //pintar el estado de atencion
                let html2 = `<a id=\"estado_atencion${idCliente}\" class=\"btn btn-success \" style=\"color:white; font-size: 12px;\">
                                <strong>ATENDIDO<strong>
                             </a>`;
                $('#td_estado_atencion'+idCliente).html(html2);
                //cierra el modal de venta
                $('#modal_VP_'+idCliente).modal('hide');
                //muestra modal de exito
                $('#atencion_correcta').modal('show');
                //cierra el modal de exito
                setTimeout(function () {
                    $('#atencion_correcta').modal('hide');
                },'2000');
            }

            console.log("la respuesta del server es = "+response);


            //+++++++++++

        }
    });

}