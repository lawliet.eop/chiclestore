function atender_plus(nombreCliente,idCliente,monto) {
        //se activa el modal
        $('#psPLUS_cliente').html(nombreCliente);
        $('#atender_ps_plus').modal('show');
        $('#vender_plus').attr('onclick',`vender_plus(${idCliente},${monto})`);
        console.log("Atender a: "+nombreCliente+" Con el id="+idCliente+" monto="+monto+" ?");

}
function vender_plus(idCliente,monto) {
        console.log("llego estos datos:"+"idCliente"+idCliente+" monto="+monto)
        $.ajax({
                url:'atender_ps_plus.php',
                type:'POST',
                data:{idCliente,monto},
                success:function (response){
                        let r = JSON.parse(response);
                        //cambia es estilo del boton de estado de atencion
                        $('#estado_atencion'+idCliente).attr('class',`btn btn-success`);
                        //cambia el texto del boton de estado de atencion
                        $('#estado_atencion'+idCliente).html(`<strong>ATENDIDO<strong>`);

                        //cambio en la clase del boton de estado de cuenta
                        $('#estado_cuenta'+idCliente).attr('class','btn btn-dark');
                        $('#estado_cuenta'+idCliente).attr('onclick',`venta_finalizada(${idCliente},'${r.nombre_cliente}','','${r.fecha_atencion}')`);
                        $('#estado_cuenta'+idCliente).html(`<strong>VENTA FINALIZADA</strong>`);
                        $('#atender_ps_plus').modal('hide');
                        $('#atencion_correcta').modal('show');
                        setTimeout(function () {
                                $('#atencion_correcta').modal('hide');
                        },'2000');
                        console.log("Se vendio a id="+idCliente);
                }
        });

}