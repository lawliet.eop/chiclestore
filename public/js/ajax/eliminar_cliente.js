var forzar = 0;

function eliminar_cuenta(idCliente,estadoCliente='nada',nomCliente='nada') {
    if (nomCliente != 'nada'){
        //llego un nombre y estado como parametro
        if (estadoCliente == 'ATENDIDO'){
            $('#mensaje_eliminar').html(`¿Está seguro de eliminar al cliente <strong>${nomCliente}</strong>?`);
        }else {
            $('#mensaje_eliminar').html(`¿Está seguro de eliminar al cliente <strong>${nomCliente}</strong>?, este cliente aún <strong>NO HA SIDO ATENDIDO</strong>`);
        }
        $('#chkForzar').prop("checked", false);
        $('#chkForzar').attr('onclick',`cambiarforzado(this);`);
        $('#boton_eliminar').attr('onclick',`eliminar_cuenta(${idCliente})`);
        //activa el modal para preguntar antes de eliminar
        $('#eliminar').modal('show');
    }else {
        console.log("LLegaron solo dos parametros * "+idCliente+" "+forzar)
        //solo llego idCliente como parametro - y se envia al servidor para su eliminacion
        $('#eliminar').modal('hide');
        $('.modal-backdrop').css("display", "none");
        $.ajax({
            url:'eliminar-clte.php',
            type:'POST',
            data:{idCliente,forzar},
            success:function (response) {
                let r = JSON.parse(response);
                if (r.resultado == 'correcto'){
                    //se puede eliminar
                    $('#exampleModalLabel').html('¡Hecho!');
                    $('#texto').attr('class','toast-body');
                    $('#texto').html(`El cliente <strong>${r.nombre}</strong> se eliminó correctamente.`);
                    $('#estilo1').attr('class','toast-header bg-success text-white');
                    $('#estilo2').attr('class','fas fa-check-circle');
                    //borra la fila
                    var fila = document.getElementById('fila'+idCliente);
                    fila.parentNode.removeChild(fila);
                    $('#mensaje_eliminacion').css("display", "block");
                    $('#mensaje_eliminacion').toast({delay: 4000});
                    $('#mensaje_eliminacion').toast('show');
                    setTimeout(function(){$("#mensaje_eliminacion").css("display", "none");},5000);

                }else if (r.resultado == 'falla'){
                    //no se puede eliminar
                    $('#exampleModalLabel').html('¡Advertencia!');
                    $('#texto').attr('class','toast-body');
                    $('#texto').html(`<strong>${r.nombre}</strong> aún está asociado a <strong>${r.email}</strong>`);
                    $('#estilo1').attr('class','toast-header bg-warning text-white');
                    $('#estilo2').attr('class','fas fa-exclamation-triangle');
                    //muestra el modal
                    $('#mensaje_eliminacion').css("display", "block");
                    $('#mensaje_eliminacion').toast({delay: 4000});
                    $('#mensaje_eliminacion').toast('show');
                    setTimeout(function(){$("#mensaje_eliminacion").css("display", "none");},5000);
                }
            }
        });
        forzar = 0;
    }

}
