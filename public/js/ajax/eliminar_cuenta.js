function eliminar_cuenta(idCuenta) {
    console.log("La cuenta con id - "+idCuenta+" sera eliminada");
    $('#confirmar'+idCuenta).modal('hide');
    $('.modal-backdrop').css("display", "none");
    $.ajax({
        url:'eliminar-cta.php',
        type:'POST',
        data:{idCuenta},
        success:function (response) {
            let r = JSON.parse(response);
            if(r.resultado == 'correcto'){
              //se puede eliminar
              $('#exampleModalLabel').html('¡Hecho!');
              $('#texto').attr('class','toast-body');
              $('#texto').html(`La cuenta <strong>${r.email}</strong> se eliminó correctamente.`);
              $('#estilo1').attr('class','toast-header bg-success text-white');
              $('#estilo2').attr('class','fas fa-check-circle');
              //borra la fila
                var fila = document.getElementById('fila'+idCuenta);
                fila.parentNode.removeChild(fila);
                $('#eliminando').css("display", "block");
                $('#eliminando').toast({delay: 4000});
                $('#eliminando').toast('show');
                setTimeout(function(){$("#eliminando").css("display", "none");},5000);
            }else if (r.resultado == 'con hijo'){
                //no se puede eliminar xq tiene hijo(s)
                $('#exampleModalLabel').html('¡Advertencia!');
                $('#texto').attr('class','toast-body');
                $('#texto').html(`Cuenta <strong>${r.email}</strong> aún asociado a <strong>${r.hijos}</strong> hijo(s).`);
                $('#estilo1').attr('class','toast-header bg-warning text-white');
                $('#estilo2').attr('class','fas fa-exclamation-triangle');
                $('#eliminando').css("display", "block");
                $('#eliminando').toast({delay: 4000});
                $('#eliminando').toast('show');
                setTimeout(function(){$("#eliminando").css("display", "none");},5000);
            }else if (r.resultado == 'con cliente'){
                //no se puede eliminar xq tiene cliente
                $('#exampleModalLabel').html('¡Advertencia!');
                $('#texto').attr('class','toast-body');
                $('#texto').html(`Cuenta <strong>${r.email}</strong> aún asociado a <strong>${r.cliente}</strong>.`);
                $('#estilo1').attr('class','toast-header bg-warning text-white');
                $('#estilo2').attr('class','fas fa-exclamation-triangle');
                //$('#M_email').html(r.email);
                //$('#M_cliente').html(r.cliente);
                $('#eliminando').css("display", "block");
                $('#eliminando').toast({delay: 4000});
                $('#eliminando').toast('show');
                setTimeout(function(){$("#eliminando").css("display", "none");},5000);
            }
            console.log("La respuesta del servidor es= "+response);

            //console.log(response);
        }
    })
}
