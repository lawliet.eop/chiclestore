function eliminar_usuario(idUsuario) {
    console.log("El usuario con id - "+idUsuario+" sera eliminada");
    $.ajax({
        url:'eliminar-user.php',
        type:'POST',
        data:{idUsuario},
        success:function (response) {
            let r = JSON.parse(response);
            if(r.resultado == 'correcto'){
                //sera eliminado
                $('#confirmar'+idUsuario).remove();
                $('.modal-backdrop').remove();
                var fila = document.getElementById('fila'+idUsuario);
                fila.parentNode.removeChild(fila);
                $('#eliminado').css("display", "block");
                $('#eliminado').toast({delay: 4000});
                $('#eliminado').toast('show');
                setTimeout(function(){$("#eliminado").css("display", "none");},5000);
            }
            console.log("La respuesta del servidor es= "+response);

            //console.log(response);
        }
    })
}
