function cambiarEstado(id,Nestado){
    let urlServer = '';
    let nuevoEstado = 0;
    if (Nestado == 'PETICIÓN DE RESETEO'){
        urlServer = 'ce-espdesc2.php';
        nuevoEstado = 3;
    }else if (Nestado == 'DISPONIBLE'){
        urlServer = 'ce-petreset.php';
        nuevoEstado = 1;
    }
    console.log("llegaron estos datos:"+id+"-"+Nestado);
    const postData = {
        id: id,
        nuevoEstado: nuevoEstado
    };

    $.post(urlServer,postData,function (response) {
        console.log(response);
        $('#confirmas'+id).modal('hide');
        if (Nestado == 'DISPONIBLE'){
            const R = JSON.parse(response);
            $('#nReseteos'+id).html(R.nReseteos);
            $('#ultimoReseteo'+id).html(R.FechaReseteo);
            $('#submit'+id).attr('class','btn btn-success');
            $('#submit'+id).attr('style','font-size: 12px; color:white;');
            $('#submit'+id).attr('data-toggle','');
            $('#submit'+id).attr('data-target','');
            $('#submit'+id).html('<strong>DISPONIBLE</strong>')
        }else if (Nestado == 'PETICIÓN DE RESETEO'){
            $('#submit'+id).attr('class','btn btn-info');
            $('#submit'+id).html('<strong>PETICIÓN DE RESETEO</strong>');
            let correo = $('#correo'+id).val();
            $('#pregunta'+id).html(`¿Está seguro de que la cuenta <strong>${correo}</strong> ha sido reseteada?`);

            let valor = `cambiarEstado(${id},'DISPONIBLE')`;
            $('#botonSi'+id).attr('onclick',valor);
        }
    });

}
