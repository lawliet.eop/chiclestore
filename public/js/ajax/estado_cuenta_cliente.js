function estado_cuenta_ED(idCliente,nombreCliente,cuentaComprada,fechaAtencion) {
    //lanza el modal
    console.log("cambiar a: "+"id="+idCliente+" nombre="+nombreCliente+" cuenta="+cuentaComprada);
    $('#ED_nombre_cliente').html(nombreCliente);
    $('#boton_ED').attr('onclick',`cambiarEstado_ED(${idCliente},'3','${nombreCliente}','${cuentaComprada}','${fechaAtencion}')`);
    $('#modal_esperando_descarga').modal('show');
}

function cambiarEstado_ED(idCliente,estadoNuevo,nombreCliente,cuentaComprada,fechaAtencion) {
    //envia al servidor y cambia el DOM
    $.ajax({
        url:'cambio_ED.php',
        type:'POST',
        data:{idCliente,estadoNuevo,cuentaComprada},
        success:function (response){
            console.log("se cambio a peticion de reseteo a la cuenta="+cuentaComprada);
            $('#esperando_descarga'+idCliente).attr('class','btn btn-dark');
            $('#esperando_descarga'+idCliente).attr('onclick',`venta_finalizada(${idCliente},'${nombreCliente}','${cuentaComprada}','${fechaAtencion}')`);
            $('#esperando_descarga'+idCliente).html(`<strong>VENTA FINALIZADA</strong>`);
            //oculta el modal anterior
            $('#modal_esperando_descarga').modal('hide');
            //cambia el modal de satisfacion - falta el envio al servidor
            $('#operacion_exitosa').html('La operacion se realizo Correctamente.');
            $('#atencion_correcta').modal('show');
            setTimeout(function () {
                $('#atencion_correcta').modal('hide');
            },'2000');
        }
    });
}