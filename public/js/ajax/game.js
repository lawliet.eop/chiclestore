$(function () {
    $('#formNewGame').submit(function (e) {
        const postData = {
            nameGame: $('#namegame').val()
        };
        $.post('addJuego.php',postData,function (response) {
            //console.log(response);
            $('#newgame').modal('hide');
            mostrarJuego();
        });
        e.preventDefault(); //cancelar el evento natural de recarga del form
    });
    function mostrarJuego() {
        let gameDefault;
        if ($('#game').val() == "Sin Juego"){
            gameDefault = '';
        }else {
            gameDefault = '<option>'+$('#game').val()+'</option>';
        }
        //console.log(gameDefault);
        $('#game').html('');
        $.ajax({
            url:'addJuego.php',
            type:'GET',
            success:function (response) {
                //console.log(response);
                let juegos = JSON.parse(response);
                let template = gameDefault;
                console.log(template);
                juegos.forEach(juego=>{
                    template += `<option>${juego.name}</option>`;
                });
                $('#game').html(template);
                //console.log(response);
            }
        });
    }
});