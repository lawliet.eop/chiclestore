$("form").submit(function (event) {
  var p1 = document.getElementById('pswd').value;
  var p2 = document.getElementById('pswd2').value;
    if (p1 == p2) {
      return;
    }else {
      document.getElementById('alert').innerHTML= '&nbsp&nbsp&nbspLas contraseñas no coinciden';
      event.preventDefault();
    }
});

function revelpass1(){
  document.getElementById('pswd').type = 'text';
  document.getElementById('ojito1').classList.remove('fa-eye');
  document.getElementById('ojito1').classList.add('fa-eye-slash');
  document.getElementById('revel1').onclick = norevelpass1;
}
function norevelpass1(){
  document.getElementById('pswd').type = 'password';
  document.getElementById('ojito1').classList.remove('fa-eye-slash');
  document.getElementById('ojito1').classList.add('fa-eye');
  document.getElementById('revel1').onclick = revelpass1;
}


function revelpass2(){
  document.getElementById('pswd2').type = 'text';
  document.getElementById('ojito2').classList.remove('fa-eye');
  document.getElementById('ojito2').classList.add('fa-eye-slash');
  document.getElementById('revel2').onclick = norevelpass2;
}

function norevelpass2(){
  document.getElementById('pswd2').type = 'password';
  document.getElementById('ojito2').classList.remove('fa-eye-slash');
  document.getElementById('ojito2').classList.add('fa-eye');
  document.getElementById('revel2').onclick = revelpass2;
}
