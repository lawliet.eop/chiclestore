<?php
    include 'conexion/conn.php';

    // variables de conexion
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    //password_hash('contrasena',PASSWORD_BCRYPT);
    //password_verify($pswd,$cifrado)

    $user = $_POST["user"];
    $pswd = $_POST["pswd"];
    //echo "pasword cifrada: ".$pswd_cifrada;

    $result2 = mysqli_query($conn, "SELECT * FROM `usuario` WHERE `user` = '$user'");

    if(mysqli_num_rows($result2) == 0){
        //si el usuario es falso
        @session_start();
        $_SESSION['fallo'] = "Usuario y/o Contraseña Incorrecto(s)";
        header("location: index.php");
    }else{
        $row = mysqli_fetch_assoc($result2);
        $pswd_cifrado = $row['pswd'];
        if (password_verify($pswd,$pswd_cifrado)){
            //la contraseña es correcta - el usuario existe
            @session_start();
            $_SESSION['ok'] = "ok";
            //los datos son correctos
            $rol = $row['rol_idRol'];
            //echo "el rol es: ".$rol;
            $url = '';
            switch ($rol){
                case 0:
                    $url = "location: Admin/actualizar_estados.php";
                    break;
                case 1:
                    $url = "location: Ventas/clientes.php";
                    break;
                case 2:
                    $url = "location: CuentasyCompras/cuentas.php";
                    break;
                default:
                    echo "Algo Malio Sal !!!";
                    break;
            }
            header($url);
        }else{
            //la contraseña es incorrecta - el usuario no existe
            @session_start();
            $_SESSION['fallo'] = "Usuario y/o Contraseña Incorrecto(s)";
            header("location: index.php");
        }

    }
