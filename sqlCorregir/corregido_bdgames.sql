-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-02-2020 a las 13:22:23
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `corregido_bdgames`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `cuenta_idCuenta` int(11) DEFAULT NULL,
  `fechAten` varchar(12) CHARACTER SET latin1 NOT NULL,
  `nomCliente` varchar(50) CHARACTER SET latin1 NOT NULL,
  `urlChat` text CHARACTER SET latin1 NOT NULL,
  `metpagos_idPago` int(11) NOT NULL,
  `monto` float NOT NULL,
  `fechDepo` varchar(12) CHARACTER SET latin1 NOT NULL,
  `estCliente` varchar(25) CHARACTER SET latin1 NOT NULL,
  `juego_idJuego` int(11) NOT NULL,
  `cuenta_Comprada` int(11) DEFAULT NULL,
  `tVenta_idtVenta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `cuenta_idCuenta`, `fechAten`, `nomCliente`, `urlChat`, `metpagos_idPago`, `monto`, `fechDepo`, `estCliente`, `juego_idJuego`, `cuenta_Comprada`, `tVenta_idtVenta`) VALUES
(58, NULL, '2020-01-30', 'Jeyson Daniel Figueroa Santa', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005882584132', 10, 29, '2020-01-25', 'ATENDIDO', 98, 282, 0),
(59, NULL, '2020-01-30', 'Henry Caro ', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100028515496660', 10, 119, '2020-01-27', 'ATENDIDO', 39, NULL, 0),
(60, 285, '2020-01-30', 'Fabriszio Manturano Inga', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001927118735', 10, 79, '2020-01-27', 'ATENDIDO', 40, 285, 0),
(62, NULL, '2020-01-30', 'Piero Sebastian Moret', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002420932163', 8, 39, '2020-01-28', 'ATENDIDO', 74, NULL, 0),
(63, NULL, '2020-01-30', 'Victor Valdivia Celis', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1569951508', 10, 39, '2020-01-28', 'ATENDIDO', 74, NULL, 0),
(64, NULL, '2020-01-30', 'Luis Yataco', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001263726733', 10, 34, '2020-01-28', 'ATENDIDO', 99, 294, 0),
(65, NULL, '2020-01-30', 'Christian Yovera', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011107177796', 10, 34, '2020-01-28', 'ATENDIDO', 100, 290, 0),
(66, 274, '2020-01-30', 'Christian Yovera', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011107177796', 10, 19, '2020-01-28', 'ATENDIDO', 47, 274, 0),
(67, 208, '2020-01-30', 'Giampiero Figueroa Mejorada', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100015446497671', 8, 14, '2020-01-29', 'ATENDIDO', 75, 208, 0),
(68, NULL, '2020-01-30', 'Leonardo Cesar', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003029312881', 10, 14, '2020-01-29', 'ATENDIDO', 75, 295, 0),
(69, 293, '2020-01-30', 'Yack Munguia', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003256997534', 10, 34, '2020-01-29', 'ATENDIDO', 101, 293, 0),
(70, NULL, '2020-01-30', 'Yack Munguia -DAR SI PIDE', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003256997534', 10, 49, '2020-01-29', 'NO ATENDIDO', 43, NULL, 0),
(76, NULL, '2020-01-31', 'Gerson Edward JuÃ¡rez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001443186224', 10, 14, '2020-01-31', 'ATENDIDO', 75, 296, 0),
(77, NULL, '2020-01-31', 'Eleazar Supo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1147208884', 11, 49, '2020-01-31', 'ATENDIDO', 73, 288, 0),
(78, NULL, '2020-01-31', 'Eleazar Supo -DAR SI PIDE', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1147208884', 11, 34, '2020-01-31', 'NO ATENDIDO', 100, NULL, 0),
(79, NULL, '2020-01-31', 'Carlos Carrillo Flores', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1674440523', 12, 14, '2020-01-16', 'ATENDIDO', 75, 297, 0),
(80, 84, '2020-01-31', 'Antonio Champac Quispe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1171396445', 12, 59, '2020-01-31', 'ATENDIDO', 48, 84, 0),
(82, NULL, '2020-01-31', 'Julio Villanueva', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001506786743', 12, 14, '2020-01-31', 'ATENDIDO', 75, NULL, 0),
(84, NULL, '2020-01-31', 'Joseph Sernaque Espinoza', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1447341667', 10, 5, '2020-01-30', 'ATENDIDO', 102, NULL, 0),
(85, NULL, '2020-01-31', 'Fantino Camara', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005048400109', 12, 5, '2020-01-31', 'ATENDIDO', 102, NULL, 0),
(86, NULL, '2020-01-31', 'Gustavo Adolfo MT', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011330443606', 12, 79, '2020-01-31', 'ATENDIDO', 40, 289, 0),
(87, 65, '2020-01-31', 'Claudio Lopez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100026589740577', 12, 49, '2020-01-31', 'ATENDIDO', 41, 65, 0),
(98, NULL, '2020-02-01', 'Bill Ãœ Pereyra', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007680723372', 12, 34, '2020-02-01', 'ATENDIDO', 47, 310, 0),
(99, 268, '2020-02-01', 'Roberto Carlos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002299785431', 12, 19, '2020-02-01', 'ATENDIDO', 47, 268, 0),
(100, 108, '2020-02-01', 'Antonio Champac Quispe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1171396445', 12, 34, '2020-02-01', 'ATENDIDO', 46, 108, 0),
(101, NULL, '2020-02-01', 'Adrian Rosillo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100028623621970', 11, 19, '2020-02-01', 'ATENDIDO', 47, NULL, 0),
(102, NULL, '2020-02-01', 'Gabriel Ramos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100027511471446', 12, 5, '2020-02-01', 'ATENDIDO', 102, NULL, 0),
(103, NULL, '2020-02-01', 'Patrick Sandoval Oliveira', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004545873736', 12, 5, '2020-02-01', 'ATENDIDO', 102, NULL, 0),
(104, NULL, '2020-02-01', 'Luiz Moreno Sangama', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1197165496', 11, 19, '2020-02-01', 'ATENDIDO', 47, NULL, 0),
(105, NULL, '2020-02-01', 'Michael Santa Cruz Soto', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001570043314', 11, 19, '2020-02-01', 'ATENDIDO', 47, 303, 0),
(106, NULL, '2020-02-01', 'Henry Caro', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100028515496660', 10, 79, '2020-02-01', 'ATENDIDO', 59, 304, 0),
(107, NULL, '2020-02-01', 'Jose Baluarte Galvez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003563201417', 10, 5, '2020-02-01', 'ATENDIDO', 102, NULL, 0),
(108, NULL, '2020-02-01', 'EstÃ©fano Castillo Armijos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002203597187', 8, 19, '2020-02-01', 'ATENDIDO', 47, 308, 0),
(109, NULL, '2020-02-01', 'Jose Cojal Guerra', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1784681423', 10, 19, '2020-02-01', 'ATENDIDO', 47, 309, 0),
(110, NULL, '2020-02-01', 'R MartÃ­n MorÃ³n Pereyra', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=732659543', 11, 79, '2020-02-01', 'ATENDIDO', 40, 307, 0),
(111, NULL, '2020-02-01', 'Cristhian Palacios', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001114346674', 12, 19, '2020-02-01', 'ATENDIDO', 47, 310, 0),
(112, NULL, '2020-02-01', 'Luis Iparraguirre Soberon', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001645806844', 11, 14, '2020-02-01', 'ATENDIDO', 75, 305, 0),
(113, NULL, '2020-02-01', 'Sebastian Murillo Guizado', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006134440027', 10, 5, '2020-02-01', 'ATENDIDO', 102, NULL, 0),
(114, NULL, '2020-02-01', 'Diego Llanos Marrufo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011487874513', 10, 19, '2020-02-01', 'ATENDIDO', 47, 311, 0),
(115, NULL, '2020-02-01', 'Eleazar Supo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1147208884', 10, 5, '2020-02-01', 'ATENDIDO', 102, NULL, 0),
(116, NULL, '2020-02-01', 'Kevin Tananta Alatrista', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100014958434537', 12, 19, '2020-02-01', 'ATENDIDO', 47, 312, 0),
(117, NULL, '2020-02-01', 'Albert Olivarez Vera', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001799499027', 8, 24, '2020-02-01', 'ATENDIDO', 103, 306, 0),
(118, 210, '2020-02-01', 'Jhon Rufasto Halire', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000945133847', 10, 14, '2020-02-01', 'ATENDIDO', 75, 210, 0),
(120, NULL, '2020-02-03', 'Christian Jesus', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007616291723', 12, 29, '2020-02-03', 'ATENDIDO', 104, 321, 0),
(121, NULL, '2020-02-03', 'Key Acevedo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003834113721', 12, 34, '2020-02-01', 'ATENDIDO', 100, 320, 0),
(122, 302, '2020-02-03', 'Ciro QR', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002297752372', 12, 19, '2020-02-02', 'ATENDIDO', 47, 302, 0),
(123, NULL, '2020-02-03', 'Miguel Rodriguez Layango', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003321265549', 12, 79, '2020-02-03', 'ATENDIDO', 40, 324, 0),
(130, NULL, '2020-02-03', 'Juan Salas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100014040857781', 10, 34, '2020-02-03', 'ATENDIDO', 100, 325, 0),
(131, NULL, '2020-02-03', 'Jorge Mestas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007860901313', 12, 5, '2020-02-03', 'ATENDIDO', 102, NULL, 0),
(132, 287, '2020-02-03', 'Adrian Rosillo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100028623621970', 11, 39, '2020-02-03', 'ATENDIDO', 74, 287, 0),
(133, 175, '2020-02-03', 'Andres Pedraza', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004090464440', 9, 19, '2020-02-03', 'ATENDIDO', 47, 175, 0),
(134, NULL, '2020-02-03', 'Alexander Palacios Gomez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100014556435156', 9, 49, '2020-02-03', 'ATENDIDO', 106, 323, 0),
(135, 325, '2020-02-03', 'Andre QuicaÃ±a', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000451918642', 10, 34, '2020-02-03', 'ATENDIDO', 100, 325, 0),
(136, NULL, '2020-02-03', 'Fabrizio Carrasco RamÃ­rez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100013926847711', 8, 5, '2020-02-03', 'ATENDIDO', 102, NULL, 0),
(137, NULL, '2020-02-03', 'Miguel Martinez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006388780220', 8, 5, '2020-02-03', 'ATENDIDO', 102, NULL, 0),
(138, NULL, '2020-02-03', 'Garcia Cordova Marcelo Erick', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100021658954355', 10, 39, '2020-02-03', 'ATENDIDO', 107, 319, 0),
(139, 324, '2020-02-03', 'Yosmel Diaz Ortega', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007095724767', 8, 79, '2020-02-03', 'ATENDIDO', 40, 324, 0),
(141, NULL, '2020-02-04', 'Johan Medina Izquierdo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002817189537', 10, 34, '2020-02-05', 'ATENDIDO', 79, 211, 0),
(142, NULL, '2020-02-05', 'Andre Vasquez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000011405326', 12, 79, '2020-02-05', 'ATENDIDO', 45, 88, 0),
(143, 300, '2020-02-05', 'Luis Rosario', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1095642353', 12, 39, '2020-02-05', 'ATENDIDO', 74, 300, 0),
(144, 271, '2020-02-05', 'Harvey Chesuaf RuÃ¬z Ilave', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007871283359', 9, 19, '2020-02-05', 'ATENDIDO', 47, 271, 0),
(145, 231, '2020-02-05', 'Jose Alonso Sanchez Tafur', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002916353351', 8, 34, '2020-02-05', 'ATENDIDO', 76, 231, 0),
(146, 114, '2020-02-05', 'Oscar P. F Ramos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006290610293', 10, 34, '2020-02-05', 'ATENDIDO', 46, 114, 0),
(147, 304, '2020-02-05', 'Gerson Marcelo Mendizabal', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009513415900', 12, 79, '2020-02-05', 'ATENDIDO', 59, 304, 0),
(148, 301, '2020-02-05', 'Rodrigo Solf Carrasco', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1159061350', 12, 19, '2020-02-05', 'ATENDIDO', 47, 301, 0),
(149, 308, '2020-02-05', 'Gustavo Adolfo MT', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011330443606', 12, 19, '2020-02-05', 'ATENDIDO', 47, 308, 0),
(150, 260, '2020-02-05', 'Karen Basilio', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100013308643507', 8, 14, '2020-02-05', 'ATENDIDO', 75, 260, 0),
(151, 108, '2020-02-05', 'Sebastian Machado', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100017542660963', 11, 34, '2020-02-03', 'ATENDIDO', 46, 108, 0),
(152, NULL, '2020-02-05', 'Jack Silva Narva', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003616701509', 10, 14, '2020-02-05', 'ATENDIDO', 75, 296, 0),
(153, NULL, '2020-02-05', 'Diego Oviedo Velasquez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006102404917', 12, 5, '2020-02-05', 'ATENDIDO', 102, NULL, 0),
(154, 289, '2020-02-05', 'Michael Salguero Cisniegas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000173575103', 11, 79, '2020-02-05', 'ATENDIDO', 40, 289, 0),
(155, 307, '2020-02-05', 'Luiis Christian Olivera', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004181740260', 8, 79, '2020-02-05', 'ATENDIDO', 40, 307, 0),
(156, 312, '2020-02-05', 'Frank Zamir Garcia', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001354822983', 8, 19, '2020-02-05', 'ATENDIDO', 47, 312, 0),
(157, 320, '2020-02-05', 'Roque Valencia Arredondo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1531632819', 8, 34, '2020-02-05', 'ATENDIDO', 100, 320, 0),
(158, NULL, '2020-02-05', 'FRancisco Osorio', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=718281369', 12, 14, '2020-02-05', 'ATENDIDO', 75, 305, 0),
(159, NULL, '2020-02-05', 'Cesar Cesar', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001379286309', 8, 39, '2020-02-05', 'ATENDIDO', 74, 322, 0),
(160, 309, '2020-02-05', 'Luis Alberto Santamaria Chavesta', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001670980768', 11, 19, '2020-02-05', 'ATENDIDO', 47, 309, 0),
(161, 296, '2020-02-05', 'Jack Vasquez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003051437427', 8, 14, '2020-02-05', 'ATENDIDO', 75, 296, 0),
(162, 157, '2020-02-05', 'George Brian', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1655975889', 12, 79, '2020-02-05', 'ATENDIDO', 45, 157, 0),
(163, NULL, '2020-02-05', 'Jefferson Cueto', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1183058797', 12, 34, '2020-02-05', 'ATENDIDO', 100, 290, 0),
(165, NULL, '2020-02-06', 'Raul Vera Nieto', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008954826980', 12, 79, '2020-02-06', 'ATENDIDO', 110, 341, 0),
(166, 295, '2020-02-06', 'Enzo Reyes Rodriguez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005899620265', 12, 14, '2020-02-06', 'ATENDIDO', 75, 295, 0),
(167, 233, '2020-02-06', 'Herlin LH', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003671991597', 8, 34, '2020-02-05', 'ATENDIDO', 76, 233, 0),
(169, NULL, '2020-02-06', 'Helthon Caballero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100016662813458', 11, 5, '2020-02-06', 'ATENDIDO', 102, NULL, 0),
(171, 305, '2020-02-06', 'Aaron Sarango', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000403211400', 8, 14, '2020-02-06', 'ATENDIDO', 75, 305, 0),
(172, NULL, '2020-02-06', 'Marco Vasquez Levano', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001308224917', 12, 5, '2020-02-06', 'ATENDIDO', 102, NULL, 0),
(173, 297, '2020-02-06', 'Romero Mego Marcelo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100023592979730', 10, 14, '2020-02-06', 'ATENDIDO', 75, 297, 0),
(174, 218, '2020-02-06', 'Bryan Martel Oliveira', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002479041798', 11, 44, '2020-02-06', 'ATENDIDO', 83, 218, 0),
(175, 64, '2020-02-06', 'Bryan Martel Oliveira', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002479041798', 11, 45, '2020-02-06', 'ATENDIDO', 37, 64, 0),
(176, 224, '2020-02-06', 'Bryan Martel Oliveira', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002479041798', 11, 34, '2020-02-06', 'ATENDIDO', 85, 224, 0),
(177, NULL, '2020-02-06', 'Nick Maldonado Aroni', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008830689144', 8, 34, '2020-02-06', 'ATENDIDO', 46, 342, 0),
(178, 290, '2020-02-06', 'HJ Urdiales', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1527841720', 8, 34, '2020-02-06', 'ATENDIDO', 100, 290, 0),
(179, 88, '2020-02-06', 'Percy Meneses', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1633385277', 8, 79, '2020-02-06', 'ATENDIDO', 45, 88, 0),
(180, NULL, '2020-02-06', 'Pierre Quispe Cuadros', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=778680843', 8, 5, '2020-02-06', 'ATENDIDO', 102, NULL, 0),
(181, 283, '2020-02-06', 'Axel QuiÃ±onez Tapia', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004946710487', 12, 59, '2020-02-06', 'ATENDIDO', 39, 283, 0),
(182, 221, '2020-02-07', 'Jose Valencia Alca', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001420927414', 8, 34, '2020-02-07', 'ATENDIDO', 84, 221, 0),
(183, NULL, '2020-02-07', 'CÃ©sar GarcÃ­a Delacruz', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009133839839', 10, 34, '2020-02-07', 'ATENDIDO', 46, 343, 0),
(184, 258, '2020-02-07', 'Gerson Franco Rosales', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1710561291', 8, 24, '2020-02-07', 'ATENDIDO', 92, 258, 0),
(185, NULL, '2020-02-07', 'Junior Caro', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1782784728', 8, 59, '2020-02-07', 'ATENDIDO', 40, 285, 0),
(186, NULL, '2020-02-07', 'Gabriel Mike Mendoza Dlc', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007781144467', 11, 5, '2020-02-07', 'ATENDIDO', 102, NULL, 0),
(187, NULL, '2020-02-07', 'Jean Marco Chinchay Molina', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009569243834', 12, 59, '2020-02-07', 'ATENDIDO', 40, 340, 0),
(188, NULL, '2020-02-07', 'Jose Jesus Sulca Uribe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002462738561', 8, 59, '2020-02-07', 'ATENDIDO', 39, 328, 0),
(189, NULL, '2020-02-07', 'Jordy Galea', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001848600196', 8, 59, '2020-02-07', 'ATENDIDO', 39, 329, 0),
(190, NULL, '2020-02-07', 'Diego Guevara', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004855756665', 8, 59, '2020-02-07', 'ATENDIDO', 39, 330, 0),
(191, NULL, '2020-02-07', 'Jean Pierre VÃ¡squez PÃ©rez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000980344894', 8, 59, '2020-02-07', 'ATENDIDO', 39, 331, 0),
(192, NULL, '2020-02-07', 'Giancarlo Facho Lerzundi', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1426653609', 11, 34, '2020-02-07', 'ATENDIDO', 46, 345, 0),
(193, NULL, '2020-02-07', 'Ricardo CoveÃ±as', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=643698974', 10, 59, '2020-02-07', 'ATENDIDO', 39, 332, 0),
(194, NULL, '2020-02-07', 'Ricardo CoveÃ±as', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=643698974', 10, 39, '2020-02-07', 'ATENDIDO', 38, 335, 0),
(195, NULL, '2020-02-07', 'Edhu CastaÃ±on Hernandez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000385037867', 12, 59, '2020-02-07', 'ATENDIDO', 39, 333, 0),
(196, 329, '2020-02-07', 'Jorge Marino', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1609037260', 10, 59, '2020-02-07', 'ATENDIDO', 39, 329, 0),
(197, 86, '2020-02-07', 'Renzo Guillermo Ccahuin SÃ¡nchez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004918816351', 10, 119, '2020-02-07', 'ATENDIDO', 49, 86, 0),
(198, 333, '2020-02-07', 'David Flores', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100027896776175', 10, 59, '2020-02-07', 'ATENDIDO', 39, 333, 0),
(199, NULL, '2020-02-07', 'Franz Astuvilca', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000602512839', 9, 49, '2020-02-07', 'ATENDIDO', 51, 334, 0),
(200, NULL, '2020-02-07', 'Daniel Huertas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001297758324', 12, 19, '2020-02-07', 'ATENDIDO', 37, 261, 0),
(201, NULL, '2020-02-08', 'Jahiro Huerta', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000885193799', 11, 59, '2020-02-07', 'ATENDIDO', 39, 338, 0),
(202, NULL, '2020-02-08', 'Yordham Gallardo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003480600936', 8, 5, '2020-02-08', 'ATENDIDO', 102, NULL, 0),
(203, NULL, '2020-02-08', 'David Romero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002467573987', 11, 59, '2020-02-08', 'ATENDIDO', 40, 339, 0),
(204, NULL, '2020-02-08', 'Renato Macarron', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1062835578', 10, 59, '2020-02-08', 'ATENDIDO', 39, 336, 0),
(205, NULL, '2020-02-08', 'Leo Medrano', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002164223497', 8, 59, '2020-02-08', 'ATENDIDO', 39, 337, 0),
(206, 340, '2020-02-08', 'Jose Luis Pacheco', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000736707471', 11, 59, '2020-02-08', 'ATENDIDO', 40, 340, 0),
(207, NULL, '2020-02-08', 'Alexander Estela', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002380360262', 12, 5, '2020-02-08', 'ATENDIDO', 102, NULL, 0),
(208, 285, '2020-02-08', 'Johan Medina Izquierdo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002817189537', 11, 59, '2020-02-08', 'ATENDIDO', 40, 285, 0),
(209, 339, '2020-02-08', 'Gerson Marcelo Mendizabal', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009513415900', 12, 59, '2020-02-08', 'ATENDIDO', 40, 339, 0),
(210, NULL, '2020-02-08', 'Fabrizzio Alvarado Otero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006888312322', 8, 34, '2020-02-08', 'ATENDIDO', 46, 346, 0),
(211, NULL, '2020-02-08', 'Esteban MA', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002369862887', 8, 5, '2020-02-08', 'ATENDIDO', 102, NULL, 0),
(212, NULL, '2020-02-08', 'Carlos Oblitas Cuba', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=513793232', 10, 59, '2020-02-08', 'ATENDIDO', 111, 344, 0),
(213, NULL, '2020-02-08', 'Omarion Noe Austin Jacobo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100027942132639', 8, 34, '2020-02-08', 'ATENDIDO', 46, 347, 0),
(214, NULL, '2020-02-08', 'Omarion Noe Austin Jacobo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100027942132639', 8, 34, '2020-02-08', 'ATENDIDO', 43, 299, 0),
(215, 336, '2020-02-08', 'Juver Carrasco Medina', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1484834449', 12, 59, '2020-02-08', 'ATENDIDO', 39, 336, 0),
(216, NULL, '2020-02-08', 'Alexis Sanchez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001742209690', 12, 5, '2020-02-08', 'ATENDIDO', 102, NULL, 0),
(217, NULL, '2020-02-08', 'Jopser German Rosales Barba', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100025083155727', 8, 5, '2020-02-08', 'ATENDIDO', 102, NULL, 0),
(218, NULL, '2020-02-08', 'Aki Higa', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001693110390', 8, 5, '2020-02-08', 'ATENDIDO', 102, NULL, 0),
(219, NULL, '2020-02-08', 'Emmanuel Alexander Dorante Salas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003440277616', 8, 59, '2020-02-08', 'ATENDIDO', 40, 352, 0),
(220, NULL, '2020-02-08', 'Bryan Apuc', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000016812396', 12, 49, '2020-02-08', 'ATENDIDO', 73, 365, 0),
(221, NULL, '2020-02-08', 'Marco DueÃ±as', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009214805860', 8, 59, '2020-02-08', 'ATENDIDO', 40, 353, 0),
(222, 124, '2020-02-08', 'Jefferson Oyola', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1532366127', 11, 49, '2020-02-08', 'ATENDIDO', 57, 124, 0),
(223, 337, '2020-02-08', 'Hollis Hershelo Palacios Elespuru', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008670186094', 10, 59, '2020-02-08', 'ATENDIDO', 39, 337, 0),
(224, 155, '2020-02-08', 'Hollis Hershelo Palacios Elespuru', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008670186094', 10, 59, '2020-02-08', 'ATENDIDO', 67, 155, 0),
(225, 350, '2020-02-08', 'Hollis Hershelo Palacios Elespuru', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008670186094', 10, 49, '2020-02-08', 'ATENDIDO', 112, 350, 0),
(226, 342, '2020-02-08', 'Hollis Hershelo Palacios Elespuru', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008670186094', 10, 34, '2020-02-08', 'ATENDIDO', 46, 342, 0),
(228, NULL, '2020-02-10', 'Steve Rocha Avelino', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&_rdc=1&_rdr&selected_item_id=100025648234618', 8, 59, '2020-02-09', 'ATENDIDO', 39, 354, 0),
(230, 331, '2020-02-10', 'AdriÃ¡n Ãlvarez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1812403141', 12, 59, '2020-02-10', 'ATENDIDO', 39, 331, 0),
(231, NULL, '2020-02-10', 'Jair Montes Yataco', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100014123534214', 11, 5, '2020-02-08', 'ATENDIDO', 102, NULL, 0),
(232, NULL, '2020-02-10', 'Jair Dipas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007386758609', 11, 59, '2020-02-10', 'ATENDIDO', 39, 355, 0),
(233, 328, '2020-02-10', 'Tonii Velasquez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1086318813', 8, 59, '2020-02-08', 'ATENDIDO', 39, 328, 0),
(234, 341, '2020-02-10', 'Sebastian Alonso YM', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002744400190', 8, 49, '2020-02-09', 'ATENDIDO', 110, 341, 0),
(235, NULL, '2020-02-10', 'â€ŽØ§ÙÙ„ØªÙˆÙ† ÙƒØ§Ø¨Ø§Ø¬ÙŠØ±Ùˆâ€Ž', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002060589544', 8, 5, '2020-02-09', 'ATENDIDO', 102, NULL, 0),
(236, NULL, '2020-02-10', 'Maykol Jordan CG', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100010945333745', 8, 59, '2020-02-10', 'ATENDIDO', 39, 356, 0),
(237, NULL, '2020-02-10', 'Yossimar Alvaro Ramirez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004780119634', 8, 59, '2020-02-10', 'ATENDIDO', 39, 361, 0),
(238, NULL, '2020-02-10', 'Stefano Javier Carbajal', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1431771639', 11, 49, '2020-02-10', 'ATENDIDO', 64, 351, 0),
(239, 261, '2020-02-10', 'Marcos Dmz', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=605108112', 12, 19, '2020-02-10', 'ATENDIDO', 37, 261, 0),
(240, 346, '2020-02-10', 'Nicolas Chirito Torres', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100022541100337', 10, 34, '2020-02-10', 'ATENDIDO', 46, 346, 0),
(241, 347, '2020-02-10', 'Christofer Chavarri', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003107202463', 11, 34, '2020-02-10', 'ATENDIDO', 46, 347, 0),
(242, NULL, '2020-02-10', 'Patrick Sandoval Oliveira', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004545873736', 12, 25, '2020-02-10', 'ATENDIDO', 60, 369, 0),
(243, NULL, '2020-02-10', 'Renato Guzman Torres', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1348902264', 12, 5, '2020-02-10', 'ATENDIDO', 102, NULL, 0),
(244, 343, '2020-02-10', 'Ivan Polar Rivero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1527497096', 11, 34, '2020-02-10', 'ATENDIDO', 46, 343, 0),
(245, 362, '2020-02-10', 'Katty Riveros Bavilon', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006867198614', 9, 60, '2020-02-10', 'ATENDIDO', 39, 362, 0),
(246, 209, '2020-02-10', 'Gerson Leandro Reyes Condori', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100010376649104', 9, 11, '2020-02-10', 'ATENDIDO', 78, 209, 0),
(247, NULL, '2020-02-10', 'Edson NuÃ±ez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001294669779', 8, 59, '2020-02-10', 'ATENDIDO', 39, 362, 0),
(248, NULL, '2020-02-10', 'Edson NuÃ±ez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001294669779', 8, 49, '2020-02-10', 'ATENDIDO', 113, 367, 0),
(249, NULL, '2020-02-10', 'Carlos CoveÃ±as Valladares', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001472003539', 8, 5, '2020-02-10', 'ATENDIDO', 102, NULL, 0),
(250, NULL, '2020-02-10', 'Cristhian R. Gianmarco', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1407862915', 8, 59, '2020-02-10', 'ATENDIDO', 39, 359, 0),
(251, 352, '2020-02-10', 'Antonio Sanchez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003583467000', 9, 59, '2020-02-10', 'ATENDIDO', 40, 352, 0),
(252, 353, '2020-02-10', 'Cesar Orbegoso Garrido', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1045298763', 10, 59, '2020-02-10', 'ATENDIDO', 40, 353, 0),
(253, NULL, '2020-02-10', 'Raid Chavez Buendia', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000965214165', 9, 39, '2020-02-10', 'ATENDIDO', 68, 165, 0),
(256, NULL, '2020-02-11', 'Frank Zamir Garcia', 'https://www.facebook.com/chiclestoreperu/inbox/2627179390670529/?notif_id=1581317782779394&notif_t=page_message&mailbox_id=264286250415069&selected_item_id=100001354822983', 8, 50, '2020-02-11', 'ATENDIDO', 80, 368, 0),
(257, NULL, '2020-02-11', 'Javier Tellez Salas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001929514770', 10, 59, '2020-02-10', 'ATENDIDO', 39, 360, 0),
(259, 330, '2020-02-11', 'Carlos Castellanos Quiroz', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=587597877', 9, 59, '2020-02-11', 'ATENDIDO', 39, 330, 0),
(261, NULL, '2020-02-11', 'Jose Alonso Sanchez Tafur', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002916353351', 9, 59, '2020-02-11', 'ATENDIDO', 39, 370, 0),
(262, NULL, '2020-02-11', 'Frank Paredes Zea', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008884716435', 12, 34, '2020-02-11', 'ATENDIDO', 59, 379, 0),
(263, 299, '2020-02-11', 'Cleiver Guerrero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000854324963', 8, 39, '2020-02-11', 'ATENDIDO', 43, 299, 0),
(264, NULL, '2020-02-11', 'Emmanuel Alexander Dorante Salas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003440277616', 8, 19, '2020-02-11', 'ATENDIDO', 78, 394, 0),
(265, NULL, '2020-02-11', 'Sergio Alejandro Julca Lopez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001114874318', 8, 59, '2020-02-11', 'ATENDIDO', 39, 363, 0),
(266, 338, '2020-02-11', 'Alvaro Marcel Hostiliano', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003190559271', 8, 59, '2020-02-11', 'ATENDIDO', 39, 338, 0),
(267, NULL, '2020-02-11', 'Luis Ruiz', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002115767465', 11, 59, '2020-02-11', 'ATENDIDO', 40, 375, 0),
(268, NULL, '2020-02-11', 'Luis Castilla', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003709094049', 10, 59, '2020-02-11', 'ATENDIDO', 40, 374, 0),
(269, 386, '2020-02-11', 'Paul Romero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1156260815', 11, 34, '2020-02-11', 'ATENDIDO', 100, 386, 0),
(270, NULL, '2020-02-11', 'Jean Pool Cruzado Callupe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001506271695', 8, 34, '2020-02-11', 'ATENDIDO', 38, 366, 0),
(272, 345, '2020-02-11', 'Marco Castillo Duran', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100028473785629', 8, 34, '2020-02-11', 'ATENDIDO', 46, 345, 0),
(273, NULL, '2020-02-11', 'Leonardo Cesar', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003029312881', 10, 59, '2020-02-11', 'ATENDIDO', 39, 364, 0),
(274, NULL, '2020-02-11', 'Jonh Condori Lapas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1065169039', 8, 50, '2020-02-11', 'ATENDIDO', 102, NULL, 0),
(275, NULL, '2020-02-11', 'Noviazgo VZ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100022365602842', 12, 59, '2020-02-11', 'ATENDIDO', 39, 355, 0),
(277, NULL, '2020-02-11', 'Jhampier Jose Cruz Chaipe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001732756013', 12, 60, '2020-02-11', 'ATENDIDO', 40, 372, 0),
(278, NULL, '2020-02-11', 'Diego Guerrero Rodriguez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100019745433031', 12, 5, '2020-02-11', 'ATENDIDO', 102, NULL, 0),
(279, NULL, '2020-02-12', 'Kevin Palomino', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004877763546', 11, 59, '2020-02-12', 'ATENDIDO', 40, 380, 0),
(280, 354, '2020-02-12', 'Mario Du Santos Ladines', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009673230974', 9, 59, '2020-02-12', 'ATENDIDO', 39, 354, 0),
(281, 133, '2020-02-12', 'Javier Quispe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000829369663', 8, 55, '2020-02-12', 'ATENDIDO', 61, 133, 0),
(282, NULL, '2020-02-12', 'Maberik Farfan Alata', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001820173613', 8, 79, '2020-02-12', 'ATENDIDO', 40, 376, 0),
(283, NULL, '2020-02-12', 'D Manuel CL', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001256750166', 11, 25, '2020-02-12', 'ATENDIDO', 114, 378, 0),
(284, 369, '2020-02-12', 'Carlos Alberto Taipe Figueroa', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001342652035', 8, 25, '2020-02-12', 'ATENDIDO', 60, 369, 0),
(285, NULL, '2020-02-12', 'Oscar P. F Ramos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006290610293', 10, 39, '2020-02-12', 'ATENDIDO', 115, 371, 0),
(286, NULL, '2020-02-12', 'Bornat MV', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100039134808079', 10, 40, '2020-02-12', 'ATENDIDO', 116, 377, 0),
(287, 385, '2020-02-12', 'Jose CA', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006900168173', 8, 59, '2020-02-12', 'ATENDIDO', 39, 385, 0),
(288, 360, '2020-02-12', 'Jarri Zarate Olaya', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100014759419457', 11, 59, '2020-02-12', 'ATENDIDO', 39, 360, 0),
(289, 361, '2020-02-12', 'Jean Pieer Anthony Quispe Gaspar', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004643619939', 12, 59, '2020-02-12', 'ATENDIDO', 39, 361, 0),
(290, NULL, '2020-02-12', 'Branco Padilla', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002392890519', 8, 50, '2020-02-12', 'ATENDIDO', 80, 389, 0),
(291, NULL, '2020-02-12', 'Noviazgo VZ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100022365602842', 12, 49, '2020-02-12', 'ATENDIDO', 110, 407, 0),
(293, NULL, '2020-02-13', 'Arnold Chavez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001525011738', 8, 19, '2020-02-12', 'NO ATENDIDO', 117, NULL, 0),
(294, 364, '2020-02-13', 'Memphis Barrueto', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100023461310682', 11, 59, '2020-02-12', 'ATENDIDO', 39, 364, 0),
(295, NULL, '2020-02-13', 'Gussepi Martinez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002549881797', 8, 34, '2020-02-12', 'ATENDIDO', 46, 393, 0),
(297, 303, '2020-02-13', 'Diego Leon', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1628712159', 8, 19, '2020-02-13', 'ATENDIDO', 47, 303, 0),
(298, NULL, '2020-02-13', 'Martin Sarango', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100010447320041', 10, 5, '2020-02-13', 'ATENDIDO', 102, NULL, 0),
(299, NULL, '2020-02-13', 'Manuel Hernandez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002667070764', 12, 34, '2020-02-13', 'ATENDIDO', 100, 246, 0),
(301, 311, '2020-02-13', 'Alex Pinillos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004341523804', 8, 19, '2020-02-13', 'ATENDIDO', 47, 311, 0),
(302, 374, '2020-02-13', 'Bryan Flores', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100024259923609', 12, 59, '2020-02-13', 'ATENDIDO', 40, 374, 0),
(303, NULL, '2020-02-13', 'Bryan Flores', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100024259923609', 12, 24, '2020-02-13', 'ATENDIDO', 60, 388, 0),
(304, NULL, '2020-02-13', 'Piero Alessandro', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005287278833', 8, 5, '2020-02-13', 'ATENDIDO', 102, NULL, 0),
(305, 294, '2020-02-13', 'Dan Ber', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006739234523', 8, 34, '2020-02-13', 'ATENDIDO', 99, 294, 0),
(306, NULL, '2020-02-13', 'Dan Ber', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006739234523', 8, 49, '2020-02-13', 'ATENDIDO', 73, 170, 0),
(307, NULL, '2020-02-13', 'Dan Ber', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006739234523', 8, 59, '2020-02-13', 'ATENDIDO', 40, 396, 0),
(308, NULL, '2020-02-13', 'Dan Ber', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006739234523', 8, 49, '2020-02-13', 'ATENDIDO', 80, 390, 0),
(309, NULL, '2020-02-13', 'Manuel Garcia', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007592568820', 10, 49, '2020-02-13', 'ATENDIDO', 57, 387, 0),
(310, NULL, '2020-02-13', 'Jermy Quispe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100041956421698', 12, 59, '2020-02-13', 'ATENDIDO', 39, 397, 0),
(311, NULL, '2020-02-13', 'Cristian Uc', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001431104661', 10, 49, '2020-02-13', 'ATENDIDO', 80, 391, 0),
(312, NULL, '2020-02-13', 'GloriaElvira NarbastaEspinal', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100016056788865', 12, 34, '2020-02-13', 'ATENDIDO', 46, 404, 0),
(313, NULL, '2020-02-13', 'Rmo Sebis', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001104758887', 8, 119, '2020-02-13', 'ATENDIDO', 52, 395, 0),
(314, NULL, '2020-02-13', 'Mathias Panta', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003341599817', 10, 39, '2020-02-13', 'ATENDIDO', 118, 400, 0),
(315, NULL, '2020-02-13', 'Carlos Alberto Taipe Figueroa', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001342652035', 8, 59, '2020-02-13', 'ATENDIDO', 40, 403, 0),
(316, NULL, '2020-02-13', 'Rolando Rivero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100021946639348', 12, 49, '2020-02-13', 'ATENDIDO', 43, 236, 0),
(317, NULL, '2020-02-13', 'Alex Fernandez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001870001624', 10, 89, '2020-02-13', 'ATENDIDO', 119, 402, 0),
(318, 396, '2020-02-13', 'Jhaecc CalderÃ³n Collazos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100024546136891', 9, 59, '2020-02-13', 'ATENDIDO', 40, 396, 0),
(319, NULL, '2020-02-13', 'Johan CalderÃ³n', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007171596750', 12, 59, '2020-02-13', 'ATENDIDO', 40, 422, 0),
(320, NULL, '2020-02-13', 'Manuel Sanchez Guillermo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001683596087', 8, 59, '2020-02-13', 'ATENDIDO', 39, 401, 0),
(321, NULL, '2020-02-13', 'Alonso Caceres', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003535463218', 12, 119, '2020-02-13', 'ATENDIDO', 52, 395, 0),
(322, NULL, '2020-02-13', 'Alonso Caceres', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003535463218', 12, 55, '2020-02-13', 'ATENDIDO', 56, 119, 0),
(325, 363, '2020-02-13', 'Ricardo Favio Oliva Flores', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002406789525', 8, 59, '2020-02-13', 'ATENDIDO', 39, 363, 0),
(326, NULL, '2020-02-13', 'Eitan Sandoval Goicochea', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007729420090', 8, 34, '2020-02-13', 'ATENDIDO', 46, 405, 0),
(327, NULL, '2020-02-13', 'Edson Sebastian', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003742593105', 12, 49, '2020-02-13', 'ATENDIDO', 53, 398, 0),
(328, NULL, '2020-02-13', 'Jhairs Flores', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007169822007', 10, 34, '2020-02-13', 'ATENDIDO', 46, 406, 0),
(329, NULL, '2020-02-13', 'Arturo Mendoza GastelÃº', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002072979668', 10, 55, '2020-02-13', 'ATENDIDO', 120, 409, 0),
(330, 372, '2020-02-13', 'Yarfe GC', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1054433139', 10, 59, '2020-02-13', 'ATENDIDO', 40, 372, 0),
(331, 319, '2020-02-13', 'David Vidal Viera', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003272187389', 12, 31, '2020-02-13', 'ATENDIDO', 107, 319, 0),
(332, NULL, '2020-02-13', 'Sebastian Iberico Fuentes', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004842941250', 8, 129, '2020-02-13', 'ATENDIDO', 124, 414, 0),
(333, NULL, '2020-02-13', 'Oscar Antonio Quezada Ruiz', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000041779518', 8, 5, '2020-02-13', 'ATENDIDO', 102, NULL, 0),
(334, 335, '2020-02-13', 'Alonso Caceres', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003535463218', 12, 34, '2020-02-13', 'ATENDIDO', 38, 335, 0),
(335, 334, '2020-02-13', 'Traian Vasile Paredes Mercado', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008328496046', 8, 45, '2020-02-13', 'ATENDIDO', 51, 334, 0),
(336, 380, '2020-02-13', 'Traian Vasile Paredes Mercado', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008328496046', 8, 59, '2020-02-13', 'ATENDIDO', 40, 380, 0),
(337, 359, '2020-02-13', 'Zoe Manzano Ponce', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100038023732498', 11, 66, '2020-02-13', 'ATENDIDO', 39, 359, 0),
(338, 332, '2020-02-13', 'Juan J Cor Enl', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005870767954', 11, 59, '2020-02-13', 'ATENDIDO', 39, 332, 0),
(339, NULL, '2020-02-13', 'David Valladares Fuero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007577022510', 10, 59, '2020-02-13', 'ATENDIDO', 65, 413, 0),
(340, NULL, '2020-02-13', 'Hemerson Howard', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003175088119', 12, 59, '2020-02-13', 'ATENDIDO', 122, 412, 0),
(341, NULL, '2020-02-14', 'Giordi Zevallos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004260990350', 10, 5, '2020-02-13', 'ATENDIDO', 102, NULL, 0),
(342, NULL, '2020-02-14', 'Enyel Benavente', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006199931232', 12, 5, '2020-02-14', 'ATENDIDO', 102, NULL, 0),
(343, 109, '2020-02-14', 'Patrick Sandoval Oliveira', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004545873736', 12, 39, '2020-02-14', 'ATENDIDO', 54, 109, 0);
INSERT INTO `cliente` (`idCliente`, `cuenta_idCuenta`, `fechAten`, `nomCliente`, `urlChat`, `metpagos_idPago`, `monto`, `fechDepo`, `estCliente`, `juego_idJuego`, `cuenta_Comprada`, `tVenta_idtVenta`) VALUES
(344, NULL, '2020-02-14', 'Patrick Sandoval Oliveira', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004545873736', 12, 34, '2020-02-14', 'ATENDIDO', 38, 410, 0),
(345, 265, '2020-02-14', 'Diego Jave', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011493200530', 12, 15, '2020-02-14', 'ATENDIDO', 94, 265, 0),
(346, 393, '2020-02-14', 'Hiroshi Takeda', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011791710323', 12, 34, '2020-02-14', 'ATENDIDO', 46, 393, 0),
(347, 356, '2020-02-14', 'Sebastian Santana', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100012169505444', 12, 59, '2020-02-14', 'ATENDIDO', 39, 356, 0),
(348, NULL, '2020-02-14', 'Sebastian Santana', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100012169505444', 12, 49, '2020-02-14', 'ATENDIDO', 53, 398, 0),
(349, NULL, '2020-02-14', 'Andres Flores', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011297083831', 8, 59, '2020-02-14', 'ATENDIDO', 123, 416, 0),
(350, NULL, '2020-02-14', 'Cristhian Ato', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005483731752', 8, 10, '2020-02-14', 'ATENDIDO', 102, NULL, 0),
(351, NULL, '2020-02-14', 'Dellanira CR', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000441965225', 10, 45, '2020-02-14', 'ATENDIDO', 51, 418, 0),
(352, 241, '2020-02-14', 'Kevin Jharley Vasquez Barboza', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004774087920', 8, 51, '2020-02-14', 'ATENDIDO', 88, 241, 0),
(353, 407, '2020-02-14', 'Junior Caro', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1782784728', 10, 49, '2020-02-14', 'ATENDIDO', 110, 407, 0),
(354, NULL, '2020-02-14', 'Sebastian Ponce de LeÃ³n', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001467313788', 8, 34, '2020-02-14', 'ATENDIDO', 46, 399, 0),
(355, NULL, '2020-02-14', 'Sebastian Ponce de LeÃ³n', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001467313788', 8, 5, '2020-02-14', 'ATENDIDO', 102, NULL, 0),
(356, NULL, '2020-02-14', 'Harold LobatÃ³n', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004027755503', 12, 59, '2020-02-14', 'ATENDIDO', 39, 419, 0),
(357, NULL, '2020-02-14', 'Patrick Azcarate', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011051250570', 10, 45, '2020-02-14', 'ATENDIDO', 51, 411, 0),
(358, 408, '2020-02-15', 'PRUEBA-CRISTIAN YOVERA', 'URLPRUEBA', 8, 0, '2020-02-15', 'ATENDIDO', 100, 408, 0),
(359, NULL, '2020-02-15', 'Raul Vera Nieto', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100008954826980', 12, 49, '2020-02-14', 'ATENDIDO', 81, 415, 0),
(360, 401, '2020-02-15', 'Gustavo Gutierrez Marin', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100026696403282', 10, 60, '2020-02-15', 'ATENDIDO', 39, 401, 0),
(361, 67, '2020-02-15', 'Miguel Angel VicuÃ±a', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=761432785', 10, 39, '2020-02-15', 'ATENDIDO', 42, 67, 0),
(362, 371, '2020-02-15', 'Miguel Angel VicuÃ±a', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=761432785', 10, 39, '2020-02-15', 'ATENDIDO', 115, 371, 0),
(363, NULL, '2020-02-15', 'Piero Christopher ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002453864136', 12, 34, '2020-02-15', 'ATENDIDO', 46, 404, 0),
(364, 405, '2020-02-15', 'Jhunior Norabuena', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003531894155', 10, 34, '2020-02-15', 'ATENDIDO', 46, 405, 0),
(365, NULL, '2020-02-15', 'Jhunior Norabuena', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003531894155', 10, 59, '2020-02-15', 'ATENDIDO', 40, 428, 0),
(366, NULL, '2020-02-15', 'Juan Diego Flores ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002442674153', 8, 60, '2020-02-15', 'ATENDIDO', 39, 397, 0),
(367, NULL, '2020-02-15', 'Rmo Sebis', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001104758887', 8, 15, '2020-02-15', 'ATENDIDO', 102, NULL, 0),
(368, NULL, '2020-02-15', 'Richard La Torre ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003758891010', 8, 59, '2020-02-15', 'ATENDIDO', 39, 419, 0),
(369, NULL, '2020-02-15', 'Bryan Arizanca ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005460969314', 12, 59, '2020-02-15', 'ATENDIDO', 40, 434, 0),
(370, NULL, '2020-02-15', 'Gerson CoveÃ±as', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000928272713', 8, 29, '2020-02-15', 'ATENDIDO', 91, 420, 0),
(371, NULL, '2020-02-15', 'Emerson Andre Valdivia Mema', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100045968473586', 9, 34, '2020-02-15', 'ATENDIDO', 46, 426, 0),
(372, NULL, '2020-02-15', 'Giulios Kong San', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001339082288', 8, 45, '2020-02-15', 'ATENDIDO', 51, 411, 0),
(373, 391, '2020-02-15', 'Ciro QR', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002297752372', 10, 49, '2020-02-15', 'ATENDIDO', 80, 391, 0),
(374, NULL, '2020-02-15', 'Renzo ER', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003658298044', 12, 5, '2020-02-15', 'ATENDIDO', 102, NULL, 0),
(375, 366, '2020-02-15', 'Jesus Uribe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100041417720786', 11, 34, '2020-02-15', 'ATENDIDO', 38, 366, 0),
(377, NULL, '2020-02-15', 'Raid Chavez Buendia', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000965214165', 8, 48, '2020-02-15', 'ATENDIDO', 65, 421, 0),
(378, NULL, '2020-02-15', 'Jordan Iparraguirre Ubillus -', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005747836416', 12, 60, '2020-02-15', 'ATENDIDO', 40, 403, 0),
(379, NULL, '2020-02-15', 'Cristian Canales Torres', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002159420352', 8, 59, '2020-02-15', 'ATENDIDO', 40, 429, 0),
(380, NULL, '2020-02-15', 'Jerson Cueva ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100010730350064', 8, 59, '2020-02-15', 'ATENDIDO', 39, 430, 0),
(381, NULL, '2020-02-15', 'Kevin Abel Rivera Rodriguez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001872478495', 12, 5, '2020-02-15', 'ATENDIDO', 102, NULL, 0),
(382, NULL, '2020-02-15', 'Diego Q. Torres', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003456385048', 12, 34, '2020-02-15', 'ATENDIDO', 38, 410, 0),
(383, NULL, '2020-02-15', 'Yael Abs', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005140355803', 11, 34, '2020-02-15', 'ATENDIDO', 46, 427, 0),
(384, NULL, '2020-02-15', 'Carlos Marquez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000257544999', 8, 19, '2020-02-13', 'ATENDIDO', 125, 424, 0),
(385, 368, '2020-02-15', 'Stefano Javier Carbajal', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1431771639', 8, 39, '2020-02-15', 'ATENDIDO', 80, 368, 0),
(386, NULL, '2020-02-15', 'Kevin Arnold Tuesta Lluncor', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006979090144', 12, 34, '2020-02-15', 'ATENDIDO', 46, 426, 0),
(387, 378, '2020-02-15', 'Carlos Cal', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001072383177', 11, 49, '2020-02-15', 'ATENDIDO', 114, 378, 0),
(388, NULL, '2020-02-17', 'Steve Rocha Avelino', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100025648234618', 9, 49, '2020-02-16', 'ATENDIDO', 110, 425, 0),
(389, NULL, '2020-02-17', 'Jose Seminario', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000112783728', 12, 19, '2020-02-17', 'ATENDIDO', 51, 418, 0),
(390, NULL, '2020-02-17', 'Sebastian Huapaya Camacho', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100014028011281', 10, 34, '2020-02-16', 'ATENDIDO', 46, 469, 0),
(391, NULL, '2020-02-17', 'Jhonatan Marca Medina', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002184865137', 12, 30, '2020-02-17', 'ATENDIDO', 40, 459, 0),
(392, NULL, '2020-02-17', 'Abel Junior Jurado Melendez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002275003926', 8, 59, '2020-02-17', 'ATENDIDO', 40, 458, 0),
(393, NULL, '2020-02-17', 'Beto Taipe Ayala - MITAD', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003145148978', 10, 30, '2020-02-17', 'ATENDIDO', 40, 375, 0),
(394, NULL, '2020-02-17', 'Ricardo Borjas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000914459113', 10, 59, '2020-02-17', 'ATENDIDO', 39, 436, 0),
(395, NULL, '2020-02-17', 'Alexis Ramos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004250980021', 10, 19, '2020-02-17', 'ATENDIDO', 127, 431, 0),
(396, NULL, '2020-02-17', 'Kevin De La Cruz', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000180465650', 11, 49, '2020-02-17', 'ATENDIDO', 80, 389, 0),
(398, NULL, '2020-02-17', 'Danilo NiquÃ©n', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002039156872', 12, 59, '2020-02-17', 'ATENDIDO', 39, 455, 0),
(399, NULL, '2020-02-17', 'Diego Guevara', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004855756665', 8, 49, '2020-02-17', 'ATENDIDO', 110, 433, 0),
(400, NULL, '2020-02-17', 'Cristhian Villar', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100025840683480', 10, 94, '2020-02-16', 'ATENDIDO', 128, 432, 0),
(401, NULL, '2020-02-17', 'Alberto Alberto Alberto', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1010587956', 10, 59, '2020-02-17', 'ATENDIDO', 39, 454, 0),
(402, NULL, '2020-02-17', 'Manuel Pacheco', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1623513210', 8, 59, '2020-02-17', 'ATENDIDO', 39, 453, 0),
(403, NULL, '2020-02-17', 'Carlos Pariasca', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001111776676', 10, 59, '2020-02-17', 'ATENDIDO', 39, 435, 0),
(404, NULL, '2020-02-17', 'Carlos Pariasca', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001111776676', 10, 49, '2020-02-17', 'ATENDIDO', 110, 433, 0),
(405, NULL, '2020-02-17', 'Axel Mancilla Sotomayor', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001526525369', 8, 59, '2020-02-17', 'ATENDIDO', 39, 452, 0),
(406, NULL, '2020-02-17', 'Danna GarcÃ­a', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011670962856', 12, 5, '2020-02-17', 'ATENDIDO', 102, NULL, 0),
(407, NULL, '2020-02-17', 'Carlos Bart TE', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006033876528', 8, 34, '2020-02-17', 'ATENDIDO', 46, 468, 0),
(408, NULL, '2020-02-17', 'Xavier Llamosas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000543224338', 10, 59, '2020-02-17', 'ATENDIDO', 40, 434, 0),
(409, NULL, '2020-02-17', 'Cleiber Rojas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002696850428', 8, 34, '2020-02-17', 'ATENDIDO', 46, 467, 0),
(410, NULL, '2020-02-17', 'Frank NuÃ±ez Zeballos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100026834137408', 10, 49, '2020-02-17', 'ATENDIDO', 80, 390, 0),
(411, NULL, '2020-02-17', 'Frank NuÃ±ez Zeballos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100026834137408', 10, 29, '2020-02-17', 'ATENDIDO', 91, 420, 0),
(412, NULL, '2020-02-17', 'Jason Villantoy Espejo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006336905455', 11, 49, '2020-02-17', 'ATENDIDO', 110, 425, 0),
(413, NULL, '2020-02-17', 'Arturo Fernandez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1061131002', 10, 89, '2020-02-17', 'ATENDIDO', 128, 432, 0),
(414, NULL, '2020-02-17', 'Jason Villantoy Espejo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006336905455', 11, 49, '2020-02-17', 'ATENDIDO', 41, 464, 0),
(415, NULL, '2020-02-17', 'Jaime Matta', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009618695190', 12, 49, '2020-02-17', 'ATENDIDO', 43, 236, 0),
(416, NULL, '2020-02-17', 'Diego Williams Rivera Blas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003802022868', 11, 55, '2020-02-17', 'ATENDIDO', 61, 135, 0),
(417, NULL, '2020-02-17', 'Diego Williams Rivera Blas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003802022868', 12, 34, '2020-02-17', 'ATENDIDO', 38, 462, 0),
(418, NULL, '2020-02-17', 'Diego Williams Rivera Blas - DAR CUANDO PIDE', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003802022868', 12, 89, '2020-02-17', 'ATENDIDO', 129, 438, 0),
(419, NULL, '2020-02-17', 'Diego Williams Rivera Blas - DAR CUANDO PIDE', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003802022868', 12, 59, '2020-02-17', 'ATENDIDO', 65, 413, 0),
(420, NULL, '2020-02-17', 'Javier Quispe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000829369663', 8, 34, '2020-02-17', 'ATENDIDO', 100, 246, 0),
(421, NULL, '2020-02-17', 'Jhonatan Parodi', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=789869929', 12, 59, '2020-02-17', 'ATENDIDO', 39, 451, 0),
(422, NULL, '2020-02-17', 'Andy Navarro Abad', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001776659062', 12, 59, '2020-02-17', 'ATENDIDO', 39, 430, 0),
(423, NULL, '2020-02-17', 'Manuel Pacheco', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1623513210', 8, 25, '2020-02-17', 'ATENDIDO', 60, 388, 0),
(425, NULL, '2020-02-17', 'Luis Espinoza', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002108933548', 8, 49, '2020-02-17', 'ATENDIDO', 80, 475, 0),
(426, NULL, '2020-02-17', 'Miguel Angel Rodriguez Peredo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100010792151822', 8, 50, '2020-02-17', 'ATENDIDO', 43, 472, 0),
(427, NULL, '2020-02-17', 'Gabriel RamÃ³n', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1828008933', 10, 34, '2020-02-17', 'ATENDIDO', 46, 466, 0),
(428, NULL, '2020-02-18', 'Alvaro Ipince Cuevas', 'https://www.facebook.com/chiclestoreperu/inbox/2983283631692875/?notif_id=1581906827327734&notif_t=page_message&mailbox_id=264286250415069&selected_item_id=100000338028109', 8, 59, '2020-02-18', 'ATENDIDO', 39, 451, 0),
(429, 437, '2020-02-18', 'Alejandro SÃ¡nchez Caballero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001226472582', 8, 59, '2020-02-18', 'ATENDIDO', 39, 437, 0),
(430, 286, '2020-02-18', 'Alejandro SÃ¡nchez Caballero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001226472582', 8, 59, '2020-02-18', 'ATENDIDO', 40, 286, 0),
(431, NULL, '2020-02-18', 'Alejandro SÃ¡nchez Caballero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001226472582', 8, 49, '2020-02-18', 'ATENDIDO', 80, 477, 0),
(432, NULL, '2020-02-18', 'Edson GÃ³mez GarcÃ­a', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002559141766', 11, 60, '2020-02-18', 'ATENDIDO', 39, 455, 0),
(433, NULL, '2020-02-18', 'Cristian GÃ¡lvez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005058210507', 11, 34, '2020-02-18', 'ATENDIDO', 46, 473, 0),
(434, NULL, '2020-02-18', 'Alexayev Aranda', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006601872489', 10, 39, '2020-02-18', 'ATENDIDO', 46, 466, 0),
(435, NULL, '2020-02-18', 'Jarley Bocanegra Pereda', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003560775894', 9, 34, '2020-02-18', 'ATENDIDO', 46, 473, 0),
(436, NULL, '2020-02-18', 'Victor Zambrano', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100021856844877', 8, 5, '2020-02-18', 'ATENDIDO', 102, NULL, 0),
(437, NULL, '2020-02-18', 'Jaromir Gainza', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1682600682', 9, 25, '2020-02-17', 'ATENDIDO', 114, 478, 0),
(438, NULL, '2020-02-18', 'Pablo Acosta Salazar', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001925517423', 8, 40, '2020-02-18', 'ATENDIDO', 46, 471, 0),
(439, NULL, '2020-02-18', 'Sebas Lopez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001287536144', 10, 59, '2020-02-18', 'ATENDIDO', 40, 429, 0),
(440, NULL, '2020-02-18', 'Anibal Mozo Berrocal', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003128112793', 10, 19, '2020-02-18', 'ATENDIDO', 125, 457, 0),
(441, NULL, '2020-02-18', 'Jory Hyun Joong', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001961938137', 8, 68, '2020-02-18', 'ATENDIDO', 65, 421, 0),
(443, NULL, '2020-02-18', 'JosÃ¨ SÃ¡nchez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1328727458', 10, 34, '2020-02-18', 'ATENDIDO', 46, 470, 0),
(444, NULL, '2020-02-18', 'Junior Vilca', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006440114815', 8, 59, '2020-02-18', 'ATENDIDO', 80, 476, 0),
(446, NULL, '2020-02-18', 'Carlos Alberto Taipe Figueroa', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001342652035', 8, 19, '2020-02-18', 'ATENDIDO', 127, 431, 0),
(447, NULL, '2020-02-18', 'Angel Huaranga Oscategui', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005332204064', 8, 59, '2020-02-18', 'ATENDIDO', 39, 435, 0),
(448, NULL, '2020-02-18', 'Carlos Chiquinta', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007959488940', 8, 34, '2020-02-18', 'ATENDIDO', 38, 463, 0),
(449, NULL, '2020-02-18', 'Julian Alcantara Espinoza', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=525658705', 8, 59, '2020-02-18', 'ATENDIDO', 39, 461, 0),
(450, NULL, '2020-02-18', 'Cristofer Portillo Pizarro', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001770385918', 9, 19, '2020-02-18', 'ATENDIDO', 130, 480, 0),
(451, NULL, '2020-02-18', 'Cristofer Portillo Pizarro', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001770385918', 8, 11, '2020-02-18', 'ATENDIDO', 82, 216, 0),
(452, NULL, '2020-02-18', 'Marco Ortega Vega', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004802329066', 8, 5, '2020-02-18', 'ATENDIDO', 102, NULL, 0),
(453, NULL, '2020-02-18', 'Marco Ortega Vega', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004802329066', 8, 19, '2020-02-18', 'ATENDIDO', 125, 424, 0),
(454, NULL, '2020-02-18', 'Ã‘Ä«kÄ«Är Ä€Å«ÄÄÄsÄ«', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006168541994', 10, 49, '2020-02-18', 'ATENDIDO', 56, 119, 0),
(455, NULL, '2020-02-18', 'Eduardo Sifuentes ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001509041313', 8, 45.5, '2020-02-18', 'ATENDIDO', 51, 474, 0),
(459, NULL, '2020-02-18', 'Eder Villanueva Ruiz ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000004899339', 12, 59, '2020-02-18', 'ATENDIDO', 40, 460, 0),
(460, NULL, '2020-02-18', 'Ivan Saenz', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001893255649', 11, 19, '2020-02-18', 'ATENDIDO', 86, 225, 0),
(461, 456, '2020-02-18', 'Sonny Joel Zapata Calle - MITAD', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100014473907953', 8, 30, '2020-02-18', 'ATENDIDO', 39, 456, 0),
(463, NULL, '2020-02-19', 'Gerson Alfredo Ramirez Ismodes', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001898301145', 8, 58, '2020-02-19', 'ATENDIDO', 80, 475, 0),
(464, NULL, '2020-02-19', 'Jesus Grados Anton', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007056967856', 10, 34, '2020-02-19', 'ATENDIDO', 38, 462, 0),
(465, NULL, '2020-02-19', 'Piero Christopher', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002453864136', 12, 5, '2020-02-19', 'ATENDIDO', 102, NULL, 0),
(466, NULL, '2020-02-19', 'Jairo SC Bocanegra SauÃ±e', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001725524426', 8, 34, '2020-02-19', 'ATENDIDO', 46, 406, 0),
(467, NULL, '2020-02-19', 'Alex Junior Palomino', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001721263497', 11, 34, '2020-02-19', 'ATENDIDO', 100, 292, 0),
(468, NULL, '2020-02-19', 'David Romero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002467573987', 11, 49, '2020-02-19', 'ATENDIDO', 59, 379, 0),
(469, NULL, '2020-02-19', 'Yannett Rocio Palomino Ramirez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002777047714', 8, 39, '2020-02-19', 'ATENDIDO', 39, 62, 1),
(470, NULL, '2020-02-20', 'Xavier Santiago Melgarejo Dongo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100013817944229', 12, 79, '2020-02-20', 'ATENDIDO', 80, 476, 0),
(471, NULL, '2020-02-20', 'Hemerson Howard', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003175088119', 12, 25, '2020-02-20', 'ATENDIDO', 47, 173, 0),
(472, NULL, '2020-02-21', 'Diego Guevara', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004855756665', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(473, NULL, '2020-02-21', 'Gabriel RamÃ³n', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1828008933', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(474, NULL, '2020-02-21', 'Oscar P. F Ramos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006290610293', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(475, NULL, '2020-02-21', 'Aaron Espinoza', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003217805514', 8, 5, '2020-02-19', 'ATENDIDO', 102, NULL, 0),
(476, NULL, '2020-02-21', 'Jose Baluarte Galvez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003563201417', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(477, NULL, '2020-02-21', 'Andre QuicaÃ±a', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000451918642', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(479, NULL, '2020-02-21', 'Christopher NiquÃ­n Ato', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002220764205', 12, 34, '2020-02-21', 'ATENDIDO', 100, 483, 0),
(480, NULL, '2020-02-21', 'Pierre Quispe Cuadros', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=778680843', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(481, NULL, '2020-02-21', 'Emerson Andre Valdivia Mema', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100045968473586', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(482, NULL, '2020-02-21', 'Edson Sebastian', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003742593105', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(483, NULL, '2020-02-21', 'Juan Holgado', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001647951179', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(484, NULL, '2020-02-21', 'Diego Jave', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011493200530', 12, 25, '2020-02-21', 'ATENDIDO', 132, 502, 0),
(485, NULL, '2020-02-21', 'Sebastian Murillo Guizado', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006134440027', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(486, NULL, '2020-02-21', 'Andre QuicaÃ±a', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000451918642', 12, 0, '2020-02-21', 'ATENDIDO', 102, NULL, 0),
(487, NULL, '2020-02-21', 'AdriÃ¡n Fernandez Linares', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001011347760', 11, 34, '2020-02-21', 'ATENDIDO', 46, 468, 0),
(488, 494, '2020-02-21', 'Richard Ramos Villaverde-MITAD', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002090971119', 12, 30, '2020-02-21', 'ATENDIDO', 45, 494, 0),
(489, NULL, '2020-02-21', 'Helen Blas - DAR SI PIDE', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007051862920', 10, 59, '2020-02-21', 'NO ATENDIDO', 39, NULL, 0),
(490, NULL, '2020-02-22', 'Miguel Martinez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006388780220', 12, 0, '2020-02-22', 'ATENDIDO', 102, NULL, 0),
(491, NULL, '2020-02-22', 'Elizabeth Rocha', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=560854175', 8, 15, '2020-02-22', 'ATENDIDO', 133, 496, 0),
(492, NULL, '2020-02-22', 'arley Bocanegra Pereda', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003560775894', 12, 0, '2020-02-22', 'ATENDIDO', 102, NULL, 0),
(493, NULL, '2020-02-22', 'Victor Perleche Raffo', 'https://www.facebook.com/chiclestoreperu/inbox/10219107508411914/?notif_id=1582303633018017&notif_t=page_message&mailbox_id=264286250415069&selected_item_id=1032599248', 11, 59, '2020-02-22', 'ATENDIDO', 40, 459, 0),
(494, NULL, '2020-02-22', 'Juan Soto CÃ«rna', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100010634100898', 10, 69, '2020-02-22', 'ATENDIDO', 88, 499, 0),
(495, NULL, '2020-02-22', 'Renato Lino Robles', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000997131917', 8, 59, '2020-02-22', 'ATENDIDO', 39, 453, 0),
(496, 489, '2020-02-22', 'Cecibett Lock', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1258473613', 12, 34, '2020-02-22', 'ATENDIDO', 100, 489, 0),
(497, NULL, '2020-02-22', 'Nelson Sarango', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100016038453016', 11, 10, '2020-02-22', 'ATENDIDO', 102, NULL, 0),
(498, NULL, '2020-02-22', 'Miguel Rodriguez Layango  More Items', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003321265549', 8, 59, '2020-02-22', 'ATENDIDO', 45, 492, 0),
(499, NULL, '2020-02-22', 'Marco A. Vera', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1247155860', 10, 34, '2020-02-22', 'ATENDIDO', 100, 491, 0),
(500, NULL, '2020-02-22', 'Gabriel Mike Mendoza Dlc', 'https://www.facebook.com/chiclestoreperu/inbox/2570599503209396/?notif_id=1582303633018017&notif_t=page_message&mailbox_id=264286250415069&selected_item_id=100007781144467', 12, 0, '2020-02-22', 'ATENDIDO', 102, NULL, 0),
(501, 501, '2020-02-22', 'Yerson Fernandez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100038701472886', 12, 25, '2020-02-22', 'ATENDIDO', 47, 501, 0),
(502, NULL, '2020-02-22', 'Diego Vergonzini', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000495650387', 8, 43, '2020-02-22', 'ATENDIDO', 136, 504, 0),
(503, NULL, '2020-02-22', 'Victor Morales Quispe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001828552509', 8, 59, '2020-02-22', 'ATENDIDO', 45, 493, 0),
(504, NULL, '2020-02-22', 'Ricardo Arteaga', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009247980979', 12, 5, '2020-02-22', 'ATENDIDO', 102, NULL, 0),
(505, NULL, '2020-02-22', 'Miguel Angel VicuÃ±a', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=761432785', 12, 0, '2020-02-22', 'ATENDIDO', 102, NULL, 0),
(506, NULL, '2020-02-22', 'Miguel Angel', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003788229185', 8, 89, '2020-02-22', 'ATENDIDO', 137, 508, 0),
(507, NULL, '2020-02-22', 'Fabian F Segura', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100013433655798', 8, 69, '2020-02-22', 'ATENDIDO', 88, 498, 0),
(508, NULL, '2020-02-22', 'Sebastian Acosta', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001865573262', 11, 5, '2020-02-22', 'ATENDIDO', 102, NULL, 0),
(509, NULL, '2020-02-22', 'Jhonatan Marca Medina', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002184865137', 12, 34, '2020-02-22', 'ATENDIDO', 76, 500, 0),
(510, 493, '2020-02-22', 'Carlos CoveÃ±as Valladares', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001472003539', 8, 59, '2020-02-22', 'ATENDIDO', 45, 493, 0),
(511, NULL, '2020-02-22', 'Cristhian HM', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100007384219450', 10, 129, '2020-02-22', 'ATENDIDO', 52, 497, 0),
(512, NULL, '2020-02-22', 'Manuel Bruzual', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1211782523', 8, 15, '2020-02-22', 'ATENDIDO', 102, NULL, 0),
(513, NULL, '2020-02-22', 'Juan Manuel Cueto', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=800464573', 8, 19, '2020-02-22', 'ATENDIDO', 100, 488, 1),
(514, 488, '2020-02-22', 'V Joel Cordova', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100045895959412', 12, 34, '2020-02-22', 'ATENDIDO', 100, 488, 0),
(515, NULL, '2020-02-22', 'Joshua Alvarez Moron', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001283690515', 11, 19, '2020-02-22', 'ATENDIDO', 60, 131, 1),
(517, NULL, '2020-02-24', 'Piero Daniel Ccana', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100018470732071', 8, 5, '2020-02-24', 'ATENDIDO', 102, NULL, 0),
(520, NULL, '2020-02-24', 'Jairo SC Bocanegra SauÃ±e', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001725524426', 8, 51, '2020-02-24', 'ATENDIDO', 134, 481, 0),
(521, NULL, '2020-02-24', 'Jose Seminario', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000112783728', 12, 19, '2020-02-24', 'ATENDIDO', 37, 506, 0),
(522, NULL, '2020-02-24', 'Luis Zurita', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002074833068', 11, 59, '2020-02-24', 'ATENDIDO', 45, 495, 0),
(523, NULL, '2020-02-24', 'Froot Loops', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100039330540538', 8, 34, '2020-02-24', 'ATENDIDO', 46, 467, 0),
(524, NULL, '2020-02-24', 'Rafael Ramos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1274761465', 8, 61, '2020-02-24', 'ATENDIDO', 138, 505, 0),
(525, NULL, '2020-02-24', 'Esteban MA', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002369862887', 12, 0, '2020-02-24', 'ATENDIDO', 102, NULL, 0),
(526, NULL, '2020-02-24', 'Alejandro Rodriguez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100026691183671', 11, 0, '2020-02-24', 'ATENDIDO', 102, NULL, 0),
(527, NULL, '2020-02-24', 'Carlos Belon Quispe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002865961324', 11, 0, '2020-02-24', 'ATENDIDO', 102, NULL, 0),
(528, NULL, '2020-02-24', 'Renato Alvarez ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001677699747', 11, 34, '2020-02-24', 'ATENDIDO', 100, 503, 0),
(529, NULL, '2020-02-24', 'Jason Laura', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002399259337', 8, 59, '2020-02-24', 'ATENDIDO', 40, 460, 0),
(530, NULL, '2020-02-24', 'Alexis Aquino Luque', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002234229243', 10, 19, '2020-02-24', 'ATENDIDO', 100, 140, 1),
(531, NULL, '2020-02-24', 'Jesus Yupanqui Ripas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001706098057', 10, 49, '2020-02-24', 'ATENDIDO', 56, 514, 0),
(532, NULL, '2020-02-24', 'Jesus Yupanqui Ripas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001706098057', 10, 5, '2020-02-24', 'ATENDIDO', 102, NULL, 0),
(533, NULL, '2020-02-24', 'Sebastian Romero', 'https://web.facebook.com/chiclestoreperu/inbox/10215884121632727/?notif_id=1582476437353885&notif_t=page_message&mailbox_id=264286250415069&selected_item_id=1136610580', 11, 25, '2020-02-24', 'ATENDIDO', 47, 512, 0),
(534, NULL, '2020-02-24', 'Kevin Cruz PeÃ±a', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001486767090', 12, 89, '2020-02-24', 'ATENDIDO', 137, 515, 0),
(537, NULL, '2020-02-25', 'Alex Junior Palomino', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001721263497', 11, 59, '2020-02-25', 'ATENDIDO', 39, 454, 0),
(538, NULL, '2020-02-25', 'Victor Perleche Raffo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1032599248', 12, 0, '2020-02-25', 'ATENDIDO', 102, NULL, 0),
(539, NULL, '2020-02-25', 'Omarion Noe Austin Jacobo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100027942132639', 12, 0, '2020-02-25', 'ATENDIDO', 102, NULL, 0),
(540, 511, '2020-02-25', 'Aldair Lujan Vega - MITAD', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009276378183', 12, 25, '2020-02-25', 'ATENDIDO', 67, 511, 0),
(541, NULL, '2020-02-25', 'Aldair Lujan Vega - MITAD', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009276378183', 12, 25, '2020-02-25', 'ATENDIDO', 43, 472, 0),
(542, NULL, '2020-02-25', 'Aldair Lujan Vega - MITAD', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009276378183', 12, 30, '2020-02-25', 'ATENDIDO', 45, 493, 0),
(543, NULL, '2020-02-25', 'Renato Guzman Torres', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1348902264', 11, 0, '2020-02-25', 'ATENDIDO', 102, NULL, 0),
(544, NULL, '2020-02-25', 'Carloandre Luna', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1053726526', 11, 59, '2020-02-25', 'ATENDIDO', 45, 519, 0),
(545, NULL, '2020-02-25', 'Esteban LÃ³pez Guevara', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003824110042', 12, 34, '2020-02-25', 'ATENDIDO', 55, 110, 0),
(546, 520, '2020-02-25', 'Gerson Marcelo Mendizabal', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009513415900', 12, 59, '2020-02-25', 'ATENDIDO', 45, 520, 0),
(547, 523, '2020-02-25', 'Victor Hugo Flores Mango', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002286416806', 10, 34, '2020-02-24', 'ATENDIDO', 76, 523, 0),
(548, 513, '2020-02-25', 'Alex Fernandez', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&_rdc=1&_rdr&selected_item_id=100001870001624', 10, 61, '2020-02-25', 'ATENDIDO', 84, 513, 0),
(549, NULL, '2020-02-25', 'Carlos AndrÃ©s', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004137155786', 11, 0, '2020-02-25', 'ATENDIDO', 102, NULL, 0),
(550, NULL, '2020-02-25', 'Alexander Estela', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002380360262', 12, 0, '2020-02-25', 'ATENDIDO', 102, NULL, 0),
(551, NULL, '2020-02-25', 'Renzo Aguero', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004891543192', 8, 51, '2020-02-25', 'ATENDIDO', 134, 517, 0),
(552, 508, '2020-02-25', 'Moises Sensebe', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100013297427134', 9, 89, '2020-02-25', 'ATENDIDO', 137, 508, 0),
(553, NULL, '2020-02-25', 'Fabio BriceÃ±o Garcia', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002263518694', 12, 39, '2020-02-25', 'ATENDIDO', 74, 322, 0),
(556, NULL, '2020-02-25', 'Juancho CL', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100018737918685', 8, 49, '2020-02-25', 'ATENDIDO', 41, 464, 0),
(557, 509, '2020-02-25', 'Jeyner Kemmy Talledo Farfan', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1243765555', 10, 19, '2020-02-25', 'ATENDIDO', 37, 509, 0),
(558, NULL, '2020-02-25', 'David Gonzales Garcia', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000977352995', 8, 59, '2020-02-25', 'ATENDIDO', 45, 519, 0),
(560, 524, '2020-02-25', 'Renato Guzman Torres', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1348902264', 8, 25, '2020-02-25', 'ATENDIDO', 139, 524, 0),
(564, NULL, '2020-02-25', 'Alee x HG', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001304177623', 12, 59, '2020-02-25', 'ATENDIDO', 45, 518, 0),
(566, NULL, '2020-02-25', 'Sebastian Murillo Guizado', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006134440027', 10, 49, '2020-02-25', 'NO ATENDIDO', 67, NULL, 0),
(567, NULL, '2020-02-25', 'Alberto Vargas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000892921665', 12, 49, '2020-02-25', 'ATENDIDO', 43, 479, 0),
(568, NULL, '2020-02-25', 'Anthony Mamani Camacho', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006169655894', 10, 50, '2020-02-25', 'ATENDIDO', 134, 517, 0),
(569, NULL, '2020-02-26', 'Diego Jave', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100011493200530', 12, 0, '2020-02-26', 'ATENDIDO', 102, NULL, 0),
(570, NULL, '2020-02-26', 'Jesus Cornejo Utrilla', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001978692510', 10, 69, '2020-02-26', 'ATENDIDO', 39, 452, 0),
(571, NULL, '2020-02-26', 'Cesar San', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1591135402', 12, 0, '2020-02-26', 'ATENDIDO', 102, NULL, 0),
(572, 533, '2020-02-26', 'Greg Rodridlc', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1393051635', 12, 45, '2020-02-26', 'ATENDIDO', 140, 533, 0),
(573, NULL, '2020-02-26', 'Axel QuiÃ±onez Tapia', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100004946710487', 10, 59, '2020-02-26', 'ATENDIDO', 111, 344, 0),
(574, 522, '2020-02-26', 'Brayan R. Vasquez', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000940232941', 11, 19, '2020-02-25', 'ATENDIDO', 37, 522, 0),
(575, NULL, '2020-02-26', 'Sebastian Alvarado', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100012967159278', 8, 34, '2020-02-26', 'ATENDIDO', 100, 292, 0),
(576, NULL, '2020-02-26', 'Jair Montes Yataco', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100014123534214', 12, 0, '2020-02-26', 'ATENDIDO', 102, NULL, 0),
(577, NULL, '2020-02-26', 'Maicol O. Huamancayo', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002570027024', 11, 49, '2020-02-26', 'ATENDIDO', 81, 415, 0),
(578, NULL, '2020-02-26', 'Stefano Javier Carbajal', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1431771639', 12, 0, '2020-02-26', 'ATENDIDO', 102, NULL, 0),
(579, NULL, '2020-02-26', 'Leandro MuÃ±ico', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001666919195', 12, 0, '2020-02-26', 'ATENDIDO', 102, NULL, 0),
(582, NULL, '2020-02-26', 'Edson NuÃ±ez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001294669779', 11, 19, '2020-02-26', 'ATENDIDO', 78, 394, 0),
(583, 529, '2020-02-26', 'Bryan Apuc', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000016812396', 11, 59, '2020-02-26', 'ATENDIDO', 45, 529, 0),
(584, 521, '2020-02-26', 'Xavier Morera', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100028088794278', 11, 37, '2020-02-26', 'ATENDIDO', 141, 521, 0),
(585, NULL, '2020-02-26', 'Italo Pinedo Soriano', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001642353179', 12, 0, '2020-02-26', 'ATENDIDO', 102, NULL, 0),
(586, NULL, '2020-02-26', 'Jose Joel Rojas Valdivia', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100023734419581', 10, 34, '2020-02-26', 'ATENDIDO', 38, 463, 0),
(587, NULL, '2020-02-26', 'SN Aron', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003958782044', 11, 19, '2020-02-26', 'ATENDIDO', 37, 506, 0),
(588, 530, '2020-02-26', 'Anthony Rivera', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001437613727', 11, 59, '2020-02-26', 'ATENDIDO', 45, 530, 0),
(589, 528, '2020-02-26', 'Anthony Alvaro Capcha Ramos', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002543016773', 12, 34, '2020-02-26', 'ATENDIDO', 76, 528, 0),
(590, NULL, '2020-02-26', 'Jordi Pasquel Calle', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002404426005', 10, 34, '2020-02-26', 'ATENDIDO', 46, 399, 0),
(591, NULL, '2020-02-26', 'Christofer Chavarri', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003107202463', 11, 59, '2020-02-26', 'ATENDIDO', 45, 531, 0),
(592, NULL, '2020-02-26', 'Jhonny Torres', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005182236274', 8, 59, '2020-02-26', 'ATENDIDO', 45, 492, 0),
(593, NULL, '2020-02-27', 'Aki Higa', 'https://www.facebook.com/chiclestoreperu/inbox/2846060268773875/?notif_id=1582649248351475&notif_t=page_message&mailbox_id=264286250415069&selected_item_id=100001693110390', 12, 0, '2020-02-27', 'ATENDIDO', 102, NULL, 0),
(594, NULL, '2020-02-27', 'Luis Alejandro Pottozen', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000233082307', 11, 34, '2020-02-27', 'ATENDIDO', 46, 427, 0),
(595, NULL, '2020-02-27', 'Patch Caoz Cachay Sanchez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002215005766', 9, 25, '2020-02-27', 'ATENDIDO', 125, 457, 0),
(596, 532, '2020-02-27', 'Patch Caoz Cachay Sanchez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002215005766', 9, 12, '2020-02-27', 'ATENDIDO', 143, 532, 0),
(597, 525, '2020-02-27', 'Patch Caoz Cachay Sanchez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002215005766', 9, 29, '2020-02-27', 'ATENDIDO', 144, 525, 0),
(598, 526, '2020-02-27', 'Patch Caoz Cachay Sanchez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002215005766', 9, 37, '2020-02-27', 'ATENDIDO', 145, 526, 0),
(599, NULL, '2020-02-27', 'Melishitas Guzman', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002028639972', 8, 59, '2020-02-27', 'NO ATENDIDO', 39, NULL, 0),
(600, NULL, '2020-02-27', 'Melishitas Guzman', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002028639972', 8, 34, '2020-02-27', 'ATENDIDO', 100, 483, 0),
(601, NULL, '2020-02-27', 'Raid Chavez Buendia', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000965214165', 12, 0, '2020-02-27', 'ATENDIDO', 102, NULL, 0),
(602, NULL, '2020-02-27', 'Rolando Piero Injante Bolivar', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001048008636', 9, 79, '2020-02-26', 'ATENDIDO', 80, 477, 0),
(603, NULL, '2020-02-27', 'Benjamin Sebastian Ayma', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100029138916915', 9, 34, '2020-02-27', 'ATENDIDO', 76, 500, 0),
(604, NULL, '2020-02-27', 'Thalia Mercedes Barrero', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100043153973490', 8, 59, '2020-02-27', 'ATENDIDO', 40, 376, 0),
(605, NULL, '2020-02-27', 'Eduardo CF', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000449569013', 11, 25, '2020-02-27', 'ATENDIDO', 47, 512, 0),
(606, NULL, '2020-02-27', 'Alfredo Pittman', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=514944604', 8, 19, '2020-02-27', 'NO ATENDIDO', 37, NULL, 0),
(607, NULL, '2020-02-27', 'Alvaro Eyzaguirre', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000498160292', 8, 59, '2020-02-27', 'ATENDIDO', 45, 495, 0),
(608, 527, '2020-02-27', 'Christian Carrion', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100010114216264', 10, 37, '2020-02-27', 'ATENDIDO', 99, 527, 0),
(609, NULL, '2020-02-27', 'Anthony Mamani Camacho - DAR SI PIDE', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006169655894', 10, 0, '2020-02-25', 'NO ATENDIDO', 134, NULL, 0),
(610, NULL, '2020-02-27', 'Carlos PM', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000907688781', 8, 34, '2020-02-27', 'NO ATENDIDO', 76, NULL, 0);
INSERT INTO `cliente` (`idCliente`, `cuenta_idCuenta`, `fechAten`, `nomCliente`, `urlChat`, `metpagos_idPago`, `monto`, `fechDepo`, `estCliente`, `juego_idJuego`, `cuenta_Comprada`, `tVenta_idtVenta`) VALUES
(611, NULL, '2020-02-27', 'Yann Pier P. Andaluz', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1244877888', 8, 59, '2020-02-27', 'ATENDIDO', 45, 518, 0),
(612, NULL, '2020-02-27', 'Yorgos Guitton Bracamonte', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1651155933', 12, 34, '2020-02-27', 'ATENDIDO', 46, 469, 0),
(613, NULL, '2020-02-27', 'Antonio Mejia', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001278945877', 12, 34, '2020-02-27', 'NO ATENDIDO', 101, NULL, 0),
(614, NULL, '2020-02-27', 'Jhosymar Reyna Silvestre', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001467358919', 10, 39, '2020-02-27', 'NO ATENDIDO', 74, NULL, 0),
(615, NULL, '2020-02-27', 'Sissy Anchita', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002604298575', 8, 34, '2020-02-27', 'NO ATENDIDO', 76, NULL, 0),
(616, NULL, '2020-02-27', 'Sissy Anchita', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002604298575', 8, 49, '2020-02-27', 'NO ATENDIDO', 67, NULL, 0),
(617, NULL, '2020-02-27', 'R MartÃ­n MorÃ³n Pereyra', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=732659543', 11, 59, '2020-02-27', 'ATENDIDO', 45, 531, 0),
(618, NULL, '2020-02-27', 'Alfonso Masias Orejuela', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001152490760', 11, 60, '2020-02-27', 'NO ATENDIDO', 45, NULL, 0),
(619, NULL, '2020-02-27', 'Jose Chavez Vasquez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006800378539', 8, 60, '2020-02-27', 'NO ATENDIDO', 45, NULL, 0),
(620, NULL, '2020-02-28', 'Kevin Jovany', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003295926549', 8, 12, '2020-02-28', 'ATENDIDO', 62, 134, 0),
(621, NULL, '2020-02-28', 'Frank Castillo ZÃºÃ±iga', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1027834955', 10, 49, '2020-02-28', 'ATENDIDO', 134, 517, 0),
(622, NULL, '2020-02-28', 'Fernando Alanya Perez Albela', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001456258630', 8, 59, '2020-02-27', 'ATENDIDO', 40, 422, 0),
(623, NULL, '2020-02-28', 'Alberto Vargas', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000892921665', 9, 0, '2020-02-28', 'NO ATENDIDO', 102, NULL, 0),
(624, NULL, '2020-02-28', 'CÃ©sar MaÃºrtua Angulo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003377531230', 11, 36, '2020-02-28', 'ATENDIDO', 100, 488, 0),
(625, NULL, '2020-02-28', 'CÃ©sar MaÃºrtua Angulo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003377531230', 11, 59, '2020-02-27', 'NO ATENDIDO', 45, NULL, 0),
(626, NULL, '2020-02-28', 'CÃ©sar MaÃºrtua Angulo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003377531230', 11, 25, '2020-02-27', 'NO ATENDIDO', 47, NULL, 0),
(627, NULL, '2020-02-28', 'Rodrigo Asencios Gonzales', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002330511855', 11, 34, '2020-02-27', 'ATENDIDO', 100, 488, 0),
(629, NULL, '2020-02-28', 'Sebastian Ramirez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100025911374548', 9, 25, '2020-02-26', 'NO ATENDIDO', 47, NULL, 0),
(630, NULL, '2020-02-28', 'Ãlvaro Paredes', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009266030354', 11, 39, '2020-02-28', 'NO ATENDIDO', 146, NULL, 0),
(631, NULL, '2020-02-28', 'Ø§ÙÙ„ØªÙˆÙ† ÙƒØ§Ø¨Ø§Ø¬ÙŠØ±Ùˆ', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002060589544', 9, 0, '2020-02-28', 'NO ATENDIDO', 102, NULL, 0),
(632, NULL, '2020-02-28', 'Martin Sarango', 'facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100010447320041', 10, 0, '2020-02-28', 'NO ATENDIDO', 102, NULL, 0),
(634, NULL, '2020-02-28', 'Alex Chirinos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1663899960', 13, 59, '2020-02-28', 'NO ATENDIDO', 45, NULL, 0),
(635, NULL, '2020-02-28', 'Jose Luis Paredes', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1027105168', 10, 34, '2020-02-28', 'ATENDIDO', 100, 488, 0),
(636, NULL, '2020-02-28', 'Pepe Ochoa Lazo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=651041991', 12, 34, '2020-02-28', 'ATENDIDO', 100, 488, 0),
(637, NULL, '2020-02-28', 'Andres Agreda Galdos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000720735120', 11, 25, '2020-02-28', 'NO ATENDIDO', 47, NULL, 0),
(638, NULL, '2020-02-28', 'Enyel Benavente', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006199931232', 12, 0, '2020-02-28', 'NO ATENDIDO', 102, NULL, 0),
(639, NULL, '2020-02-28', 'Carlos Marquez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000257544999', 12, 0, '2020-02-28', 'NO ATENDIDO', 102, NULL, 0),
(640, NULL, '2020-02-28', 'Reymont Castillo', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100010408112665', 12, 0, '2020-02-28', 'ATENDIDO', 100, 488, 0),
(641, NULL, '2020-02-28', 'Cecibett Lock - DAR SI PIDE', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1258473613', 12, 0, '2020-02-28', 'NO ATENDIDO', 100, NULL, 0),
(642, NULL, '2020-02-28', 'Paolo Munaylla - MITAD', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1613766462', 8, 24, '2020-02-28', 'NO ATENDIDO', 148, NULL, 0),
(643, NULL, '2020-02-28', 'Javier Ramirez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009940247684', 10, 44, '2020-02-28', 'ATENDIDO', 43, 479, 0),
(644, NULL, '2020-02-28', 'Javier Ramirez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100009940247684', 10, 80, '2020-02-28', 'ATENDIDO', 137, 515, 0),
(645, NULL, '2020-02-28', 'Gabriel BriceÃ±o Medrano', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003147091354', 12, 25, '2020-02-28', 'NO ATENDIDO', 109, NULL, 0),
(646, NULL, '2020-02-28', 'Huincho Huincho', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100006877965513&_rdc=1&_rdr', 11, 59, '2020-02-28', 'ATENDIDO', 39, 436, 0),
(647, NULL, '2020-02-28', 'Jorge Mendoza', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&_rdc=1&_rdr&selected_item_id=100002747629486', 8, 24, '2020-02-28', 'ATENDIDO', 100, 488, 0),
(648, NULL, '2020-02-28', 'Adrian Espinola', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001468389434', 10, 15, '2020-02-28', 'ATENDIDO', 130, 480, 0),
(649, NULL, '2020-02-28', 'Hanyel J Ch Guerrero', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100028652842087', 8, 47, '2020-02-28', 'ATENDIDO', 40, 428, 0),
(650, NULL, '2020-02-28', 'Gm Alex Reynozo Aspajo -MITAD', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=639742712', 8, 30, '2020-02-28', 'NO ATENDIDO', 45, NULL, 0),
(651, NULL, '2020-02-28', 'Mark C. LoyOla', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002859136583', 11, 34, '2020-02-28', 'ATENDIDO', 100, 488, 0),
(652, NULL, '2020-02-28', 'Moises Sensebe', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100013297427134', 8, 36, '2020-02-28', 'ATENDIDO', 51, 474, 0),
(653, NULL, '2020-02-28', 'Marvin J. Davila Culqui', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002081782013', 8, 39, '2020-02-28', 'ATENDIDO', 57, 387, 0),
(654, NULL, '2020-02-28', 'Jaime Abanto Ponce', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005964098229', 11, 64, '2020-02-28', 'ATENDIDO', 39, 461, 0),
(655, NULL, '2020-02-28', 'Jaime Abanto Ponce', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100005964098229', 11, 0, '2020-02-28', 'NO ATENDIDO', 102, NULL, 0),
(656, NULL, '2020-02-28', 'Bryan Hurtado', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100000919720916', 9, 34, '2020-02-28', 'ATENDIDO', 100, 488, 0),
(657, NULL, '2020-02-28', 'Edson NuÃ±ez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001294669779', 8, 12, '2020-02-28', 'ATENDIDO', 133, 496, 0),
(658, NULL, '2020-02-28', 'Edson NuÃ±ez', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100001294669779', 8, 23, '2020-02-28', 'ATENDIDO', 40, 63, 1),
(659, NULL, '2020-02-28', 'Giam Carlos', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=1134183712', 10, 39, '2020-02-28', 'ATENDIDO', 56, 514, 0),
(660, NULL, '2020-02-28', 'Alvaro Marcel Hostiliano', 'https://www.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100003190559271', 8, 37, '2020-02-28', 'NO ATENDIDO', 149, NULL, 0),
(661, NULL, '2020-02-28', 'Arturo Mendoza GastelÃº', 'https://web.facebook.com/chiclestoreperu/inbox/?mailbox_id=264286250415069&selected_item_id=100002072979668', 12, 0, '2020-02-28', 'NO ATENDIDO', 102, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `idCuenta` int(11) NOT NULL,
  `cuenta_idPadre` int(11) DEFAULT NULL,
  `rango` varchar(10) NOT NULL,
  `email` varchar(40) NOT NULL,
  `pswd` varchar(40) NOT NULL,
  `Fnacimiento` varchar(12) NOT NULL,
  `ventaPrincipal` int(5) NOT NULL,
  `ventaSecundario` int(5) NOT NULL,
  `nReseteos` int(5) NOT NULL,
  `ultimoReseteo` varchar(12) NOT NULL,
  `nPSN` varchar(40) NOT NULL,
  `precio` float NOT NULL,
  `precioVendido` float NOT NULL,
  `estadoCuenta` int(11) NOT NULL,
  `juego_idJuego` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`idCuenta`, `cuenta_idPadre`, `rango`, `email`, `pswd`, `Fnacimiento`, `ventaPrincipal`, `ventaSecundario`, `nReseteos`, `ultimoReseteo`, `nPSN`, `precio`, `precioVendido`, `estadoCuenta`, `juego_idJuego`) VALUES
(58, NULL, 'Adulto', 'chiclestoreps1@gmail.com', 'null100', '1994-09-21', 0, 0, 0, '', '', 0, 0, 1, 36),
(59, 58, 'Menor', 'jpejm5jh@yopmail.com', 'Lh4y6Am4', '2002-05-01', 2, 1, 1, '2019-10-11', 'StreetFighterCs1', 0, 0, 4, 37),
(60, 58, 'Menor', 't8csy7r4@yopmail.com', 'nHV2VvV5', '2002-05-10', 2, 0, 1, '2019-10-13', 'StreetFighterCs3', 0, 0, 4, 37),
(61, 58, 'Menor', 'vneed5jd@yopmail.com', 'N5PPMk3P', '2002-05-10', 2, 2, 1, '2019-10-19', 'DragonBallFZCS2', 0, 0, 4, 38),
(62, 58, 'Menor', 'nzaw8efb@yopmail.com', '84mUXJCv', '2002-05-10', 2, 1, 1, '2019-10-25', 'Fifa20Cs1', 0, 39, 4, 39),
(63, 58, 'Menor', 'cdgrzg5v@yopmail.com', 'v4Y8whfu', '2002-05-10', 2, 1, 1, '2019-10-12', 'CrashTRCs1', 0, 0, 4, 40),
(64, 58, 'Menor', 'bcsqdb3z@yopmail.com', 'Rnhs2aXQ', '2002-05-10', 2, 0, 1, '', 'StreetFighterCs2', 0, 0, 4, 37),
(65, NULL, 'Adulto', 'ch.i.clestoreper@gmail.com', 'y89GtKCq', '1970-01-01', 2, 0, 1, '2020-01-16', 'MortaKombCs1', 0, 0, 4, 41),
(66, 65, 'Menor', 'c.h.i.clestoreper@gmail.com', 'FS5yYaLP', '2005-01-01', 2, 0, 1, '2020-01-14', 'CrashTRCs11', 0, 0, 4, 40),
(67, NULL, 'Adulto', 'ch.ic.le.storeper@gmail.com', 'd6gWr3g2', '1970-01-01', 2, 0, 1, '2020-01-20', 'CallDuWWIICs1', 0, 39, 4, 42),
(68, NULL, 'Adulto', 'chiclestoreps11@gmail.com', 'liolio100', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(69, 68, 'Menor', '5knaek@moimoi.re', 'UdCKj5F8', '2005-01-01', 2, 0, 1, '2020-01-11', 'GodofWarCS5', 0, 0, 4, 43),
(70, NULL, 'Adulto', 'chiclestoreps2@gmail.com', 'averiguar', '1994-09-21', 0, 0, 0, '', '', 0, 0, 1, 36),
(71, 68, 'Menor', '7g2hm@moimoi.re', '7ZyvS2st', '2005-01-01', 2, 0, 1, '2020-09-11', 'GodofWarCS6', 0, 0, 4, 43),
(72, NULL, 'Adulto', 'chi.c.le.storeper@gmail.com', 'HdGD87BE', '1970-01-01', 1, 0, 1, '2020-01-18', 'DyingLigthCs1', 0, 0, 1, 44),
(73, 70, 'Menor', 'cwah9mte@yopmail.com', 't7WHZKp3', '2003-05-10', 2, 0, 1, '2019-11-03', 'Pes20Cs1', 0, 0, 4, 45),
(74, 68, 'Menor', '8pbnh@moimoi.re', 'MJkG7EAp', '2005-01-01', 2, 0, 1, '2020-01-11', 'GodofWarCS7', 0, 0, 4, 43),
(75, 70, 'Menor', 'p3t7h2lm@yopmail.com', 'DShCZ6YA', '2003-05-10', 2, 0, 1, '2019-12-30', 'CrashTRCs2', 0, 0, 4, 40),
(76, 72, 'Menor', 'c.hi.c.le.storeper@gmail.com', 'fB5gYGR6', '2005-01-01', 2, 0, 1, '2020-01-18', 'GodofWarCS18', 0, 0, 4, 43),
(77, 68, 'Menor', '73gi335@moimoi.re', 't44MSDnT', '2005-01-01', 2, 0, 1, '2020-01-11', 'GodofWarCS8', 0, 0, 4, 43),
(78, 68, 'Menor', '283m6m@moimoi.re', '9uGxnyg2', '2005-01-01', 2, 1, 1, '2020-01-10', 'GtaVCs1', 0, 0, 4, 46),
(79, 72, 'Menor', 'ch.i.c.le.storeper@gmail.com', 'y8cBwTpr', '2005-01-01', 2, 0, 1, '2020-01-20', 'DragonBallFZCS10', 0, 0, 4, 38),
(80, 68, 'Menor', 'e2ebc@moimoi.re', 'a7NLsnpt', '2005-01-01', 2, 0, 1, '2020-01-13', 'GtaVCs2', 0, 0, 4, 46),
(81, 70, 'Menor', 'hkkde@moimoi.re', 'WTSz4hBM', '2005-05-10', 2, 0, 1, '2019-12-28', 'Fifa20Cs2', 0, 0, 4, 39),
(82, 72, 'Menor', 'c.h.i.c.le.storeper@gmail.com', 'gwc39ZH6', '2005-01-01', 2, 0, 1, '2020-01-20', 'EASPORTCs1', 0, 0, 4, 47),
(83, 70, 'Menor', 'm2ga2j4@moimoi.re', 'V5Npt5Qn', '2005-01-01', 2, 1, 1, '2020-01-02', 'Pes20Cs2', 0, 0, 4, 45),
(84, 70, 'Menor', '7he292@moimoi.re', '8KQrY5Np', '2003-05-10', 2, 0, 1, '2020-01-04', '', 0, 0, 4, 48),
(85, NULL, 'Adulto', 'chiclestoreps12@gmail.com', 'liolio100', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(86, 72, 'Menor', 'chicl.e.storeper@gmail.com', 'WsBZXm3V', '2005-01-01', 2, 1, 1, '2020-01-24', 'DraBallKakaCs1', 0, 0, 4, 49),
(87, 85, 'Menor', '35anfi@moimoi.re', 'fT6jECrZ', '2005-01-01', 2, 0, 1, '2020-01-16', 'Pes20Cs13', 0, 0, 4, 45),
(88, 85, 'Menor', '833k@moimoi.re', 'ZBA3scQ4', '2005-01-01', 2, 0, 1, '2020-02-06', 'Pes20Cs14', 0, 0, 4, 45),
(89, 72, 'Menor', 'c.hicl.e.storeper@gmail.com', 'Z2RNXzwj', '2005-01-01', 2, 0, 1, '2020-01-18', 'Pes20Cs19', 0, 0, 4, 45),
(90, 85, 'Menor', 'f99d@moimoi.re', 'e2kj2AK4', '2005-01-01', 2, 0, 1, '2020-01-11', 'Pes20Cs15', 0, 0, 4, 45),
(91, 72, 'Menor', 'ch.icl.e.storeper@gmail.com', 'yjcC4cfq', '2005-01-01', 2, 0, 1, '2020-01-24', 'Pes20Cs20', 0, 0, 4, 45),
(92, 85, 'Menor', 'cf7npje@moimoi.re', 'GWn6s5Nb', '2005-01-01', 2, 0, 1, '2020-01-11', 'Pes20Cs16', 0, 0, 4, 45),
(93, NULL, 'Adulto', 'c.hi.cl.e.storeper@gmail.com', 'b4UrgxAP', '1970-01-01', 2, 0, 1, '2020-01-18', 'ResidentEvil2Cs1', 0, 0, 4, 51),
(94, 70, 'Menor', 'CFMMGM@MOIMOI.RE', '5kajEK2W', '2005-01-01', 2, 0, 1, '2020-01-04', 'CrashTRCs3', 0, 0, 4, 40),
(95, 85, 'Menor', '428ne9n@moimoi.re', '2EhREaPF', '2005-01-01', 2, 0, 1, '2020-01-11', 'Pes20Cs17', 0, 0, 4, 45),
(96, 85, 'Menor', 'kd37e@moimoi.re', 'H2YLfpZB', '2005-01-01', 2, 0, 1, '2020-01-14', 'Pes20Cs18', 0, 0, 4, 45),
(97, NULL, 'Adulto', 'chiclestoreps3@gmail.com', 'verificar', '1994-09-21', 0, 0, 0, '', '', 0, 0, 1, 36),
(98, NULL, 'Adulto', 'chiclestoreps13@gmail.com', 'liolio100x2x3y1', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(99, 97, 'Menor', 'gi5emg@moimoi.re', 'E5pazEJr', '2005-01-01', 2, 0, 1, '2020-01-02', 'DragonBallFZCS3', 0, 0, 4, 38),
(100, 98, 'Menor', '2da285f@moimoi.re', 'WJ9sjNmD', '2005-01-01', 2, 0, 1, '2020-01-14', 'GtaVCs3', 0, 0, 4, 46),
(101, 93, 'Menor', 'ch.i.cl.e.storeper@gmail.com', 'A6UhU7MS', '2005-01-01', 2, 0, 1, '2020-01-20', 'GtaVCs17', 0, 0, 4, 46),
(102, 97, 'Menor', '278mcim@moimoi.re', 'Zz8QRwmC', '2005-01-01', 2, 0, 1, '2020-01-02', 'GodofWarCS1', 0, 0, 4, 43),
(103, 98, 'Menor', '3n5jan@moimoi.re', 'XWg6LKct', '2005-01-01', 2, 1, 1, '2020-01-14', 'GtaVCs4', 0, 0, 4, 46),
(104, 97, 'Menor', 'pp4kaf@moimoi.re', 'gDWG3Ywq', '2005-01-01', 2, 0, 1, '2020-01-02', 'CoDMoWarCS1', 0, 0, 4, 52),
(105, 93, 'Menor', 'c.h.i.cl.e.storeper@gmail.com', '9ptrdNL5', '2005-01-01', 2, 0, 1, '2020-01-20', 'GtaVCs18', 0, 0, 4, 46),
(106, 97, 'Menor', 'hmp7c@moimoi.re', 'gSr8dD8A', '2005-01-01', 2, 0, 1, '2020-01-05', 'Nba20CS1', 0, 0, 4, 53),
(107, 98, 'Menor', 'aj85i@moimoi.re', 'cCq6hp9W', '2005-01-01', 2, 0, 1, '2020-01-13', 'GtaVCs5', 0, 0, 4, 46),
(108, 93, 'Menor', 'chic.l.e.storeper@gmail.com', 'zMPa22P2', '2005-01-01', 2, 0, 1, '2020-01-24', 'GtaVCs19', 0, 0, 4, 46),
(109, 97, 'Menor', 'c6b5bee@moimoi.re', 'hmg7HS39', '2005-01-01', 2, 0, 1, '2020-01-31', 'RocketLeagCS1', 0, 0, 4, 54),
(110, 97, 'Menor', '36a8@moimoi.re', 'x4aG9k6C', '2005-01-01', 1, 0, 1, '2020-02-28', 'Sims4CS1', 0, 34, 1, 55),
(111, 93, 'Menor', 'c.hic.l.e.storeper@gmail.com', '7VXE6nUe', '2005-01-01', 2, 0, 1, '2020-01-23', 'GtaVCs20', 0, 0, 4, 46),
(112, 98, 'Menor', '6mddg@moimoi.re', 'fY3Dte4m', '2005-01-01', 2, 1, 1, '2020-01-14', 'GtaVCs6', 0, 0, 4, 46),
(113, 98, 'Menor', '72c3jpk@moimoi.re', 'pAQ4rGsX', '2005-01-01', 2, 1, 1, '2020-01-11', 'GtaVCs7', 0, 0, 4, 46),
(114, 93, 'Menor', 'ch.ic.l.e.storeper@gmail.com', 'hSmf65jF', '2005-01-01', 2, 0, 1, '2020-01-31', 'GtaVCs21', 0, 0, 4, 46),
(115, 98, 'Menor', 'cdip6@moimoi.re', 'D2xaLCzH', '2005-01-01', 2, 0, 1, '2020-01-11', 'GtaVCs8', 0, 0, 4, 46),
(116, 93, 'Menor', 'c.h.ic.l.e.storeper@gmail.com', '7GdEEHta', '2005-01-01', 2, 0, 1, '2020-01-25', 'GtaVCs22', 0, 0, 4, 46),
(117, NULL, 'Adulto', 'chiclestoreps14@gmail.com', 'liolio100x2x3y1', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(118, NULL, 'Adulto', 'chi.c.l.e.storeper@gmail.com', 'aAM9gpRJ', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(119, 117, 'Menor', '9c55p@moimoi.re', 'A3bHaK4V', '2005-01-01', 2, 0, 1, '2020-02-15', 'PLvsZOBFN1', 0, 49, 4, 56),
(120, NULL, 'Adulto', 'chiclestoreps4@gmail.com', 'revisar', '1994-09-21', 0, 0, 0, '', '', 0, 0, 1, 36),
(121, 117, 'Menor', '2a2pm@moimoi.re', 'nYKH36E8', '2005-01-01', 2, 0, 1, '2020-01-14', 'NeedFSpedPay1', 0, 0, 4, 58),
(122, 120, 'Menor', 'kfmcc7@moimoi.re', 'fukU6tJD', '2005-01-01', 2, 0, 1, '2020-01-02', 'Fifa20Cs3', 0, 0, 4, 39),
(123, 117, 'Menor', 'j586ibp@moimoi.re', 'RcfNJ9SL', '2005-01-01', 2, 0, 1, '2020-01-11', 'NeedFSpedPay2', 0, 0, 4, 58),
(124, 118, 'Menor', 'nah995@moimoi.re', '3PjNHQgR', '2005-01-01', 2, 0, 1, '2020-01-20', 'DeMaCrCs1', 0, 0, 4, 57),
(125, 120, 'Menor', 'ck7f27@moimoi.re', 'kg8eV7nv', '2005-01-01', 2, 0, 1, '2020-01-02', 'Pes20Cs3', 0, 0, 4, 45),
(126, 117, 'Menor', 'mabce@moimoi.re', '78x4Fm6M', '2005-01-01', 2, 0, 1, '2020-01-10', 'SBomberman1', 0, 0, 4, 59),
(127, 118, 'Menor', '3jih6bg@moimoi.re', 'BzaWSu85', '2005-01-01', 2, 0, 1, '2020-01-20', 'GodofWarCS16', 0, 0, 4, 43),
(128, 120, 'Menor', 'pi7f5@moimoi.re', 'vF9mYACe', '2005-01-01', 2, 0, 1, '2020-01-05', 'Pes20Cs5', 0, 0, 4, 45),
(129, 120, 'Menor', 'gc844bp@moimoi.re', 'wGAy4PQj', '2005-01-01', 2, 0, 1, '2020-01-16', 'CrashTRCs81', 0, 0, 4, 40),
(130, 118, 'Menor', '2imhi@moimoi.re', 'JcS69LVZ', '2005-01-01', 2, 0, 1, '2020-01-20', 'GodofWarCS17', 0, 0, 4, 43),
(131, 117, 'Menor', '5ik83k@moimoi.re', 'Ah3f6CPC', '2005-01-01', 2, 0, 1, '2020-01-14', 'tL8NS3Gw', 0, 0, 4, 60),
(132, 120, 'Menor', 'ipb6m3@moimoi.re', '2qBC9Q74', '2005-01-01', 2, 0, 1, '2020-01-14', 'CrashTRCs9', 0, 0, 4, 40),
(133, 118, 'Menor', '8p8m9m@moimoi.re', 'Prvz2eVv', '2005-01-01', 2, 0, 1, '2020-01-20', 'JumpForceCs1', 0, 0, 4, 61),
(134, 117, 'Menor', '2pfnnk@moimoi.re', '7HVMzBKk', '2005-01-01', 1, 0, 1, '2020-02-28', 'BatFIELDCS1', 0, 12, 1, 62),
(135, 118, 'Menor', '7ji3j@moimoi.re', '5Y3gNaXG', '2005-01-01', 2, 0, 1, '2020-01-22', 'JumpFroceCs2', 0, 55, 4, 61),
(136, 118, 'Menor', '7id92ja@moimoi.re', 'WFmM57wH', '2005-01-01', 2, 0, 1, '2020-01-20', 'GtaVCs23', 0, 0, 4, 46),
(137, NULL, 'Adulto', 'chiclestoreps5@gmail.com', 'verificar', '1994-09-21', 0, 0, 0, '', '', 0, 0, 1, 36),
(138, 137, 'Menor', '72k77@moimoi.re', 'W7v6BuKR', '2005-01-01', 2, 0, 1, '2020-01-05', 'Pes20Cs6', 0, 0, 4, 45),
(139, NULL, 'Adulto', 'chicl.estoreper@gmail.com', '7HVMzBKk', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(140, NULL, 'Adulto', 'chi.cles.toreper@gmail.com', 'NQ2CkbQQ', '1970-01-01', 2, 1, 1, '2020-01-24', 'GtaVCs24', 0, 0, 4, 100),
(141, 137, 'Menor', 'k5eh73@moimoi.re', 'uqCVwZ2n', '2005-01-01', 2, 0, 1, '2020-01-04', 'Pes20Cs7', 0, 0, 4, 45),
(142, 137, 'Menor', 'kai3n5@moimoi.re', 'zPT5wFdc', '2005-01-01', 2, 0, 1, '2020-01-06', 'Pes20Chs8', 0, 0, 4, 45),
(143, 137, 'Menor', 'j5m@moimoi.re', 'VwJ3kB6h', '2005-01-01', 2, 0, 1, '2020-01-03', 'Pes20Cs9', 0, 0, 4, 45),
(144, 139, 'Menor', '9dcph@moimoi.re', 'qNXF5MSu', '2005-01-01', 2, 0, 1, '2020-01-14', 'CoDBlaOpsCS1', 0, 0, 4, 45),
(145, 140, 'Menor', '4n7n7b@moimoi.re', 'Whv3CJSp', '2005-01-01', 2, 0, 1, '2020-01-21', 'DragonBallFZCS11', 0, 0, 4, 38),
(146, 137, 'Menor', 'h78859f@moimoi.re', 'c3KCU6Zf', '2005-01-01', 2, 0, 1, '2020-01-04', 'HorizonCs1', 0, 0, 4, 64),
(147, 139, 'Menor', 'ggj9hdk@moimoi.re', 'n8AemJuh', '2005-01-01', 2, 0, 1, '2020-01-14', 'GodofWarCS10', 0, 0, 4, 43),
(148, 137, 'Menor', 'k7ci@moimoi.re', 'X7c6fn8V', '2005-01-01', 2, 0, 1, '2020-01-07', 'GodofWarCS2', 0, 0, 4, 43),
(149, 139, 'Menor', '9f7j9d@moimoi.re', 'T8mNCdxB', '2005-01-01', 2, 0, 1, '2020-01-14', 'ResiEviCS1', 0, 0, 4, 65),
(150, 140, 'Menor', 'i9aeg65@moimoi.re', 'rG9bmFbM', '2005-01-01', 2, 0, 1, '2020-01-20', 'DragonBallFZCS12', 0, 0, 4, 38),
(151, 139, 'Menor', '2idake2@moimoi.re', 'pKUUS76s', '2005-01-01', 1, 0, 1, '2020-01-14', 'MonstHunWorCS1', 0, 0, 1, 66),
(152, 139, 'Menor', 'cd7hj@moimoi.re', 'wmAnp2Yh', '2005-01-01', 2, 0, 1, '2020-01-13', 'RocketLeagCS4', 0, 0, 4, 54),
(153, 140, 'Menor', '8ndm8@moimoi.re', 'wP2FkzW5', '2005-01-01', 2, 0, 1, '2020-01-18', 'DragonBallFZCS13', 0, 0, 4, 38),
(154, 139, 'Menor', '82pm@moimoi.re', 'bS6spvWf', '2005-01-01', 2, 0, 1, '2020-01-21', 'GodofWarCS9', 0, 0, 4, 43),
(155, NULL, 'Adulto', 'chiclestoreper@gmail.com', 'ByK8eCLY', '1970-01-01', 2, 0, 1, '2020-02-04', 'CallDuBlCs1', 0, 0, 4, 67),
(156, NULL, 'Adulto', 'chiclestoreps6@yahoo.com', 'verificar', '1994-09-24', 0, 0, 0, '', '', 0, 0, 1, 36),
(157, 156, 'Menor', 'acj8ajp@moimoi.re', '23hH29bx', '2005-01-01', 2, 0, 1, '2019-12-31', 'Pes20Cs10', 0, 0, 4, 45),
(158, 156, 'Menor', 'ia2d8a@moimoi.re', 'QJ7Ukd58', '2005-01-01', 2, 0, 1, '2020-01-03', 'Pes20Cs11', 0, 0, 4, 45),
(159, 155, 'Menor', 'c.hiclestoreper@gmail.com', '4KtcNBen', '2005-01-01', 2, 0, 1, '2020-01-14', 'GtaVCs9', 0, 0, 4, 46),
(160, 156, 'Menor', 'pj7i5@moimoi.re', 'UafxBC6c', '2005-01-01', 2, 0, 1, '2020-01-06', 'Pes20Cs12', 0, 0, 4, 45),
(161, 156, 'Menor', 'g2287@moimoi.re', '83MXc5vg', '2005-01-01', 2, 0, 1, '2020-01-06', 'RocketLeagCS2', 0, 0, 4, 54),
(162, 156, 'Menor', 'd3i38h5@moimoi.re', '7CfEa8xq', '2005-01-01', 2, 0, 1, '2020-01-04', 'DragonBallFZCS4', 0, 0, 4, 38),
(163, 140, 'Menor', 'fcjgj5@moimoi.re', 'd3nD9dPB', '2005-01-01', 2, 0, 1, '2020-01-21', 'DragonBallFZCS14', 0, 0, 4, 38),
(164, 156, 'Menor', 'p3e9gb@moimoi.re', '7CfEa8xq', '2005-01-01', 2, 0, 1, '2020-01-08', 'BatmanAXCS1', 0, 0, 4, 38),
(165, NULL, 'Adulto', 'ch.iclestoreper@gmail.com', 'f5ENaGpV', '1970-01-01', 2, 0, 1, '2020-01-15', 'WorWarZCs1', 0, 0, 4, 68),
(166, 165, 'Menor', 'c.h.iclestoreper@gmail.com', '4JTrzcEX', '2005-01-01', 2, 0, 1, '2020-01-14', 'CrashTRCs10', 0, 0, 4, 40),
(168, NULL, 'Adulto', 'chi.clestoreper@gmail.com', 'b96xehRd', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(169, 168, 'Menor', '5pfpae@moimoi.re', 'f9FL8MVg', '2005-01-01', 2, 0, 1, '2020-01-16', 'GtaVCs10', 0, 0, 4, 46),
(170, NULL, 'Adulto', 'ch.i.cles.toreper@gmail.com', 'XtHNaT2h', '1970-01-01', 1, 0, 1, '2020-02-17', 'TheLastOfCs1', 0, 49, 1, 73),
(171, 168, 'Menor', 'n5a6id@moimoi.re', 'YJJ2DLrJ', '2005-01-01', 2, 0, 1, '2020-01-16', 'GtaVCs11', 0, 0, 4, 46),
(172, 168, 'Menor', '3hepnb@moimoi.re', '2muyP2Dk', '2005-01-01', 2, 0, 1, '2019-01-16', 'GtaVCs12', 0, 0, 4, 46),
(173, 170, 'Menor', 'c.h.i.cles.toreper@gmail.com', '9A6JppL5', '2005-01-01', 2, 0, 1, '2020-02-20', 'EASPORTCs2', 0, 25, 4, 47),
(174, 168, 'Menor', '9jeh8@moimoi.re', 'gLDnv6bK', '2005-01-01', 2, 0, 1, '2020-01-14', 'GtaVCs13', 0, 0, 4, 46),
(175, 170, 'Menor', 'chic.les.toreper@gmail.com', 'f2urArLN', '2005-01-01', 2, 0, 1, '2020-02-04', 'EASPORTCs3', 0, 0, 4, 47),
(176, 168, 'Menor', '76g7j6@moimoi.re', 'xtgHbL2u', '2005-01-01', 2, 0, 1, '2020-01-14', 'GtaVCs14', 0, 0, 4, 46),
(178, 170, 'Menor', 'c.hic.les.toreper@gmail.com', 'xFEsN7JL', '2005-01-01', 2, 0, 1, '2020-01-24', 'DragXenoBunCs2', 0, 0, 4, 74),
(180, 170, 'Menor', 'ch.ic.les.toreper@gmail.com', 'Zu5FafQz', '2005-01-01', 2, 0, 1, '2020-01-25', 'PlaZombCs1', 0, 0, 4, 75),
(194, 168, 'Menor', '8c2fkda@moimoi.re', '2BQyMAzt', '2005-01-01', 2, 0, 1, '2020-01-14', 'GtaVCs15', 0, 0, 4, 46),
(197, NULL, 'Adulto', 'chiclestoreps7@gmail.com', 'verificar', '1994-09-21', 0, 0, 0, '', '', 0, 0, 1, 36),
(198, 197, 'Menor', '4bhfkfp@moimoi.re', '4aVYcbCw', '2005-01-01', 2, 0, 1, '2020-01-05', 'TekkenCS1', 0, 0, 4, 76),
(199, NULL, 'Adulto', 'c.hi.clestoreper@gmail.com', '9uSnW4ur', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(200, 199, 'Menor', 'ni2gmh@moimoi.re', 'YztZy6WG', '2005-01-01', 2, 0, 1, '2020-01-18', 'GtaVCs15', 0, 0, 4, 46),
(201, 199, 'Menor', 'eeme8@moimoi.re', 'Y3EtvVpY', '2005-01-01', 2, 0, 1, '2020-01-14', 'GtaVCs16', 0, 0, 4, 46),
(202, 199, 'Menor', '8cna@moimoi.re', 'uGrH7Cyq', '2005-01-01', 2, 0, 1, '2020-01-20', 'DragonBallFZCS6', 0, 0, 4, 38),
(203, 197, 'Menor', 'gn9mhm@moimoi.re', 'kUJu3QCS', '2005-01-01', 2, 0, 1, '2020-01-06', 'TekkenCS2', 0, 0, 4, 76),
(204, 199, 'Menor', '2fgk@moimoi.re', 'gVcHK9aZ', '2005-01-01', 2, 0, 1, '2020-01-14', 'DragonBallFZCS7', 0, 0, 4, 38),
(205, 197, 'Menor', '6584h@moimoi.re', 'H9dS4Fbx', '2005-01-01', 2, 0, 1, '2020-01-06', 'CrashTRCs4', 0, 0, 4, 40),
(206, 199, 'Menor', '2p2m4p7@moimoi.re', 'J9gSvFvD', '2005-01-01', 2, 0, 1, '2020-01-14', 'DragonBallFZCS8', 0, 0, 4, 38),
(207, 199, 'Menor', '6ihd5@moimoi.re', 'F8Jh2TcP', '2005-01-01', 2, 0, 1, '2020-01-14', 'NarutoUS4CS1', 0, 0, 4, 77),
(208, 170, 'Menor', 'c.h.ic.les.toreper@gmail.com', '2wbyL5ZE', '2005-01-01', 2, 0, 1, '2020-01-30', 'PlaZombCs2', 0, 0, 4, 75),
(209, NULL, 'Adulto', 'ch.i.cl.estoreper@gmail.com', 'LV3hr4ak', '1970-01-01', 2, 0, 1, '2020-01-21', 'BattleFieldCS1', 0, 0, 4, 78),
(210, 170, 'Menor', 'chi.c.les.toreper@gmail.com', 'jT33xVw7', '2005-01-01', 2, 0, 1, '2020-02-04', 'PlaZombCs3', 0, 0, 4, 75),
(211, 209, 'Menor', 'c.h.i.cl.estoreper@gmail.com', '74Zf9V6x', '2005-01-01', 2, 0, 1, '2020-01-14', 'SaintSeyaCS1', 0, 0, 4, 79),
(212, 197, 'Menor', 'm465h@moimoi.re', 'ut28kyW4', '2005-01-01', 2, 0, 1, '2020-01-06', 'Spidermancs11', 0, 0, 4, 80),
(213, 197, 'Menor', '2fi2p@moimoi.re', 'eWZ5GMsT', '2005-01-01', 2, 0, 1, '2020-01-06', 'Spidermancs2', 0, 0, 4, 36),
(214, 197, 'Menor', '43kgim@moimoi.re', 'GBHm5FdQ', '2005-01-01', 2, 0, 1, '2020-01-15', 'RocketLeagCS3', 0, 0, 4, 54),
(215, NULL, 'Adulto', 'chic.l.estoreper@gmail.com', 'YsKKB4az', '1970-01-01', 2, 0, 1, '2020-01-14', 'GodWarIIICs1', 0, 0, 4, 81),
(216, NULL, 'Adulto', 'c.hic.l.estoreper@gmail.com', 'bf8LQC8D', '2005-01-01', 2, 0, 1, '2020-01-21', 'ZombArmyCs1 ', 0, 11, 4, 82),
(217, 216, 'Menor', 'ch.ic.l.estoreper@gmail.com', 'gmvZYT97', '2005-01-01', 2, 0, 1, '2020-01-14', 'DragonBallFZCS9', 0, 0, 4, 38),
(218, NULL, 'Adulto', 'chi.c.l.estoreper@gmail.com', 'prezj8An', '2005-01-01', 2, 0, 1, '2020-01-16', 'AssCreedDeluCS1', 0, 0, 4, 83),
(219, 218, 'Menor', 'ch.i.c.l.estoreper@gmail.com', '3qJkyaTC', '2005-01-01', 2, 0, 1, '2020-01-17', 'TekkenCS3', 0, 0, 4, 76),
(220, NULL, 'Adulto', 'chiclestoreps8@gmail.com', 'verificar', '1994-09-21', 0, 0, 0, '', '', 0, 0, 1, 36),
(221, NULL, 'Adulto', 'c.hi.c.l.estoreper@gmail.com', 'dS2DADk2', '1970-01-01', 2, 0, 1, '2020-01-18', 'AssCreedOCS1', 0, 0, 4, 84),
(222, 220, 'Menor', 'f9n9@moimoi.re', 'aN9HVtFC', '2005-01-01', 2, 0, 1, '2020-01-05', 'DragonBallFZCS5', 0, 0, 4, 38),
(223, 221, 'Menor', 'c.h.i.c.l.estoreper@gmail.com', 'pTuMA6qp', '2005-01-01', 2, 0, 1, '2020-01-17', 'GodofWarCS11', 0, 0, 4, 43),
(224, 220, 'Menor', '657p2h@moimoi.re', 'ysQMUm6L', '2005-01-01', 2, 0, 1, '2020-01-05', 'UnchartedCS1', 0, 0, 4, 85),
(225, NULL, 'Adulto', 'chic.le.storeper@gmail.com', 'AtZQU3Ft', '2005-01-01', 2, 0, 1, '2020-01-16', 'KillingFlorCs1', 0, 19, 4, 86),
(226, 220, 'Menor', '2i6me@moimoi.re', '3dBhKqPP', '2005-01-01', 2, 0, 1, '2020-01-05', 'UntilDownCs1', 0, 0, 4, 38),
(228, 220, 'Menor', '6m73b5@moimoi.re', 'Hr3Sn9PP', '2005-01-01', 2, 0, 1, '2020-01-05', 'Fifa20Cs4', 0, 0, 4, 39),
(229, 225, 'Menor', 'c.hicle.storeper@gmail.com', 'L2Ub52SV', '2005-01-01', 1, 0, 1, '2020-01-17', 'BatmanRetCs1', 0, 0, 1, 87),
(230, 220, 'Menor', 'j7d5ek@moimoi.re', 'gv8RXV3e', '2005-01-01', 2, 0, 1, '2020-01-05', 'Fifa20Cs5', 0, 0, 4, 39),
(231, 225, 'Menor', 'ch.icle.storeper@gmail.com', 'TCjg96E7', '2005-01-01', 2, 0, 1, '2020-01-17', 'TekkenCS4', 0, 0, 4, 76),
(232, 220, 'Menor', '59e47e4@moimoi.re', 'pdCs5V3T', '2005-01-01', 2, 0, 1, '2020-01-05', 'DragonBallFZCS6', 0, 0, 4, 38),
(233, 225, 'Menor', 'c.h.icle.storeper@gmail.com', 'W68ZAruX', '2005-01-01', 2, 0, 1, '2020-01-17', 'TekkenCS5', 0, 0, 4, 76),
(234, 225, 'Menor', 'c.h.i.cle.storeper@gmail.com', 'M6TzN2EA', '2005-01-01', 2, 0, 1, '2020-01-17', 'GodofWarCS12', 0, 0, 4, 43),
(235, NULL, 'Adulto', 'chiclestoreps9@gmail.com', 'verificar', '1994-09-21', 0, 0, 0, '', '', 0, 0, 1, 36),
(236, 225, 'Menor', 'chi.cle.storeper@gmail.com', '4gTDce5t', '2005-01-01', 2, 0, 1, '2020-02-15', 'GodofWarCS13', 0, 49, 4, 43),
(237, 235, 'Menor', '2hg9i4@moimoi.re', 'zuK7KHaB', '2005-01-01', 2, 1, 1, '2020-01-08', 'Fifa20Cs6', 0, 0, 4, 39),
(238, 225, 'Menor', 'ch.i.cle.storeper@gmail.com', 'ydB7Hwsy', '2005-01-01', 2, 0, 1, '2020-01-20', 'GodofWarCS14', 0, 0, 4, 43),
(239, 235, 'Menor', 'ffac5@moimoi.re', 'p9Z4eK8y', '2005-01-01', 2, 0, 1, '2020-01-08', 'CrashTRCs5', 0, 0, 4, 40),
(240, 235, 'Menor', 'fnai6p@moimoi.re', 'yvwZ64Tr', '2005-01-01', 2, 0, 1, '2020-01-14', 'Fifa20Cs7', 0, 0, 4, 39),
(241, 235, 'Menor', '5kd4e@moimoi.re', 't44MSDnT', '2005-01-01', 2, 0, 1, '2020-01-08', 'WwkCs1', 0, 0, 4, 88),
(242, 235, 'Menor', '3n36b@moimoi.re', '9uGxnyg2', '2005-01-01', 2, 0, 1, '2020-01-14', 'Fifa20Css8', 0, 0, 4, 39),
(243, NULL, 'Adulto', 'c.hi.c.les.toreper@gmail.com', 'z6jFC3Gz', '1970-01-01', 1, 0, 1, '2020-01-25', 'AWayOutCs1', 0, 0, 1, 89),
(244, 235, 'Menor', 'cg2njk@moimoi.re', '4HBELp4w', '2005-01-01', 2, 1, 1, '2020-01-10', 'GodofWarCS3', 0, 0, 4, 43),
(245, NULL, 'Adulto', 'chiclestoreps10@gmail.com', 'verificar', '1994-09-21', 0, 0, 0, '', '', 0, 0, 1, 36),
(246, 243, 'Menor', 'ch.i.c.les.toreper@gmail.com', '5qnpJYty', '2005-01-01', 2, 0, 1, '2020-02-17', 'GtaVCs26', 0, 34, 4, 100),
(247, 243, 'Menor', 'c.h.i.c.les.toreper@gmail.com', 'C6d4dhdv', '2005-01-01', 2, 0, 1, '2020-01-23', 'GtaVCs27', 0, 0, 4, 100),
(248, 245, 'Menor', '2e4kaf3@moimoi.re', 'UdCKj5F8', '2005-01-01', 2, 0, 1, '2020-01-15', 'DragonBallX2Cs1', 0, 0, 4, 90),
(249, 245, 'Menor', '3eebg@moimoi.re', '7ZyvS2st', '2005-01-01', 2, 0, 1, '2020-01-05', 'CrashTRCs6', 0, 0, 4, 40),
(250, 243, 'Menor', 'chicl.es.toreper@gmail.com', 'XWuCsF6q', '2005-01-01', 2, 0, 1, '2020-01-23', 'GtaVCs28', 0, 0, 4, 100),
(251, 245, 'Menor', '5b7g8pa@moimoi.re', 'MJkG7EAp', '2005-01-01', 2, 0, 1, '2020-01-08', 'Fifa20Css9', 0, 0, 4, 39),
(252, 243, 'Menor', 'c.hicl.es.toreper@gmail.com', 'zBJ5xNxE', '2005-01-01', 2, 0, 1, '2020-01-27', 'GtaVCs29', 0, 0, 4, 100),
(253, 245, 'Menor', '2aaph@moimoi.re', 'WMgvHj2p', '2005-01-01', 2, 0, 1, '2020-05-06', 'Injustice2Cs1', 0, 0, 4, 91),
(254, 245, 'Menor', 'c48nk8a@moimoi.re', 'Sx5nnARv', '2005-01-01', 2, 0, 1, '2020-01-06', 'GodofWarCS4', 0, 0, 4, 43),
(255, 243, 'Menor', 'ch.icl.es.toreper@gmail.com', 'TJbtA2FY', '2005-01-01', 2, 0, 1, '2020-01-23', 'GtaVCs30', 0, 0, 4, 100),
(256, 245, 'Menor', '937ea38@moimoi.re', 'jt6YqkBL', '2005-01-01', 2, 0, 1, '2020-01-09', 'CrashTRCs7', 0, 0, 4, 40),
(257, 243, 'Menor', 'c.h.icl.es.toreper@gmail.com', 'yNew8Qk2', '2005-01-01', 2, 0, 1, '2020-01-24', 'GtaVCs31', 0, 0, 4, 100),
(258, NULL, 'Adulto', 'chi.cl.es.toreper@gmail.com', '5UjXtZnf', '1970-01-01', 2, 0, 1, '2020-01-30', 'TomRaiderCs1', 0, 0, 4, 92),
(259, 258, 'Menor', 'p28mhe@moimoi.re', '4TjjqJew', '2005-01-01', 2, 0, 1, '2020-01-27', 'PlaZombCs4', 0, 0, 4, 75),
(260, 258, 'Menor', 'kpbcc5@moimoi.re', 'N9ZQNDhh', '2005-01-01', 2, 0, 1, '2020-02-04', 'PlaZombCs5', 0, 0, 4, 75),
(261, 258, 'Menor', 'e2cn298@moimoi.re', 'nzcN3aet', '2005-01-01', 2, 0, 1, '2020-02-10', 'StreetFighterCs5', 0, 0, 4, 37),
(263, 258, 'Menor', '2f4m8k@moimoi.re', 'wghMsy6C', '2005-01-01', 2, 0, 1, '2020-01-23', 'EASPORTCs4', 0, 0, 4, 47),
(264, 120, 'Menor', 'di58g@moimoi.re', '7rJeY3GR', '2005-01-01', 2, 0, 1, '2020-01-05', 'Pes20Cs4', 0, 0, 4, 45),
(265, NULL, 'Adulto', 'ch.i.cl.es.toreper@gmail.com', 'FNmQ3K5A', '1970-01-01', 2, 0, 1, '2020-01-23', 'WatchDoCs1', 0, 0, 4, 94),
(266, 265, 'Menor', 'c.h.i.cl.es.toreper@gmail.com', '3pDnGUKa', '2005-01-01', 2, 0, 1, '2020-01-24', 'DragXenoBunCs1', 0, 0, 4, 74),
(267, 265, 'Menor', 'chic.l.es.toreper@gmail.com', '7Utvp2LV', '2005-01-01', 2, 0, 1, '2020-01-23', 'EASPORTCs5', 0, 0, 4, 95),
(268, 265, 'Menor', 'c.hic.l.es.toreper@gmail.com', 'SKD9Tp4G', '2005-01-01', 2, 0, 1, '2020-01-31', 'EASPORTCs6', 0, 0, 4, 47),
(269, NULL, 'Adulto', 'ch.ic.l.es.toreper@gmail.com', 'K4ewArnG', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(270, 269, 'Menor', 'ch.icle.s.toreper@gmail.com', 'B57pk6ds', '2005-01-01', 1, 0, 1, '', 'CarsDrivCs1', 0, 0, 1, 96),
(271, 269, 'Menor', 'chi.c.l.es.toreper@gmail.com', 'kGg5ekzS', '2005-01-01', 2, 0, 1, '2020-02-05', 'EASPORTCs7', 0, 0, 4, 47),
(272, 269, 'Menor', 'c.hi.c.l.es.toreper@gmail.com', 'Vs4XeNsk', '2005-01-01', 2, 0, 1, '2020-01-27', 'EASPORTCs8', 0, 0, 4, 47),
(273, 269, 'Menor', 'ch.i.c.l.es.toreper@gmail.com', 'mXHPJ5fY', '2005-01-01', 2, 0, 1, '2020-01-30', 'EASPORTCs9', 0, 0, 4, 47),
(274, 269, 'Menor', 'c.h.i.c.l.es.toreper@gmail.com', 'ZrL7d2cU', '2005-01-01', 2, 0, 1, '2020-01-30', 'EASPORTCs10', 0, 0, 4, 47),
(277, 140, 'Menor', 'i3f5p7@moimoi.re', '22YrDzzU', '2005-01-01', 1, 0, 2, '2020-02-04', 'TonyHawkCs1', 0, 0, 1, 97),
(282, NULL, 'Adulto', 'c.hicle.s.toreper@gmail.com', 'tsA64Uab', '1970-01-01', 1, 0, 1, '2020-02-03', 'TitanFallUlCs1', 12, 29, 1, 98),
(283, 282, 'Menor', 'c.h.icle.s.toreper@gmail.com', 'MM3RNz6s', '2005-01-01', 2, 1, 1, '2020-02-01', 'Fifa20Cs', 60, 0, 4, 39),
(285, 282, 'Menor', 'chi.cle.s.toreper@gmail.com', '2gDtPdPx', '2005-01-01', 2, 0, 1, '2020-02-10', 'CrashTRCs12', 40, 0, 4, 40),
(286, 282, 'Menor', 'ch.i.cle.s.toreper@gmail.com', 'SPxdTR5H', '2005-01-01', 2, 0, 0, '2020-02-21', 'CrashTRCs13', 40, 59, 2, 40),
(287, 282, 'Menor', 'c.h.i.cle.s.toreper@gmail.com', 'hDnv6MZw', '2005-01-01', 2, 0, 1, '2020-02-03', 'DragXenoBunCs3', 17, 0, 4, 74),
(288, NULL, 'Adulto', 'chic.le.s.toreper@gmail.com', 'Yb3wHLNk', '1970-01-01', 1, 0, 1, '2020-02-19', 'TheLastOfCs2', 20, 49, 1, 73),
(289, 288, 'Menor', 'c.hic.le.s.toreper@gmail.com', 'HAxRk99y', '2005-01-01', 2, 0, 1, '2020-02-05', 'CrashTRCs14', 40, 0, 4, 40),
(290, 288, 'Menor', 'ch.ic.le.s.toreper@gmail.com', 'veYb2YLx', '2005-01-01', 2, 0, 1, '2020-02-10', 'GtaVCs33', 15, 0, 4, 100),
(292, 288, 'Menor', 'chi.c.le.s.toreper@gmail.com', '483HWdj3', '2005-01-01', 2, 0, 1, '2020-02-25', 'GtaVCs35', 15, 34, 4, 100),
(293, 288, 'Menor', 'c.hi.c.le.s.toreper@gmail.com', '2xZNVugy', '2005-01-01', 1, 0, 0, '', 'InjustLegCs1', 15, 34, 2, 101),
(294, NULL, 'Adulto', 'chicl.e.s.toreper@gmail.com', 'gkZQQ24g', '1970-01-01', 2, 0, 1, '2020-02-10', 'ResidEv7Cs1', 15, 0, 4, 99),
(295, 294, 'Menor', 'c.hicl.e.s.toreper@gmail.com', 'nucP6cL2', '2005-01-01', 2, 0, 1, '2020-02-06', 'PlaZombCs6', 5, 0, 4, 75),
(296, 294, 'Menor', 'ch.icl.e.s.toreper@gmail.com', 'vtc3cKjn', '2005-01-01', 2, 0, 1, '2020-02-07', 'PlaZombCs7', 5, 0, 4, 75),
(297, 294, 'Menor', 'c.h.icl.e.s.toreper@gmail.com', '2jGExFtC', '2005-01-01', 2, 0, 1, '2020-02-11', 'PlaZombCs8', 5, 0, 4, 75),
(298, 294, 'Menor', 'chi.cl.e.s.toreper@gmail.com', '8rDa73wm', '2005-01-01', 2, 0, 1, '2020-02-04', 'PlaZombCs9', 5, 0, 4, 75),
(299, NULL, 'Adulto', 'c.hi.cl.e.s.toreper@gmail.com', 'QvxV7z7B', '1970-01-01', 2, 0, 1, '2020-02-10', 'GodofWarCS19        ', 20, 0, 4, 43),
(300, 299, 'Menor', 'ch.i.cl.e.s.toreper@gmail.com', 'Adv4TE5t', '2005-01-01', 2, 0, 1, '2020-02-04', 'DragXenoBunCs4', 17, 0, 4, 74),
(301, 299, 'Menor', 'c.h.i.cl.e.s.toreper@gmail.com', 'Sq4wgHEE', '2005-01-01', 2, 0, 1, '2020-02-04', 'EASPORTCs11', 6, 0, 4, 47),
(302, 299, 'Menor', 'chic.l.e.s.toreper@gmail.com', 'fTeX4qjJ', '2005-01-01', 2, 0, 1, '2020-02-04', 'EASPORTCs12', 6, 0, 4, 47),
(303, 299, 'Menor', 'c.hic.l.e.s.toreper@gmail.com', 'Xkdc5RSa', '2005-01-01', 2, 0, 1, '2020-02-10', 'EASPORTCs13', 6, 0, 4, 47),
(304, 299, 'Menor', 'ch.ic.l.e.s.toreper@gmail.com', 'v8QCHRv4', '2005-01-01', 2, 0, 1, '2020-02-03', 'SBomberman2', 40, 0, 4, 59),
(305, 299, 'Menor', 'c.h.ic.l.e.s.toreper@gmail.com', 'Gsz2vY3e', '2005-01-01', 2, 0, 1, '2020-02-07', 'PlaZombCs10', 5, 0, 4, 75),
(306, NULL, 'Adulto', 'chiclest.oreper@gmail.com', 'AdB6e3zd', '1970-01-01', 1, 0, 1, '2020-02-10', 'Mafia3Cs1', 10, 24, 1, 103),
(307, 306, 'Menor', 'c.h.iclest.oreper@gmail.com', 'ADkHHA9K', '2005-01-01', 2, 0, 1, '2020-02-06', 'CrashTRCs14', 40, 0, 4, 40),
(308, 306, 'Menor', 'c.hiclest.oreper@gmail.com', 'WUjE7Dcm', '2005-01-01', 2, 0, 1, '2020-02-05', 'EASPORTCs18', 6, 0, 4, 47),
(309, 306, 'Menor', 'chi.c.l.e.s.toreper@gmail.com', 'w6kdH3gM', '2005-01-01', 2, 0, 1, '2020-02-07', 'EASPORTCs17', 6, 0, 4, 47),
(310, 306, 'Menor', 'c.h.i.c.l.e.s.toreper@gmail.com', 'ZfeGLz4g', '2005-01-01', 2, 0, 1, '2020-02-07', 'EASPORTCs16', 6, 0, 4, 47),
(311, 306, 'Menor', 'ch.i.c.l.e.s.toreper@gmail.com', 'Gaw527z8', '2005-01-01', 2, 0, 1, '2020-02-12', 'EASPORTCs15', 6, 0, 4, 47),
(312, 306, 'Menor', 'c.hi.c.l.e.s.toreper@gmail.com', '2d8QfUPg', '2005-01-01', 2, 0, 1, '2020-02-05', 'EASPORTCs14', 6, 0, 4, 47),
(319, NULL, 'Adulto', 'chi.clest.oreper@gmail.com', 'PXsfVs2N', '1970-01-01', 2, 0, 1, '2020-02-05', 'Resd1A2Cs1', 16, 0, 4, 107),
(320, 319, 'Menor', 'ch.i.clest.oreper@gmail.com', 'Wey5V6Be', '2005-01-01', 2, 0, 1, '2020-02-06', 'GtaVCs36', 15, 0, 4, 100),
(321, 319, 'Menor', 'chic.lest.oreper@gmail.com', 'XcnTmG5F', '2005-01-01', 1, 0, 1, '2020-02-05', 'OnePiecCs1', 12, 29, 1, 104),
(322, 319, 'Menor', 'c.hic.lest.oreper@gmail.com', '7dFtn9Lx', '2005-01-01', 2, 0, 1, '2020-02-12', 'DragXenoBunCs5', 17, 39, 4, 74),
(323, 319, 'Menor', 'ch.ic.lest.oreper@gmail.com', 'dv9FXWJf', '2005-01-01', 1, 0, 1, '2020-02-06', 'PlZombGar2Cs1', 20, 49, 1, 106),
(324, 319, 'Menor', 'c.hi.clest.oreper@gmail.com', 'P2uTwheq', '2005-01-01', 2, 0, 1, '2020-02-05', 'CrashTRCs15', 40, 0, 4, 40),
(325, 319, 'Menor', 'c.h.ic.lest.oreper@gmail.com', '89J8aWWb', '2005-01-01', 2, 0, 1, '2020-02-05', 'GtaVCs37', 15, 0, 4, 100),
(327, NULL, 'Adulto', 'chi.c.lest.oreper@gmail.com', 'PXsfVs2N', '1970-01-01', 0, 0, 0, '', '', 0, 0, 1, 36),
(328, 327, 'Menor', 'c.hi.c.lest.oreper@gmail.com', 'u9sH7CJY', '2005-01-01', 2, 0, 1, '2020-02-10', 'Fifa20Cs10', 24, 0, 4, 39),
(329, 327, 'Menor', 'ch.i.c.lest.oreper@gmail.com', 'XHC2tp5K', '2005-01-01', 2, 0, 1, '2020-02-10', 'Fifa20Cs11', 24, 0, 4, 39),
(330, 327, 'Menor', 'c.h.i.c.lest.oreper@gmail.com', '4uMN7uxm', '2005-01-01', 2, 0, 1, '2020-02-11', 'Fifa20Cs12', 24, 0, 4, 39),
(331, 327, 'Menor', 'chicl.est.oreper@gmail.com', 'NRRMBq23', '2005-01-01', 2, 0, 1, '2020-02-10', 'Fifa20Cs13', 24, 0, 4, 39),
(332, 327, 'Menor', 'c.hicl.est.oreper@gmail.com', 'Pb5xr4uv', '2005-01-01', 2, 0, 1, '2020-02-15', 'Fifa20Cs14', 24, 59, 4, 39),
(333, 327, 'Menor', 'ch.icl.est.oreper@gmail.com', 'cbK6bzCL', '2005-01-01', 2, 0, 1, '2020-02-10', 'Fifa20Cs15', 24, 0, 4, 39),
(334, NULL, 'Adulto', 'c.h.icl.est.oreper@gmail.com', 'vzhJTg9B', '2005-01-01', 2, 0, 1, '2020-02-12', 'ResidentEvil2Cs2', 20, 0, 4, 51),
(335, 334, 'Menor', 'chi.cl.est.oreper@gmail.com', 'hUXE5yNR', '2005-01-01', 2, 0, 1, '2020-02-15', 'DragonBallFZCS15', 15, 34, 4, 38),
(336, 334, 'Menor', 'c.hi.cl.est.oreper@gmail.com', 'Ky4hkQaP', '2005-01-01', 2, 0, 1, '2020-02-10', 'Fifa20Cs16', 24, 0, 4, 39),
(337, 334, 'Menor', 'ch.i.cl.est.oreper@gmail.com', '7kzQVt6x', '2005-01-01', 2, 0, 1, '2020-02-10', 'Fifa20Cs17', 24, 0, 4, 39),
(338, 334, 'Menor', 'c.h.i.cl.est.oreper@gmail.com', 'q5qGVEB8', '2005-01-01', 2, 0, 1, '2020-02-11', 'Fifa20Cs18', 24, 0, 4, 39),
(339, 334, 'Menor', 'chic.l.est.oreper@gmail.com', 'guAv2CBR', '2005-01-01', 2, 0, 1, '2020-02-10', 'CrashTRCs16', 24, 0, 4, 40),
(340, 334, 'Menor', 'c.hic.l.est.oreper@gmail.com', 'Wqj7V7gD', '2005-01-01', 2, 0, 1, '2020-02-10', 'CrashTRCs17', 24, 0, 4, 40),
(341, NULL, 'Adulto', 'ch.ic.l.est.oreper@gmail.com', 'cwP3z8gP', '2005-01-01', 2, 0, 1, '2020-02-11', 'CrashBandiCs1', 20, 0, 4, 110),
(342, NULL, 'Adulto', 'c.h.ic.l.est.oreper@gmail.com', '5P2rdBbE', '1970-01-01', 2, 0, 1, '2020-02-10', 'GtaVCs38', 15, 0, 4, 46),
(343, 342, 'Menor', 'c.hi.c.l.est.oreper@gmail.com', 'bZwNpk7X', '2005-01-01', 2, 0, 1, '2020-02-10', 'GtaVCs39', 15, 0, 4, 46),
(344, 342, 'Menor', 'chi.c.l.est.oreper@gmail.com', 'n6ee29SV', '2005-01-01', 2, 0, 1, '2020-02-13', 'F1Cs1', 24, 59, 4, 111),
(345, 342, 'Menor', 'ch.i.c.l.est.oreper@gmail.com', 'FmgatS86', '2005-01-01', 2, 0, 1, '2020-02-12', 'GtaVCs40', 15, 0, 4, 46),
(346, 342, 'Menor', 'c.h.i.c.l.est.oreper@gmail.com', '8Brx7RWP', '2005-01-01', 2, 0, 1, '2020-02-10', 'GtaVCs41', 15, 0, 4, 46),
(347, 342, 'Menor', 'chicle.st.oreper@gmail.com', 'uYB8MeGm', '2005-01-01', 2, 0, 1, '2020-02-10', 'GtaVCs42', 15, 0, 4, 46),
(350, NULL, 'Adulto', 'c.hicle.st.oreper@gmail.com', 'weLjY5dp', '1970-01-01', 1, 0, 0, '', 'MortalKoXCs1', 20, 49, 3, 112),
(351, 350, 'Menor', 'ch.icle.st.oreper@gmail.com', 'nBwJ5RKR', '2005-01-01', 1, 0, 1, '2020-02-11', 'HorizonCs2', 20, 49, 1, 64),
(352, 350, 'Menor', 'c.h.icle.st.oreper@gmail.com', 'Ev4LHV5L', '2005-01-01', 2, 0, 1, '2020-02-11', 'CrashTRCs18', 24, 0, 4, 40),
(353, 350, 'Menor', 'chi.cle.st.oreper@gmail.com', 'SnAAW8d5', '2005-01-01', 2, 0, 1, '2020-02-11', 'CrashTRCs19', 24, 0, 4, 40),
(354, 350, 'Menor', 'c.hi.cle.st.oreper@gmail.com', '96kPgNB7', '2005-01-01', 2, 0, 1, '2020-02-12', 'Fifa20Cs19', 24, 0, 4, 39),
(355, 350, 'Menor', 'ch.i.cle.st.oreper@gmail.com', 'ucLHX5Zt', '2005-01-01', 2, 0, 1, '2020-02-11', 'Fifa20Cs20', 24, 0, 4, 39),
(356, 350, 'Menor', 'c.h.i.cle.st.oreper@gmail.com', 'qzM6k2F9', '2005-01-01', 2, 0, 1, '2020-02-15', 'Fifa20Cs21', 24, 59, 4, 39),
(358, NULL, 'Adulto', 'chic.le.st.oreper@gmail.com', '4AM3qajE', '2005-01-01', 0, 0, 0, '', 'Master', 0, 0, 1, 36),
(359, 358, 'Menor', 'c.hic.le.st.oreper@gmail.com', 'ea7fB3VY', '2005-01-01', 2, 0, 1, '2020-02-15', 'Fifa20Cs22', 24, 66, 4, 39),
(360, 358, 'Menor', 'ch.ic.le.st.oreper@gmail.com', '5PpBm86v', '2005-01-01', 2, 0, 1, '2020-02-14', 'Fifa20Cs23', 24, 0, 4, 39),
(361, 358, 'Menor', 'c.h.ic.le.st.oreper@gmail.com', 'UFBn2JvK', '2005-01-01', 2, 0, 1, '2020-02-12', 'Fifa20Cs24', 24, 0, 4, 39),
(362, 358, 'Menor', 'chi.c.le.st.oreper@gmail.com', 'AP9V8zRU', '2005-01-01', 2, 0, 1, '2020-02-12', 'Fifa20Cs25', 24, 0, 4, 39),
(363, 358, 'Menor', 'c.hi.c.le.st.oreper@gmail.com', 'CACteN96', '2005-01-01', 2, 0, 1, '2020-02-15', 'Fifa20Cs26', 24, 59, 4, 39),
(364, 358, 'Menor', 'ch.i.c.le.st.oreper@gmail.com', 'ELNP6uFD', '2005-01-01', 2, 0, 1, '2020-02-14', 'Fifa20Cs27', 24, 0, 4, 39),
(365, NULL, 'Adulto', 'c.h.i.c.le.st.oreper@gmail.com', 'pcRG4N9R', '2005-01-01', 1, 0, 1, '2020-02-14', 'TheLastOfCs3', 20, 49, 1, 73),
(366, 365, 'Menor', 'chi.cl.e.st.oreper@gmail.com', 'FU8a8qmk', '2005-01-01', 2, 0, 1, '2020-02-15', 'DragonBallFZCS16', 15, 34, 4, 38),
(367, 365, 'Menor', 'c.h.icl.e.st.oreper@gmail.com', 'MZs8mFau', '2005-01-01', 1, 0, 1, '2020-02-12', 'InfamSecndCs1', 20, 49, 1, 113),
(368, 365, 'Menor', 'ch.icl.e.st.oreper@gmail.com', 'kpTh3AZZ', '2005-01-01', 2, 0, 1, '2020-02-17', 'SpiderGameOfCs1', 20, 39, 4, 80),
(369, 365, 'Menor', 'c.hicl.e.st.oreper@gmail.com', 'uXcu84tL', '2005-01-01', 2, 0, 1, '2020-02-12', 'MarvelCapcoInCs1', 10, 0, 4, 60),
(371, NULL, 'Adulto', 'c.hi.cl.e.st.oreper@gmail.com', 'pcRG4N9R', '2005-01-01', 2, 0, 1, '2020-02-13', 'DetroidHumCs1', 15, 39, 4, 115),
(372, 371, 'Menor', 'ch.i.cl.e.st.oreper@gmail.com', 'rugHxX9G', '2005-01-01', 2, 0, 1, '2020-02-14', 'CrashTRCs20', 24, 0, 4, 40),
(374, 371, 'Menor', 'chic.l.e.st.oreper@gmail.com', 'kpTh3AZZ', '2005-01-01', 2, 0, 1, '2020-02-14', 'CrashTRCs22', 24, 0, 4, 40),
(375, 371, 'Menor', 'c.hic.l.e.st.oreper@gmail.com', 'MZs8mFau', '2005-01-01', 2, 0, 1, '2020-02-19', 'CrashTRCs23', 24, 30, 4, 40),
(376, 371, 'Menor', 'ch.ic.l.e.st.oreper@gmail.com', 'FU8a8qmk', '2005-01-01', 2, 0, 1, '2020-02-25', 'CrashTRCs24', 24, 79, 4, 40),
(377, NULL, 'Adulto', 'c.h.ic.l.e.st.oreper@gmail.com', 'pcRG4N9R', '2005-01-01', 1, 0, 1, '2020-02-18', 'Farcry5Cs1', 15, 40, 1, 116),
(378, 377, 'Menor', 'c.hi.c.l.e.st.oreper@gmail.com', 'uXcu84tL', '2005-01-01', 2, 0, 1, '2020-02-14', 'NeedSpeDdexCs1', 10, 49, 4, 114),
(379, 377, 'Menor', 'chi.c.l.e.st.oreper@gmail.com', 'rugHxX9G', '2005-01-01', 2, 0, 1, '2020-02-14', 'SBomberman3', 15, 49, 4, 59),
(380, 371, 'Menor', 'c.h.i.cl.e.st.oreper@gmail.com', '2P72quZZ', '2005-01-01', 2, 0, 1, '2020-02-15', 'CrashTRCs21', 24, 59, 4, 40),
(385, 365, 'Menor', 'chicl.e.st.oreper@gmail.com', 'rugHxX9G', '2005-01-01', 2, 0, 1, '2020-02-13', '', 24, 0, 4, 39),
(386, 258, 'Menor', 'gckh@moimoi.re', 'N95GHwzv', '2005-01-01', 2, 0, 1, '2020-02-13', '', 15, 0, 4, 100),
(387, NULL, 'Adulto', 'ch.i.c.l.e.st.oreper@gmail.com', 'vD5r7rRz', '1970-01-01', 2, 0, 1, '2020-02-15', 'DeMaCrCs2', 20, 49, 4, 57),
(388, 387, 'Menor', 'c.h.i.c.l.e.st.oreper@gmail.com', '899TCSc6', '2005-01-01', 2, 0, 1, '2020-02-19', 'MarvelCapcoInCs2', 10, 25, 4, 60),
(389, 387, 'Menor', 'chicles.t.oreper@gmail.com', 'z69HSyMw', '2005-01-01', 2, 0, 1, '2020-02-15', 'SpiderGameOfCs2', 20, 49, 4, 80),
(390, 387, 'Menor', 'c.hicles.t.oreper@gmail.com', '9bJA76MV', '2005-01-01', 2, 0, 1, '2020-02-17', 'SpiderGameOfCs3', 20, 49, 4, 80),
(391, 387, 'Menor', 'ch.icles.t.oreper@gmail.com', 'w4J6JdbU', '2005-01-01', 2, 0, 1, '2020-02-15', 'SpiderGameOfCs4', 20, 49, 4, 80),
(393, 387, 'Menor', 'chi.cles.t.oreper@gmail.com', '52VsvNUY', '2005-01-01', 2, 0, 1, '2020-02-15', 'GtaVCs43', 15, 34, 4, 46),
(394, NULL, 'Adulto', 'c.hi.cles.t.oreper@gmail.com', 'TnLHRb8Z', '1970-01-01', 2, 0, 1, '2020-02-19', 'BattleFieldCS2', 7, 19, 4, 78),
(395, 394, 'Menor', 'ch.i.cles.t.oreper@gmail.com', '8gzu7E8c', '2005-01-01', 2, 0, 1, '2020-02-19', 'CoDMoWarCS2', 60, 119, 4, 52),
(396, 394, 'Menor', 'c.h.i.cles.t.oreper@gmail.com', 'aK7nM9ph', '2005-01-01', 2, 0, 1, '2020-02-15', 'CrashTRCs25', 24, 59, 4, 40),
(397, 394, 'Menor', 'chic.les.t.oreper@gmail.com', '6zmEWVQ8', '2005-01-01', 2, 0, 1, '2020-02-18', 'Fifa20Cs29', 24, 30, 4, 39),
(398, 394, 'Menor', 'c.hic.les.t.oreper@gmail.com', 'DHn25s2Q', '2005-01-01', 2, 0, 1, '2020-02-18', 'Nba20CS2', 20, 49, 4, 53),
(399, 394, 'Menor', 'ch.ic.les.t.oreper@gmail.com', 'kYNHe64d', '2005-01-01', 2, 0, 1, '2020-02-25', 'GtaVCs44', 15, 34, 4, 46),
(400, NULL, 'Adulto', 'c.h.ic.les.t.oreper@gmail.com', 'X5hERB8w', '1970-01-01', 1, 0, 1, '2020-02-15', 'AssasiEziCs1', 16, 39, 1, 118),
(401, 400, 'Menor', 'chi.c.les.t.oreper@gmail.com', 'mM7nvrZ4', '2005-01-01', 2, 0, 1, '2020-02-17', 'DevMcryDelCs1', 24, 60, 4, 39),
(402, 400, 'Menor', 'c.hi.c.les.t.oreper@gmail.com', 'f5C6yCWn', '2005-01-01', 1, 0, 1, '2020-02-17', 'RedDeRedCs1', 36, 89, 1, 119),
(403, 400, 'Menor', 'ch.i.c.les.t.oreper@gmail.com', '44w9tnDe', '2005-01-01', 2, 0, 1, '2020-02-18', 'CrashTRCs26', 24, 30, 4, 40),
(404, 400, 'Menor', 'c.h.i.c.les.t.oreper@gmail.com', 'Tga9vB6j', '2005-01-01', 2, 0, 1, '2020-02-15', 'GtaVCs45', 15, 17, 4, 46),
(405, 400, 'Menor', 'chicl.es.t.oreper@gmail.com', 'KscQ6BmF', '2005-01-01', 2, 0, 1, '2020-02-15', 'GtaVCs46', 15, 34, 4, 46),
(406, 400, 'Menor', 'c.hicl.es.t.oreper@gmail.com', 'M5Tenb5q', '2005-01-01', 2, 0, 1, '2020-02-21', 'GtaVCs47', 15, 34, 4, 46),
(407, 387, 'Menor', 'c.h.icles.t.oreper@gmail.com', 's7a4bUZ3', '2005-01-01', 2, 0, 1, '2020-02-15', 'CrashBandiCs2', 20, 49, 4, 110),
(408, 288, 'Menor', 'c.h.ic.le.s.toreper@gmail.com', 'EH5P4eTG', '2005-01-01', 1, 0, 0, '', 'GtaVCs34', 15, 0, 2, 100),
(409, NULL, 'Adulto', 'ch.icl.es.t.oreper@gmail.com', 'dTM5c9mg', '1970-01-01', 1, 0, 1, '2020-02-19', 'DeMcryDelCs1', 24, 55, 1, 120),
(410, 409, 'Menor', 'c.h.icl.es.t.oreper@gmail.com', 'WvzrZ7SC', '2005-01-01', 2, 0, 1, '2020-02-18', 'DragonBallFZCS17', 15, 34, 4, 38),
(411, 409, 'Menor', 'chi.cl.es.t.oreper@gmail.com', 'yuD7cQ3g', '2005-01-01', 2, 0, 1, '2020-02-17', 'ResidentEvil2Cs3', 20, 45, 4, 51),
(412, 409, 'Menor', 'c.hi.cl.es.t.oreper@gmail.com', 'PFPH2WpY', '2005-01-01', 1, 0, 1, '2020-02-21', 'DragonBallFZCS18', 24, 59, 1, 122),
(413, 409, 'Menor', 'ch.i.cl.es.t.oreper@gmail.com', 'sn5DjJ6t', '2005-01-01', 2, 0, 1, '2020-02-17', 'ResidentEvil2Cs4', 24, 59, 4, 65),
(414, 409, 'Menor', 'chic.l.es.t.oreper@gmail.com', 'dxY6cYhZ', '2005-01-01', 1, 0, 1, '2020-02-17', 'StWarJeCs1', 60, 129, 1, 124),
(415, NULL, 'Adulto', 'c.hic.l.es.t.oreper@gmail.com', 'Kak2fMWh', '1970-01-01', 2, 0, 1, '2020-02-17', 'GodWarIIICs2', 20, 49, 4, 81),
(416, NULL, 'Adulto', 'ch.ic.l.es.t.oreper@gmail.com', 'ZKcn3R9m', '2005-01-01', 1, 0, 1, '2020-02-19', 'DaGonDigCs1', 25, 59, 1, 123),
(418, 423, 'Menor', 'c.h.ic.l.es.t.oreper@gmail.com', 'E3Y9rHR6', '2005-01-01', 2, 0, 1, '2020-02-18', 'ResidentEvil2Cs5', 20, 19, 4, 51),
(419, 423, 'Menor', 'c.hi.c.l.es.t.oreper@gmail.com', 'NAf8yubx', '2005-01-01', 2, 0, 1, '2020-02-17', 'Fifa20Cs31', 24, 30, 4, 39),
(420, 423, 'Menor', 'ch.i.c.l.es.t.oreper@gmail.com', '32fArGJq', '2005-01-01', 2, 0, 1, '2020-02-19', 'Injustic2Cs1', 12, 29, 4, 91),
(421, 423, 'Menor', 'c.h.i.c.l.es.t.oreper@gmail.com', 'uKT9fKPW', '2005-01-01', 2, 0, 1, '2020-02-17', 'ResidentEvil2Cs6', 24, 68, 4, 65),
(422, 423, 'Menor', 'chicle.s.t.oreper@gmail.com', 'jSYXRY9h', '2005-01-01', 2, 0, 1, '2020-02-25', 'CrashTRCs27', 24, 59, 4, 40),
(423, NULL, 'Adulto', 'chi.c.l.es.t.oreper@gmail.com', 'ZKcn3R9m', '1970-01-01', 0, 0, 0, '', 'BattleFieldCS3', 7, 0, 1, 78),
(424, NULL, 'Adulto', 'c.hicle.s.t.oreper@gmail.com', '6hFswYSS', '1970-01-01', 2, 0, 1, '2020-02-18', 'BattleRevo1Cs1', 8, 19, 4, 125),
(425, 424, 'Menor', 'ch.icle.s.t.oreper@gmail.com', 'g3nRnjM4', '2005-01-01', 2, 0, 1, '2020-02-21', 'CrashBandiCs3', 20, 49, 4, 110),
(426, 424, 'Menor', 'c.h.icle.s.t.oreper@gmail.com', '56KwK5gz', '2005-01-01', 2, 0, 1, '2020-02-18', 'GtaVCs48', 15, 34, 4, 46),
(427, 424, 'Menor', 'chi.cle.s.t.oreper@gmail.com', '3LRtuYyt', '2005-01-01', 2, 0, 1, '2020-02-25', 'GtaVCs49', 15, 34, 4, 46),
(428, 424, 'Menor', 'c.hi.cle.s.t.oreper@gmail.com', 'ca5zQpV9', '2005-01-01', 2, 1, 1, '2020-02-25', 'CrashTRCs28', 24, 59, 1, 40),
(429, 424, 'Menor', 'ch.i.cle.s.t.oreper@gmail.com', 'cMv26TPU', '2005-01-01', 2, 0, 1, '2020-02-21', 'CrashTRCs29', 24, 59, 4, 40),
(430, 424, 'Menor', 'c.h.i.cle.s.t.oreper@gmail.com', '7AFbsC3e', '2005-01-01', 2, 0, 1, '2020-02-18', 'Fifa20Cs32', 24, 59, 4, 39),
(431, NULL, 'Adulto', 'chic.le.s.t.oreper@gmail.com', 'yLTk8AdU', '1970-01-01', 2, 0, 1, '2020-02-18', 'ResindEv4Cs1', 8, 19, 4, 127),
(432, 431, 'Menor', 'c.hic.le.s.t.oreper@gmail.com', 'Tkx74Pej', '2005-01-01', 2, 0, 1, '2020-02-20', 'SekirShadCs1', 39, 89, 4, 128),
(433, 431, 'Menor', 'ch.ic.le.s.t.oreper@gmail.com', '9rc4VPTs', '2005-01-01', 2, 0, 1, '2020-02-20', 'CrashBandiCs4', 20, 49, 4, 110),
(434, 431, 'Menor', 'c.h.ic.le.s.t.oreper@gmail.com', 'Q3BW2yuA', '2005-01-01', 2, 0, 1, '2020-02-18', 'CrashTRCs30', 24, 59, 4, 40),
(435, 431, 'Menor', 'chi.c.le.s.t.oreper@gmail.com', 'vgsSKT8L', '2005-01-01', 2, 0, 1, '2020-02-21', 'Fifa20Cs33', 24, 59, 4, 39),
(436, 431, 'Menor', 'c.hi.c.le.s.t.oreper@gmail.com', 'urt7bUF5', '2005-01-01', 2, 0, 1, '2020-02-26', 'Fifa20Cs34', 24, 59, 4, 39),
(437, 431, 'Menor', 'ch.i.c.le.s.t.oreper@gmail.com', 'PMnr4WKQ', '2005-01-01', 1, 0, 0, '', 'Fifa20Cs35', 24, 59, 2, 39),
(438, NULL, 'Adulto', 'chicl.e.s.t.oreper@gmail.com', 'VZd8hQsM', '19970-01-01', 0, 0, 0, '', 'DeatStraCs1', 36, 89, 1, 129),
(451, 438, 'Menor', 'c.hicl.e.s.t.oreper@gmail.com', 'hTwA5XnM', '2005-01-01', 2, 0, 1, '2020-02-21', 'Fifa20Cs36', 24, 59, 4, 39),
(452, 438, 'Menor', 'ch.icl.e.s.t.oreper@gmail.com', 't8VcXaB4', '2005-01-01', 2, 0, 1, '2020-02-25', 'Fifa20Cs37', 24, 59, 4, 39),
(453, 438, 'Menor', 'c.h.icl.e.s.t.oreper@gmail.com', '5vcHnbVQ', '2005-01-01', 2, 0, 1, '2020-02-24', 'Fifa20Cs38', 24, 59, 4, 39),
(454, 438, 'Menor', 'chi.cl.e.s.t.oreper@gmail.com', 'NMsC8Ny9', '2005-01-01', 2, 0, 1, '2020-02-21', 'Fifa20Cs39', 24, 59, 4, 39),
(455, 438, 'Menor', 'c.hi.cl.e.s.t.oreper@gmail.com', 'uXCh6ewm', '2005-01-01', 2, 0, 1, '2020-02-21', 'Fifa20Cs40', 24, 60, 4, 39),
(456, 438, 'Menor', 'ch.i.cl.e.s.t.oreper@gmail.com', 'csTz54ph', '2005-01-01', 1, 0, 0, '', 'Fifa20Cs41', 24, 30, 2, 39),
(457, NULL, 'Adulto', 'chic.l.e.s.t.oreper@gmail.com', 'Hv49JhYZ', '1970-01-01', 2, 0, 1, '2020-02-25', 'BattleRevo1Cs2', 8, 19, 4, 125),
(458, 457, 'Menor', 'c.h.i.cl.e.s.t.oreper@gmail.com', '8DGRCqCS', '2005-01-01', 1, 0, 1, '2020-02-25', 'CrashTRCs31', 24, 59, 1, 40),
(459, 457, 'Menor', 'c.hic.l.e.s.t.oreper@gmail.com', 'LcK6b2RS', '2005-01-01', 2, 0, 1, '2020-02-27', 'CrashTRCs32', 24, 59, 4, 40),
(460, 457, 'Menor', 'ch.ic.l.e.s.t.oreper@gmail.com', 'DhqRNj8A', '2005-01-01', 2, 0, 1, '2020-02-21', 'CrashTRCs33', 24, 59, 4, 40),
(461, 457, 'Menor', 'c.h.ic.l.e.s.t.oreper@gmail.com', '8YDJnS6r', '2005-01-01', 2, 0, 1, '2020-02-25', 'Fifa20Cs42', 24, 59, 4, 39),
(462, 457, 'Menor', 'chi.c.l.e.s.t.oreper@gmail.com', '9MhgccJG', '2005-01-01', 2, 0, 1, '2020-02-21', 'DragonBallFZCS19', 15, 34, 4, 38),
(463, 457, 'Menor', 'c.hi.c.l.e.s.t.oreper@gmail.com', 'cV7jV9Dn', '2005-01-01', 2, 0, 1, '2020-02-25', 'DragonBallFZCS20', 15, 34, 4, 38),
(464, NULL, 'Adulto', 'ch.i.c.l.e.s.t.oreper@gmail.com', 'uSRkh5mF', '1970-01-01', 2, 0, 1, '2020-02-24', 'MortaKombCs2', 20, 49, 4, 41),
(466, 464, 'Menor', 'chiclesto.reper@gmail.com', '8eSeMT5Q', '2005-01-01', 2, 0, 1, '2020-02-21', 'GtaVCs50', 15, 39, 4, 46),
(467, 464, 'Menor', 'c.h.i.c.l.e.s.t.oreper@gmail.com', 'aq6FXMUK', '2005-01-01', 2, 0, 1, '2020-02-24', 'GtaVCs49', 15, 34, 4, 46),
(468, 464, 'Menor', 'c.hiclesto.reper@gmail.com', 'V4bP6KFw', '2005-01-01', 2, 0, 1, '2020-02-21', 'GtaVCs51', 15, 34, 4, 46),
(469, 464, 'Menor', 'ch.iclesto.reper@gmail.com', '7acw9kMy', '2005-01-01', 2, 0, 1, '2020-02-25', 'GtaVCs52', 15, 34, 4, 46),
(470, 464, 'Menor', 'c.h.iclesto.reper@gmail.com', 'Y5zv5aRz', '2005-01-01', 1, 0, 1, '2020-02-25', 'GtaVCs53', 15, 34, 1, 46),
(471, 464, 'Menor', 'chi.clesto.reper@gmail.com', 'z3bjGkMF', '2005-01-01', 1, 0, 1, '2020-02-21', 'GtaVCs54', 15, 40, 1, 46),
(472, NULL, 'Adulto', 'c.hi.clesto.reper@gmail.com', '7qzfDvhv', '1970-01-01', 2, 0, 1, '2020-02-25', 'GodofWarCS20', 20, 50, 4, 43),
(473, 472, 'Menor', 'ch.i.clesto.reper@gmail.com', 'Bj9nbrKx', '2005-01-01', 2, 0, 1, '2020-02-21', 'GtaVCs55', 15, 34, 4, 46),
(474, 472, 'Menor', 'c.h.i.clesto.reper@gmail.com', 'DmsNc58M', '2005-01-01', 2, 0, 1, '2020-02-27', 'ResidentEvil2Cs7', 20, 23, 4, 51),
(475, 472, 'Menor', 'chic.lesto.reper@gmail.com', 'a7WpAdxN', '2005-01-01', 2, 0, 1, '2020-02-21', 'SpiderGameOfCs5', 20, 58, 4, 80),
(476, 472, 'Menor', 'c.hic.lesto.reper@gmail.com', 'j9k3aCNH', '2005-01-01', 2, 0, 1, '2020-02-21', 'SpiderGameOfCs6', 20, 79, 4, 80),
(477, 472, 'Menor', 'ch.ic.lesto.reper@gmail.com', 'L3KEDQwT', '2005-01-01', 2, 0, 1, '2020-02-27', 'SpiderGameOfCs7', 20, 49, 4, 80),
(478, 472, 'Menor', 'c.h.ic.lesto.reper@gmail.com', 'q3RkQs5m', '2005-01-01', 1, 0, 1, '2020-02-25', 'NeedSpeDdexCs2', 10, 25, 1, 114),
(479, NULL, 'Adulto', 'chi.c.lesto.reper@gmail.com', 'EQZZ78gh', '1970-01-01', 2, 0, 1, '2020-02-27', 'GodofWarCS21', 20, 49, 4, 43),
(480, NULL, 'Adulto', 'c.hi.c.lesto.reper@gmail.com', 'SKUc4SZS', '2005-01-01', 2, 0, 1, '2020-02-25', 'ResidEv5Cs1', 8, 19, 4, 130),
(481, NULL, 'Adulto', 'dwgl9wtb@yopmail.com', 'PgY9C6Kz', '2002-05-10', 2, 0, 1, '2019-11-07', 'NarutLegacyCs1', 21, 0, 4, 134),
(483, 479, 'Menor', 'ch.i.c.lesto.reper@gmail.com', '9GEUuJzL', '2005-01-01', 2, 0, 1, '2020-02-27', 'GtaVCs56', 15, 34, 4, 100),
(487, 479, 'Menor', 'ch.icl.esto.reper@gmail.com', 'pzww2Tqf', '2005-01-01', 0, 0, 0, '', 'CrashBandiCs5', 40, 0, 1, 110),
(488, 479, 'Menor', 'c.h.i.c.lesto.reper@gmail.com', 'rVb2Uh8H', '2005-01-01', 2, 1, 1, '2020-02-27', 'GtaVCs57', 15, 34, 1, 100),
(489, 479, 'Menor', 'chicl.esto.reper@gmail.com', 'z3tnHSk8', '2005-01-01', 0, 0, 0, '', 'GtaVCs58', 15, 34, 1, 100),
(491, 479, 'Menor', 'c.hicl.esto.reper@gmail.com', '9Ad5fDU8', '2005-01-01', 1, 0, 1, '2020-02-28', 'GtaVCs59', 15, 34, 1, 100),
(492, 480, 'Menor', 'c.h.icl.esto.reper@gmail.com', 'NwyRa2Ha', '2005-01-01', 2, 0, 1, '2020-02-28', 'Pes20Cs21', 24, 59, 4, 45),
(493, 480, 'Menor', 'chi.cl.esto.reper@gmail.com', 'vrwC5gk3', '2005-01-01', 2, 0, 1, '2020-02-25', 'Pes20Cs22', 24, 59, 4, 45),
(494, 480, 'Menor', 'c.hi.cl.esto.reper@gmail.com', 'nJeT2Puz', '2005-01-01', 1, 0, 0, '', 'Pes20Cs23', 24, 30, 2, 45),
(495, 480, 'Menor', 'ch.i.cl.esto.reper@gmail.com', 'LeK58KQJ', '2005-01-01', 2, 0, 1, '2020-02-28', 'Pes20Cs24', 24, 59, 4, 45),
(496, 480, 'Menor', 'c.h.i.cl.esto.reper@gmail.com', '4DcWfPpv', '2005-01-01', 2, 0, 1, '2020-02-27', 'MonoFamCs1', 6, 15, 4, 133),
(497, NULL, 'Adulto', 'chic.l.esto.reper@gmail.com', 'HQZ78dqt', '1970-01-01', 1, 0, 1, '2020-02-27', 'CoDMoWarCS3', 60, 129, 1, 52),
(498, 497, 'Menor', 'c.hic.l.esto.reper@gmail.com', 'SY8xZLfe', '2005-01-01', 1, 0, 1, '2020-02-27', 'WwkCs2', 30, 69, 1, 88),
(499, 497, 'Menor', 'ch.ic.l.esto.reper@gmail.com', 'JN4bMLd4', '2005-01-01', 1, 0, 1, '2020-02-27', 'WwkCs3', 30, 69, 1, 88),
(500, 497, 'Menor', 'c.h.ic.l.esto.reper@gmail.com', 'kP2tfPLq', '2005-01-01', 2, 0, 1, '2020-02-28', 'TekkenCs6', 15, 34, 4, 76),
(501, 497, 'Menor', 'chi.c.l.esto.reper@gmail.com', 'Metr8yDA', '2005-01-01', 1, 0, 0, '', 'EASPORTCs19', 10, 25, 2, 47),
(502, 497, 'Menor', 'c.hi.c.l.esto.reper@gmail.com', 'SJCW7ggF', '2005-01-01', 1, 0, 1, '2020-02-26', 'ArkSurEvCs1', 10, 25, 1, 132),
(503, 497, 'Menor', 'ch.i.c.l.esto.reper@gmail.com', 'hhE3MgGB', '2005-01-01', 1, 0, 1, '2020-02-27', 'GtaVCs60', 15, 17, 4, 100),
(504, NULL, 'Adulto', 'c.h.i.c.l.esto.reper@gmail.com', 'k9pWtaJN', '1970-01-01', 1, 0, 1, '2020-02-25', 'FarCrPriCs1', 18, 43, 1, 136),
(505, 504, 'Menor', 'chicle.sto.reper@gmail.com', 'BqLjH2Ax', '2005-01-01', 1, 0, 1, '2020-02-27', 'FinlTheZoCs1', 25, 61, 1, 138),
(506, 504, 'Menor', 'c.hicle.sto.reper@gmail.com', '7Bq7hfVs', '2005-01-01', 2, 0, 1, '2020-02-27', 'StreetFighterCs6', 8, 19, 4, 37),
(508, 504, 'Menor', 'c.h.icle.sto.reper@gmail.com', 'e65kSc4h', '2005-01-01', 1, 0, 0, '', 'CrashBunCs1', 41, 89, 2, 137),
(509, 504, 'Menor', 'ch.icle.sto.reper@gmail.com', 'hFTs2umA', '2005-01-01', 1, 0, 0, '', 'StreetFighterCs7', 8, 19, 2, 37),
(511, NULL, 'Adulto', 'chi.cle.sto.reper@gmail.com', 'D56Hjv5L', '1970-01-01', 1, 0, 0, '', 'CallBlac4Cs1', 20, 25, 2, 67),
(512, 511, 'Menor', 'ch.ic.le.sto.reper@gmail.com', '3sWX5dCp', '2005-01-01', 2, 0, 1, '2020-02-27', 'EASPORTCs20', 10, 25, 4, 47),
(513, NULL, 'Adulto', 'c.hi.cle.sto.reper@gmail.com', 'vbre5FkH', '1970-01-01', 1, 0, 0, '', 'AssCreedOCS1', 15, 61, 2, 84),
(514, 513, 'Menor', 'ch.i.cle.sto.reper@gmail.com', 'BM9hSky3', '2005-01-01', 2, 0, 1, '2020-02-27', 'PLvsZOBFCs2', 20, 49, 4, 56),
(515, 513, 'Menor', 'c.h.i.cle.sto.reper@gmail.com', '94UsnwSP', '2005-01-01', 2, 0, 1, '2020-02-28', 'CrashBunCs2', 41, 89, 4, 137),
(517, 513, 'Menor', 'chic.le.sto.reper@gmail.com', 'ux5AuMTG', '2005-01-01', 2, 0, 1, '2020-02-27', 'NarutLegacCs1', 21, 51, 4, 134),
(518, 513, 'Menor', 'c.hic.le.sto.reper@gmail.com', 'Bs7k8s39', '2005-01-01', 2, 0, 1, '2020-02-28', 'Pes20Cs25', 24, 59, 4, 45),
(519, 513, 'Menor', 'c.h.ic.le.sto.reper@gmail.com', 'FkfvL6Xt', '2005-01-01', 2, 0, 1, '2020-02-27', 'Pes20Cs26', 24, 59, 4, 45),
(520, 513, 'Menor', 'chi.c.le.sto.reper@gmail.com', 'hNL5n6An', '2005-01-01', 1, 0, 0, '', 'Pes20Cs27', 24, 59, 2, 45),
(521, NULL, 'Adulto', 'c.hi.c.le.sto.reper@gmail.com', '75PzrRGq', '1970-01-01', 1, 0, 0, '', 'DarSouCs1', 15, 37, 2, 141),
(522, 521, 'Menor', 'ch.i.c.le.sto.reper@gmail.com', 'jE8k6aKU', '2005-01-01', 1, 0, 0, '', 'StreetFighterCs8', 8, 19, 2, 37),
(523, 521, 'Menor', 'c.h.i.c.le.sto.reper@gmail.com', 'G6Rkk2eM', '2005-01-01', 1, 0, 0, '', 'TekkenCs7', 15, 34, 2, 76),
(524, 521, 'Menor', 'chicl.e.sto.reper@gmail.com', 'jB3PN3ES', '2005-01-01', 1, 0, 0, '', 'RadCs1', 10, 25, 2, 139),
(525, 521, 'Menor', 'c.hicl.e.sto.reper@gmail.com', 'Gw3ZZSf5', '2005-01-01', 1, 0, 0, '', 'SAOFatBuCs1', 12, 29, 2, 144),
(526, 521, 'Menor', 'ch.icl.e.sto.reper@gmail.com', '2tDKZ9kR', '2005-01-01', 1, 0, 0, '', 'SAOHollReaCs1', 15, 37, 2, 145);
INSERT INTO `cuenta` (`idCuenta`, `cuenta_idPadre`, `rango`, `email`, `pswd`, `Fnacimiento`, `ventaPrincipal`, `ventaSecundario`, `nReseteos`, `ultimoReseteo`, `nPSN`, `precio`, `precioVendido`, `estadoCuenta`, `juego_idJuego`) VALUES
(527, NULL, 'Adulto', 'c.h.icl.e.sto.reper@gmail.com', 'Xs4D8Jpy', '1970-01-01', 1, 0, 0, '', 'ResEv7Cs1', 15, 37, 2, 99),
(528, 527, 'Menor', 'chi.cl.e.sto.reper@gmail.com', '6ae3cNRQ', '2005-01-01', 1, 0, 0, '', 'TekkenCs8', 15, 34, 2, 76),
(529, 527, 'Menor', 'c.hi.cl.e.sto.reper@gmail.com', 'R56MfyAe', '2005-01-01', 1, 0, 0, '', '', 0, 59, 2, 45),
(530, 527, 'Menor', 'ch.i.cl.e.sto.reper@gmail.com', 'uJyqXE4B', '2005-01-01', 1, 0, 0, '', 'Pes20Cs29', 24, 59, 2, 45),
(531, 527, 'Menor', 'c.h.i.cl.e.sto.reper@gmail.com', 'D4rZqaqm', '2005-01-01', 2, 0, 1, '2020-02-28', 'Pes20Cs30', 24, 59, 4, 45),
(532, 527, 'Menor', 'chic.l.e.sto.reper@gmail.com', '4ASceABr', '2005-01-01', 1, 0, 0, '', 'SAOHollCs1', 5, 12, 2, 143),
(533, 527, 'Menor', 'c.hic.l.e.sto.reper@gmail.com', 'ZKCqYh5p', '2005-01-01', 1, 0, 0, '', 'KingDHear3', 18, 45, 2, 140);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocuenta`
--

CREATE TABLE `estadocuenta` (
  `idEstado` int(11) NOT NULL,
  `nombreEstado` varchar(22) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estadocuenta`
--

INSERT INTO `estadocuenta` (`idEstado`, `nombreEstado`) VALUES
(1, 'DISPONIBLE'),
(2, 'ESPERANDO DESCARGA'),
(3, 'PETICIÓN DE RESETEO'),
(4, 'NO DISPONIBLE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `finanzas`
--

CREATE TABLE `finanzas` (
  `idFinanza` int(11) NOT NULL,
  `ingresos` float NOT NULL,
  `saldoComprado` float NOT NULL,
  `gastoPersonal` float NOT NULL,
  `gastoPublicidad` float NOT NULL,
  `gastoExtra` float NOT NULL,
  `total` float NOT NULL,
  `diaAnterior` int(11) DEFAULT NULL,
  `hastaHoy` float NOT NULL,
  `fechFinan` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `finanzas`
--

INSERT INTO `finanzas` (`idFinanza`, `ingresos`, `saldoComprado`, `gastoPersonal`, `gastoPublicidad`, `gastoExtra`, `total`, `diaAnterior`, `hastaHoy`, `fechFinan`) VALUES
(19, 631, 0, 34, 0, 0, 597, NULL, 597, '2020-01-30'),
(20, 322, 1107, 36, 25, 0, -846, 19, -249, '2020-01-31'),
(21, 474, 640, 37, 30, 475, -708, 20, -957, '2020-02-01'),
(22, 474, 451, 36, 0, 0, -13, 21, -970, '2020-02-03'),
(23, 34, 0, 35, 0, 0, -1, 22, -971, '2020-02-04'),
(24, 799, 0, 37, 30, 0, 732, 23, -239, '2020-02-05'),
(25, 514, 0, 0, 0, 0, 514, 24, 275, '2020-02-06'),
(26, 947, 490, 36, 0, 0, 421, 25, 696, '2020-02-07'),
(27, 1139, 933, 33, 38, 14, 121, 26, 817, '2020-02-08'),
(28, 1072, 544, 41, 0, 0, 487, 27, 1304, '2020-02-10'),
(29, 924, 860, 18, 121, 0, -75, 28, 1229, '2020-02-11'),
(30, 657, 598, 0, 120, 120, -181, 29, 1048, '2020-02-12'),
(31, 2201, 1412, 46, 0, 0, 743, 30, 1791, '2020-02-13'),
(32, 597, 0, 38, 0, 34, 525, 31, 2316, '2020-02-14'),
(33, 1162, 1095, 38, 87, 14, -72, 32, 2244, '2020-02-15'),
(34, 1887, 1003, 20, 85, 0, 779, 33, 3023, '2020-02-17'),
(35, 1129.5, 2052, 0, 250, 0, -1172.5, 34, 1850.5, '2020-02-18'),
(36, 253, 0, 0, 30, 0, 223, 35, 2073.5, '2020-02-19'),
(37, 104, 0, 0, 30, 0, 74, 36, 2147.5, '2020-02-20'),
(38, 187, 0, 0, 12, 0, 175, 37, 2322.5, '2020-02-21'),
(39, 943, 1240, 0, 0, 0, -297, 38, 2025.5, '2020-02-22'),
(40, 509, 335, 0, 160, 0, 14, 39, 2039.5, '2020-02-24'),
(41, 924, 691, 0, 40, 0, 193, 40, 2232.5, '2020-02-25'),
(42, 688, 0, 0, 126, 0, 562, 41, 2794.5, '2020-02-26'),
(43, 1004, 720, 0, 185, 0, 99, 42, 2893.5, '2020-02-27'),
(44, 1156, 0, 0, 0, 0, 1156, 43, 4049.5, '2020-02-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juego`
--

CREATE TABLE `juego` (
  `idJuego` int(11) NOT NULL,
  `nombre` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `juego`
--

INSERT INTO `juego` (`idJuego`, `nombre`) VALUES
(36, 'Sin Juego'),
(37, 'Street FighterÂ® V'),
(38, 'DRAGON BALL FIGHTERZ'),
(39, 'EA SPORTSâ„¢ FIFA 20'),
(40, 'Crashâ„¢ Team Racing Nitro-Fueled'),
(41, 'Mortal Kombat XL'),
(42, 'Call of DutyÂ®: WWII - Gold Edition'),
(43, 'God of War'),
(44, 'Dying Light: The Following - Enhanced Edition'),
(45, 'eFootball PES 2020 Standard Edition'),
(46, 'Grand Theft Auto V: Premium Online Edition '),
(47, 'EA SPORTSâ„¢ UFCÂ® 3'),
(48, 'JUST DANCE'),
(49, 'DRAGON BALL Z: KAKAROT'),
(51, 'Resident Evil 2'),
(52, 'Call of DutyÂ®: Modern WarfareÂ®'),
(53, 'NBA 2K20'),
(54, 'Rocket LeagueÂ®'),
(55, 'The Simsâ„¢ 4'),
(56, 'Plants vs. Zombies: Battle for Neighborvilleâ„¢ '),
(57, 'Devil May Cry 5 (with Red Orbs)'),
(58, 'Need for Speedâ„¢ Payback '),
(59, 'Super bomberman R'),
(60, 'Marvel vs. Capcom: Infinite - Standard Edition '),
(61, 'JUMP FORCE'),
(62, 'Brothers: a Tale of two Sons'),
(64, 'Horizon Zero Dawn: Complete Edition'),
(65, 'RESIDENT EVIL 2 Deluxe Edition'),
(66, 'MONSTER HUNTER: WORLDâ„¢ '),
(67, 'Call of DutyÂ®: Black Ops 4 '),
(68, 'World War Z '),
(73, 'The Last Of Usâ„¢ Remastered'),
(74, 'DRAGON BALL XENOVERSE Super Bundle'),
(75, 'Plants vs. Zombiesâ„¢ Garden Warfare'),
(76, 'TEKKEN 7'),
(77, 'NARUTO SHIPPUDENâ„¢: Ultimate NinjaÂ® STORM 4 '),
(78, 'Battlefield 4â„¢'),
(79, 'Saint Seiya Soldiers Soul'),
(80, 'Marvels Spider-Man: Game of the Year Edition'),
(81, 'God of War III Remastered'),
(82, 'Zombie Army Trilogy '),
(83, 'Assassinâ€™s CreedÂ® Origins Deluxe Edition '),
(84, 'Assassinâ€™s CreedÂ® Origins '),
(85, 'UNCHARTED: The Lost Legacy'),
(86, 'Killing Floor 2 '),
(87, 'Batman: Return to Arkham '),
(88, 'WWE 2K20'),
(89, 'A WAY OUT'),
(90, 'DRAGON BALL XENOVERSE 2'),
(91, 'Injusticeâ„¢ 2 '),
(92, 'Rise of the Tomb Raider: 20 Year Celebration'),
(94, 'Watch Dogsâ„¢'),
(95, 'EA SPORTSâ„¢ UFCÂ® 3 Deluxe Edition'),
(96, 'Cars 3: Driven to Win'),
(97, 'Tony HawksÂ® Pro Skaterâ„¢ 5'),
(98, 'TitanfallÂ® 2: Ultimate Edition'),
(99, 'RESIDENT EVIL 7 biohazard '),
(100, 'Grand Theft Auto V'),
(101, 'Injusticeâ„¢ 2 - Legendary Edition '),
(102, 'PS Plus'),
(103, 'Mafia III'),
(104, 'One Piece: Pirate Warriors 3 '),
(105, 'JUEGOPRUEBA'),
(106, 'Plants vs.Zombies â„¢ Garden Warfare 2: EdiciÃ³n estÃ¡ndar'),
(107, 'Resident Evil Revelations 1 and 2 Bundle '),
(108, 'Prueba'),
(109, 'Batmanâ„¢: Arkham Knight'),
(110, 'Crash Bandicootâ„¢ N. Sane Trilogy'),
(111, 'F1Â® 2019'),
(112, 'Mortal Kombat X'),
(113, 'inFAMOUS Second Son'),
(114, 'Need for Speedâ„¢ Deluxe Edition'),
(115, 'Detroit: Become Human'),
(116, 'FAR CRY 5'),
(117, 'SANCIONADO'),
(118, 'Assassinâ€™s CreedÂ® The Ezio Collection '),
(119, 'Red Dead Redemption 2: Special Edition'),
(120, 'Devil May Cry 5 Deluxe Edition (with Red Orbs)'),
(121, 'STAR WARS Jedi: Fallen Orderâ„¢ Deluxe Edition'),
(122, 'DRAGON BALL FIGHTERZ - FighterZ Edition'),
(123, 'Days Gone Digital Deluxe Edition'),
(124, 'STAR WARS Jedi: Fallen Orderâ„¢'),
(125, 'Battlefieldâ„¢ 1 Revolution'),
(126, 'Resident 0'),
(127, 'Resident Evil 4'),
(128, 'Sekiroâ„¢: Shadows Die Twice'),
(129, 'DEATH STRANDING'),
(130, 'Resident Evil 5'),
(131, 'Diego Jave'),
(132, 'ARK: Survival Evolved'),
(133, 'Monopoly Family Fun Pack'),
(134, 'NARUTO SHIPPUDEN: Ultimate Ninja STORM Legacy'),
(135, 'Far CryÂ® Primal'),
(136, 'Far CryÂ® Primal - Digital Apex Edition'),
(137, 'Crash Bandicootâ„¢ Bundle - N. Sane Trilogy + CTR Nitro-Fueled'),
(138, 'Final Fantasy XII The Zodiac Age'),
(139, 'RAD'),
(140, 'KINGDOM HEARTS III'),
(141, 'DARK SOULSâ„¢ III'),
(142, 'Sword Art Online Re: Hollow Fragment	'),
(143, 'Sword Art Online Re: Hollow Fragment'),
(144, 'SWORD ART ONLINE: FATAL BULLE'),
(145, 'SWORD ART ONLINE: Hollow Realization'),
(146, 'Uncharted 4: A Thiefâ€™s End'),
(147, 'Death Stranding Diablo III: Eternal Collection'),
(148, 'Diablo III: Eternal Collection'),
(149, 'The Witcher 3: Wild Hunt â€“ Complete Edition');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metpagos`
--

CREATE TABLE `metpagos` (
  `idPago` int(11) NOT NULL,
  `nombreMP` varchar(35) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `metpagos`
--

INSERT INTO `metpagos` (`idPago`, `nombreMP`) VALUES
(1, '---'),
(8, 'Agente BCP Lima'),
(9, 'Agente BCP Provincia'),
(10, 'Interbank'),
(11, 'BBVA'),
(12, 'Yape'),
(13, 'LUKITA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `idRol` int(11) NOT NULL,
  `nombreRol` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idRol`, `nombreRol`) VALUES
(0, 'Administrador'),
(1, 'Vendedor'),
(2, 'Creador de Juegos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodeventa`
--

CREATE TABLE `tipodeventa` (
  `idTipoVenta` int(11) NOT NULL,
  `nombreVenta` varchar(35) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipodeventa`
--

INSERT INTO `tipodeventa` (`idTipoVenta`, `nombreVenta`) VALUES
(0, 'Principal'),
(1, 'Secundaria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `user` varchar(30) NOT NULL,
  `pswd` varchar(60) NOT NULL,
  `nomUsuario` varchar(50) NOT NULL,
  `apellUsuario` varchar(50) NOT NULL,
  `dniUsuario` varchar(8) NOT NULL,
  `rol_idRol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `user`, `pswd`, `nomUsuario`, `apellUsuario`, `dniUsuario`, `rol_idRol`) VALUES
(4, 'luis', '123456', '', '', '', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`),
  ADD KEY `idCuenta` (`cuenta_idCuenta`,`metpagos_idPago`,`juego_idJuego`),
  ADD KEY `juego_idJuego` (`juego_idJuego`),
  ADD KEY `metpagos_idPago` (`metpagos_idPago`),
  ADD KEY `tVenta_idtVenta` (`tVenta_idtVenta`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`idCuenta`),
  ADD KEY `cuenta_idPadre` (`cuenta_idPadre`,`juego_idJuego`),
  ADD KEY `juego_idJuego` (`juego_idJuego`),
  ADD KEY `estadoCuenta` (`estadoCuenta`);

--
-- Indices de la tabla `estadocuenta`
--
ALTER TABLE `estadocuenta`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indices de la tabla `finanzas`
--
ALTER TABLE `finanzas`
  ADD PRIMARY KEY (`idFinanza`),
  ADD KEY `diaAnterior` (`diaAnterior`);

--
-- Indices de la tabla `juego`
--
ALTER TABLE `juego`
  ADD PRIMARY KEY (`idJuego`);

--
-- Indices de la tabla `metpagos`
--
ALTER TABLE `metpagos`
  ADD PRIMARY KEY (`idPago`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `tipodeventa`
--
ALTER TABLE `tipodeventa`
  ADD PRIMARY KEY (`idTipoVenta`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `rol_idRol` (`rol_idRol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=662;

--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `idCuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=534;

--
-- AUTO_INCREMENT de la tabla `estadocuenta`
--
ALTER TABLE `estadocuenta`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `finanzas`
--
ALTER TABLE `finanzas`
  MODIFY `idFinanza` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `juego`
--
ALTER TABLE `juego`
  MODIFY `idJuego` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT de la tabla `metpagos`
--
ALTER TABLE `metpagos`
  MODIFY `idPago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipodeventa`
--
ALTER TABLE `tipodeventa`
  MODIFY `idTipoVenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`juego_idJuego`) REFERENCES `juego` (`idJuego`),
  ADD CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`metpagos_idPago`) REFERENCES `metpagos` (`idPago`),
  ADD CONSTRAINT `cliente_ibfk_3` FOREIGN KEY (`cuenta_idCuenta`) REFERENCES `cuenta` (`idCuenta`);

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `cuenta_ibfk_1` FOREIGN KEY (`juego_idJuego`) REFERENCES `juego` (`idJuego`),
  ADD CONSTRAINT `cuenta_ibfk_2` FOREIGN KEY (`cuenta_idPadre`) REFERENCES `cuenta` (`idCuenta`);

--
-- Filtros para la tabla `finanzas`
--
ALTER TABLE `finanzas`
  ADD CONSTRAINT `finanzas_ibfk_1` FOREIGN KEY (`diaAnterior`) REFERENCES `finanzas` (`idFinanza`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
